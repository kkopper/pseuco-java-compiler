/*******************************************************************************
* Copyright (c) 2013, Saarland University. All rights reserved.
* Lisa Detzler
******************************************************************************/

package include;

import java.util.LinkedList;

public class Main { 
    public static Object wildcard = new Object();
    public static PseuCoThread thread = new PseuCoThread("mainAgentThread");
    public static IntHandshakeChan ch0 = new IntHandshakeChan(), ch1 = new IntHandshakeChan(), ch2 = new IntHandshakeChan();
    public static BoolHandshakeChan done = new BoolHandshakeChan();
    
    public static void PseuCo_who(IntChannel left, IntChannel right, BoolChannel dne, int id){
        int v = 0;
        
        Work work = new Work(right, true, Thread.currentThread(), 0);
        Work work1 = new Work(left, false, id, Thread.currentThread(), 1);
        LinkedList<Work> workList = new LinkedList<Work>();
        workList.add(work);
        workList.add(work1);
        Message message = Handshake.handleSelect(workList);
        switch (message.getWorkId()) {
            case 0: {
                v = message.getMessage(v);
            
            System.out.println("Agent" + id + "is receiving from his right neighbour the value " + v);    break;
            }
            case 1: {
            
            System.out.println("Agent" + id + "is sending to his left neighbour his id " + id);    break;
            }
            default: {
                break;
            }
        }
        Work work2 = new Work(dne, false, true, Thread.currentThread(), 1);
        dne.handleSelect(work2);
    }
    
    
    public static void main(String[] args) {
        final BoolHandshakeChan doneCopy = done;
        final IntHandshakeChan ch1Copy = ch1;
        final IntHandshakeChan ch0Copy = ch0;
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_who(ch0Copy, ch1Copy, doneCopy, 1);
            } 
        }).start();
        final BoolHandshakeChan doneCopy1 = done;
        final IntHandshakeChan ch1Copy1 = ch1;
        final IntHandshakeChan ch2Copy = ch2;
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_who(ch1Copy1, ch2Copy, doneCopy1, 2);
            } 
        }).start();
        final BoolHandshakeChan doneCopy2 = done;
        final IntHandshakeChan ch2Copy1 = ch2;
        final IntHandshakeChan ch0Copy1 = ch0;
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_who(ch2Copy1, ch0Copy1, doneCopy2, 3);
            } 
        }).start();
        Work work3 = new Work(done, true, Thread.currentThread(), 1);
        done.handleSelect(work3);
        Work work4 = new Work(done, true, Thread.currentThread(), 1);
        done.handleSelect(work4);
        
        System.out.println("....... now");
        
        System.out.println("....... terminating");
    }
    
}