plugins {
    java
    distribution
}

repositories {
    jcenter()
}

val junitVersion = "5.5.2"

dependencies {
    testImplementation("junit", "junit", "4.12")

    testRuntimeOnly("org.junit.vintage", "junit-vintage-engine", junitVersion)
}

description = "pseuCo Java Compiler"
version = "2.3" // see also MainCodeGen.version

java {
    sourceCompatibility = if (JavaVersion.current().isJava12Compatible) JavaVersion.VERSION_1_8 else JavaVersion.VERSION_1_6
    targetCompatibility = JavaVersion.VERSION_1_8
}

sourceSets {
    main {
        java {
            srcDir("src")
            exclude("codeGenTests/**")
            exclude("lexerParserTests/**")
        }
    }
    test {
        java {
            setSrcDirs(listOf("src/codeGenTests", "src/lexerParserTests"))
        }
    }
}

val jar by tasks.getting(Jar::class) {
    val version by archiveVersion
    manifest {
        attributes(mapOf(
            "Main-Class" to "main.PseuCoCo",
            "Implementation-Title" to "PseuCoCo",
            "Implementation-Version" to version,
            "Class-Path" to "."
        ))
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform {
        includeEngines("junit-vintage")
    }
}

distributions {
    main {
        var baseName by distributionBaseName
        baseName = "PseuCoCo"
        contents {
            from(jar.outputs.files)
            into("include") {
                from("include")
            }
        }
    }
}
