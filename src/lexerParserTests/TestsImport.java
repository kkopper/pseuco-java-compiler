/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package lexerParserTests;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import tree.LDNode;
import tree.ParseException;
import tree.PseuCoParser;
import tree.TokenMgrError;
import typeChecker.TypeChecker;

/**
 * @author Sebastian Biewer
 */

@RunWith(Parameterized.class)
public class TestsImport {
	private File currentFile = null;	
	private PseuCoParser parser = null;
	private static int counter = 0;

	public TestsImport(File f) {
		this.currentFile = f;
	}
	
	@Parameters
	public static Collection<Object[]> configs() {
		String folder = "TestFiles/ImportTests/";
		return Arrays.asList(new Object[][]{
				{ new File(folder + "singleImport.pc") } ,
				{ new File(folder + "multipleImport.pc") },
				{ new File(folder + "singleImportDesc.pc") },
				{ new File(folder + "descLoop.pc") },
				{ new File(folder + "subFolder.pc") },
				{ new File(folder + "subLoop.pc") },
				{ new File(folder + "input_monitor.pc")}
		});
	}
		
	@Test
	public void TestImport() {
		try {
			parser = new PseuCoParser(currentFile);
			parser.Program();
			assert(parser.rootNode() instanceof LDNode);
//			System.out.println(((LDNode)parser.rootNode()).printTree());
			TypeChecker tc = new TypeChecker((LDNode)parser.rootNode());
			tc.checkTypes();
			if(!tc.isTypedCorrectly()) {
				//System.err.println("Is not correctly typed\n");
				fail();
			}
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			fail();
		} catch(TokenMgrError error) {
			System.err.println(error.getMessage());
			fail();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());
			fail();
		}
	}


}
