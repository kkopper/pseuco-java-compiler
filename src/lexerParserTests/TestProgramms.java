/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package lexerParserTests;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import tree.LDNode;
import tree.ParseException;
import tree.PseuCoParser;

/**
 * @author Sebastian Biewer
 */

@RunWith(Parameterized.class)
public class TestProgramms {
	private File currentFile = null;	
	private PseuCoParser parser = null;

	public TestProgramms(File f) {
		currentFile = f;
		try {
			parser = new PseuCoParser(currentFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Parameters
	public static Collection configs() {
		File folder = new File("TestFiles/Listings");
		LinkedList<File[]> files = new LinkedList<File[]>();
		if(folder.exists() && folder.isDirectory()) {
			for(File f : folder.listFiles()){
				if(f.isFile()){
					if(f.getName().endsWith(".pc")) {
						files.add(new File[]{f});
					}
				}
			}
		}
		return files;
	}
	
	@Before
	public void before() {
		try {
			parser = new PseuCoParser(currentFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void BasicProgram() {
		System.out.println("TEST: " + currentFile.getName());
		try {
			parser.Program();
			assert(parser.rootNode() instanceof LDNode);
//			System.out.println(((LDNode)parser.rootNode()).printTree());
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("TEST FINISHED WITH ERRORS: " + currentFile.getName());
			fail();
		}
		
		System.out.println("TEST FINISHED: " + currentFile.getName());
	}

}
