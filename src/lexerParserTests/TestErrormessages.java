/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package lexerParserTests;

/**
 * @author Sebastian Biewer
 */

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import tree.LDNode;
import tree.ParseException;
import tree.PseuCoParser;
import tree.TokenMgrError;

@RunWith(Parameterized.class)
public class TestErrormessages {
	private File currentFile = null;	
	private PseuCoParser parser = null;

	public TestErrormessages(File f) {
		currentFile = f;
		try {
			parser = new PseuCoParser(new FileReader(currentFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Parameters
	public static Collection configs() {
		File folder = new File("TestFiles/ErrorFiles");
		return recursiveConfigs(folder);
	}
	
	public static LinkedList<File[]> recursiveConfigs(File folder) {
		LinkedList<File[]> files = new LinkedList<File[]>();
		if(folder.exists() && folder.isDirectory()) {
			for(File f : folder.listFiles()){
				if(f.isFile()){
					if(f.getName().endsWith(".pc")) {
						files.add(new File[]{f});
					}
				} else if(f.isDirectory() && !f.getName().equals(".") && !f.getName().equals("..")) {
					files.addAll(recursiveConfigs(f));
				}
			}
		}
		return files;
	}
	
	@Before
	public void before() {
		try {
			System.out.println("Test: " + currentFile.getName());
			this.parser.ReInit(new FileReader(currentFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void BasicProgram() {
		try {
			parser.Program();
			assert(parser.rootNode() instanceof LDNode);
			fail();
		} catch (ParseException e) {
			System.err.println(e.getMessage());
		} catch(TokenMgrError error) {
			System.err.println(error.getMessage());
		}
	}
}
