/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package lexerParserTests;

/**
 * @author Sebastian Biewer
 */
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import tree.LDNode;
import tree.ParseException;
import tree.PseuCoParser;
import tree.TokenMgrError;
import typeChecker.TypeChecker;

@RunWith(Parameterized.class)
public class BugReports {
	private File currentFile = null;	
	private PseuCoParser parser = null;
	private static int counter = 0;

	public BugReports(File f) {
		this.currentFile = f;
	}
	
	@Parameters
	public static Collection<Object[]> configs() {
		String folder = "TestFiles/BugReports/";
		return Arrays.asList(new Object[][]{
				{ new File(folder + "06_06_return.pseuco") } ,
				{ new File(folder + "06_06_return1.pseuco") } ,
				{ new File(folder + "06_09_komma.pseuco") } ,
		});
	}
	
	@Test
	public void Bug () {
		try {
			PseuCoParser parser = new PseuCoParser(currentFile);
			parser.Program();
			assert(parser.rootNode() instanceof LDNode);
//			System.out.println(((LDNode)parser.rootNode()).printTree());
			TypeChecker tc = new TypeChecker((LDNode)parser.rootNode());
			tc.checkTypes();
			if(!tc.isTypedCorrectly()) {
				//System.err.println("Is not correctly typed\n");
				fail();
			}
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			fail(currentFile.getName());
		} catch(TokenMgrError error) {
			System.err.println(error.getMessage());
			fail(currentFile.getName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());
			fail(currentFile.getName());
		}
	}

}
