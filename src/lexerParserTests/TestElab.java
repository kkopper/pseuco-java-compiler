/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package lexerParserTests;


/**
 * @author Sebastian Biewer
 */
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import tree.LDNode;
import tree.ParseException;
import tree.PseuCoParser;
import tree.TokenMgrError;
import typeChecker.TypeChecker;

@RunWith(Parameterized.class)
public class TestElab {
	private File currentFile = null;	
	private PseuCoParser parser = null;
	private static int counter = 0;
	
	public TestElab(File f) {
		currentFile = f;
		try {
			parser = new PseuCoParser(new FileReader(currentFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Parameters
	public static Collection configs() {
		File folder = new File("TestFiles/Listings");
		return recursiveConfigs(folder, false);
	}
	
	public static LinkedList<File[]> recursiveConfigs(File folder, boolean rec) {
		LinkedList<File[]> files = new LinkedList<File[]>();
		if(folder.exists() && folder.isDirectory()) {
			for(File f : folder.listFiles()){
				if(f.isFile()){
					if(f.getName().endsWith(".pc")) {
						files.add(new File[]{f});
					}
				} else if(f.isDirectory() && !f.getName().equals(".") && !f.getName().equals("..") && rec) {
					files.addAll(recursiveConfigs(f, true));
				}
			}
		}
		return files;
	}
	
	@Before
	public void before() {
		try {
			counter++;
			System.out.println("Test["+counter+"]: " + currentFile.getName());
			this.parser.ReInit(new FileReader(currentFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void BasicProgram() {
		try {
			parser.Program();
			assert(parser.rootNode() instanceof LDNode);
			TypeChecker tc = new TypeChecker((LDNode)parser.rootNode());
			tc.checkTypes();
			if(!tc.isTypedCorrectly()) {
				//System.err.println("Is not correctly typed\n");
				fail();
			}
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			fail();
		} catch(TokenMgrError error) {
			System.err.println(error.getMessage());
			fail();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			System.err.println(e.getMessage());
			e.printStackTrace();
			fail();
		}
	}


}
