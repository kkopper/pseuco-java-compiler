/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package main;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

//import sun.misc.Queue;
import tree.PseuCoParser;
import codeGen.Code;
import codeGen.CodeGen;
import codeGen.CodeGenError;
import codeGen.MainCode;
import codeGen.MainCodeGen;

/**
 * Contains the <tt>main</tt> method of the compiler which is executed when
 * starting the compiler.
 * 
 * @author Lisa Detzler
 * 
 */
public class PseuCoCo {

	/**
	 * The <tt>main</tt> method of the compiler which is executed when starting
	 * the compiler. Reads the user input, starts the code generation process,
	 * handles potentially occurring exceptions and invokes the <tt>main</tt>
	 * method of the generated Java code if the user wants the PseuCo source
	 * code to execute.
	 * 
	 * @param args
	 *            The user input: "[-i <filepath>] [-e <folderpath>]"
	 *            <p>
	 *            "-i <filepath>\": Defines a file containing the PseuCo source
	 *            code. Without this argument the code has to be typed into the
	 *            console.
	 *            <p>
	 *            "-e <folderpath>\": This argument decides whether the compiler
	 *            should execute the code or translate it into Java.
	 *            <folderpath> specifies the path to a folder for creating the
	 *            translated code. When the folder does not exist the compiler
	 *            creates a new one with the specified path. If this argument is
	 *            not declared, the compiler is started in execution modus.
	 * @throws Exception
	 *             If a compile error occurred.
	 * 
	 * @author Lisa Detzler
	 */
	public static void main(String[] args) throws Exception {
		// Initializes the static fields.
		init();
		boolean isJavaCodeGeneration = false;

		// Gets Path of generatedJavaCodeFolder
		String path = PseuCoCo.class.getProtectionDomain().getCodeSource()
				.getLocation().getPath();
		String decodedPath = "";
		try {
			decodedPath = URLDecoder.decode(path, "UTF-8");

			int index = decodedPath.lastIndexOf("/");
			decodedPath = decodedPath.substring(0, index + 1);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		MainCodeGen.output = false;
		MainCodeGen.shortOutputs = true;
		MainCodeGen.generatedJavaCodeFolderPath = decodedPath
				+ MainCodeGen
						.getPlatformIndependentPath(MainCodeGen.generatedJavaCodeFolder);

		File f = new File(MainCodeGen.generatedJavaCodeFolderPath);
		if (!f.exists()) {
			CodeGenError.locationOfJarFile();
			return;
		}

		String[] pseuCoParserArgs = new String[0];

		// If an argument is typed
		if (args.length > 0) {
			// If the helpd dialog is called
			if (args[0].equals("--help") || args[0].equals("-help")) {
				// Prints the help dialog
				String out = CodeGen.getLineSeparator() + "Usage:"
						+ CodeGen.getLineSeparator()
						+ CodeGen.getLineSeparator();
				out = out
						+ "The compiler can be executed by typing the following command in a "
						+ "command line interpreter: "
						+ CodeGen.getLineSeparator()
						+ CodeGen.getLineSeparator()
						+ "    java -jar /<path>/PseuCoCo.jar [-i <filepath>] [-e <folderpath>]"
						+ CodeGen.getLineSeparator()
						+ CodeGen.getLineSeparator()
						+ "Two optional arguments can be declared: "
						+ CodeGen.getLineSeparator()
						+ "1.  \"-i <filepath>\": Defines a file containing the PseuCo source code."
						+ " Without this argument the code has to be typed into the console. "
						+ "The input can be confirmed by \"Enter -> strg+Z -> Enter\" (Linux und Windows) "
						+ "or \"Enter -> strg+D -> Enter\" (MacOS)"
						+ CodeGen.getLineSeparator()
						+ "2.  \"-e <folderpath>\": This argument decides whether the compiler should execute the"
						+ " code or translate it into Java. <folderpath> specifies the path to a folder for "
						+ "creating the translated code. When the folder does not exist the compiler creates a new"
						+ " one with the specified path. If this argument is not declared, the compiler is started "
						+ "in execution modus."
						+ CodeGen.getLineSeparator()
						+ CodeGen.getLineSeparator()
						+ "Hint: "
						+ CodeGen.getLineSeparator()
						+ "The combination \"strg+C\" (Linux, Windows) and \"strg+Z\" (MacOS) terminates "
						+ "running programs."
						+ CodeGen.getLineSeparator()
						+ "\"java -jar /<path>/PseuCoCo.jar -version\" displays the version of the compiler."
						+ CodeGen.getLineSeparator()
						+ "\"java -jar /<path>/PseuCoCo.jar -help\" provides a short overview about the compiler commands.";
				System.out.println(out);
				return;
			}
			// If the current version of the compiler is asked
			if (args[0].equals("-version")) {
				// Prints the version of the compiler.
				System.out.println("Current Version: " + MainCodeGen.version);
				return;
			}

		}

		for (int i = 0; i < args.length; i++) {

			// Handles source code file
			if (args[i].equals("-i")) {
				if (args.length > i + 1) {
					if (args[i + 1] != null && !args[i + 1].equals("-e")) {
						pseuCoParserArgs = new String[1];
						pseuCoParserArgs[0] = MainCodeGen
								.getPlatformIndependentPath(args[i + 1]);
					}
				} else {
					CodeGenError.missingImportFile();
					return;
				}
				i = i + 1;

			} else {
				// If the code should be exported
				if (args[i].equals("-e")) {

					// If a export folder path is specified
					if (args.length > i + 1) {
						// Handle code generation process
						MainCodeGen.generatedJavaCodeFolderPath = MainCodeGen
								.getPlatformIndependentPath(args[i + 1]);
						String seperator = MainCodeGen.seperator;
						String[] s = MainCodeGen
								.getPlatformIndependentPath(args[i + 1])
								.replace(seperator, "/").split("/");
						String packageName = s[s.length - 1];
						CodeGen.mainPackageName = packageName;
						CodeGen.monitorPackageName = packageName;
						CodeGen.structPackageName = packageName;

						// Creates the external files
						try {
							File dir = new File(
									MainCodeGen.generatedJavaCodeFolderPath);
							dir.mkdir();
							MainCodeGen.initiateListOfExternJavaFiles();
							MainCodeGen
									.createClassesOfExternalFiles(MainCodeGen.generatedJavaCodeFolderPath);
						} catch (IOException e) {
							CodeGenError.unexistingExportFilePath();
							return;
						}
						isJavaCodeGeneration = true;
					} else { // No export folder path specified
						CodeGenError.missingExportFile();
						return;
					}
					i = i + 1;
				} else { // Missing key word "-i" or "-e"
					CodeGenError.missingImportExportFileSpecification();
					return;
				}
			}
		}
		MainCodeGen.isJavaCodeGeneration = isJavaCodeGeneration;

		// Code Generation and execution
		startCodeGenerationAndExecutionProcess(pseuCoParserArgs);
	}

	/**
	 * Starts the code generation process, handles potentially occurring
	 * exceptions and invokes the <tt>main</tt> method of the generated Java
	 * code if the user wants the PseuCo source code to execute.
	 * 
	 * @param pseuCoParserArgs
	 *            Contains the file containing the PseuCo source code or is
	 *            empty if the file is typed into the console.
	 * @throws Exception
	 *             If a compile error occurred.
	 * 
	 * @author Lisa Detzler
	 */
	private static void startCodeGenerationAndExecutionProcess(
			String[] pseuCoParserArgs) throws Exception {
		try {
			PseuCoParser.main(pseuCoParserArgs);
		} catch (Exception e) {
			if (MainCodeGen.shortOutputs) {
				CodeGenError.exceptionToString(e);
			} else {
				CodeGenError.printStackTrace(e);
			}
			return;
		}

		// User wants to execute the PseuCo source code
		if (!MainCodeGen.isJavaCodeGeneration) {

			// Compile the generated Java code using the Java compiler
			JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
			if (compiler == null) {
				CodeGenError.noJDKUsed();
				return;
			}

			StandardJavaFileManager fileManager = compiler
					.getStandardFileManager(null, null, null);
			Iterable<? extends JavaFileObject> units;
			File javaSrcFile = new File(MainCodeGen.generatedJavaCodeFolderPath
					+ MainCodeGen.seperator + "Main.java");
			units = fileManager.getJavaFileObjectsFromFiles(Arrays
					.asList(javaSrcFile));
			CompilationTask task = compiler.getTask(null, fileManager, null,
					null, null, units);
			task.call();
			try {
				fileManager.close();

				// Execute the compiled Java code.
				URLClassLoader classLoader;

				classLoader = new URLClassLoader(new URL[] { javaSrcFile
						.getAbsoluteFile().getParentFile().getParentFile()
						.toURI().toURL() });

				Class<?> c = Class.forName(MainCodeGen.generatedJavaCodeFolder
						+ ".Main", true, classLoader);
				String[] argv = { null };
				Method m = c.getMethod("main", argv.getClass());
				try {
					// Invoke the main method of the generated Java code.
					m.invoke(null, new Object[] { argv });
				} catch (InvocationTargetException e) {
					handleExceptions(e.getCause());
					return;
				}
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
	}

	/**
	 * Handles the runtime exceptions thrown during the execution of the
	 * generated java code.
	 * 
	 * @param e
	 *            The thrown exception.
	 * @author Lisa Detzler
	 */
	public static void handleExceptions(Throwable e) {
		StackTraceElement[] traces = e.getStackTrace();

		String exception = "";
		if (e != null) {
			exception = e.toString();
		}
		String part1 = "Error: " + exception;

		for (int i = 0; i < traces.length; i++) {
			StackTraceElement trace = traces[i];
			String fileName = trace.getFileName();
			int index = fileName.indexOf(".java");
			String fileName2 = fileName.substring(0, index);
			File file = new File(MainCodeGen.generatedJavaCodeFolderPath
					+ MainCodeGen.seperator + fileName);
			if (file.exists()
					&& (!MainCodeGen.listOfExternJavaFiles.contains(fileName) || fileName
							.contains("Main.java"))) {
				CodeGenError.errorInPseuCoCode(e, part1, trace, fileName2);
				return;
			}
		}
		CodeGenError.compilerError();
	}

	/**
	 * Returns the path printed in the exception and corresponding to the
	 * specified file and method names.
	 * 
	 * @param fileName
	 *            The name of the file the error occurred in.
	 * @param methodName
	 *            The name of the method the error occurred in.
	 * @return The error path of the compiler exception.
	 * 
	 * @author Lisa Detzler
	 */
	public static String getErrorPath(String fileName, String methodName) {
		if (fileName.equals("Main")) {
			if (methodName.equals("main")) {
				fileName = "mainAgent";
			} else {
				fileName = "procedure " + methodName;
			}
		} else {
			fileName = fileName + ", procedure " + methodName;
		}
		return fileName;
	}

	/**
	 * Transfers the input line number list to a string representation. If the
	 * line number "-2" is contained an exception is thrown.
	 * 
	 * @param pseuCoLineNumber
	 *            The PseuCo line number list.
	 * @return The string representation.
	 * 
	 * @author Lisa Detzler
	 */
	public static String handleLineNumberMatching(
			LinkedList<Integer> pseuCoLineNumber) {
		String pseuCoLineNumbers = "";
		if (pseuCoLineNumber.contains(-2)) {
			// compiler error, no user error
			CodeGenError.compilerError();
		}

		for (Integer s : pseuCoLineNumber) {
			pseuCoLineNumbers = pseuCoLineNumbers + s + " ";
		}
		return pseuCoLineNumbers;
	}

	/**
	 * Initializes the static fields.
	 * 
	 * @author Lisa Detzler
	 */
	private static void init() {
		MainCodeGen.rootNode = null;
		MainCodeGen.output = true;
		MainCodeGen.generatedCode = null;
		MainCodeGen.isJavaCodeGeneration = true;

		CodeGen.mainCodeObject = new MainCode();
		CodeGen.monitors = new HashMap<String, Code>();
		CodeGen.structs = new HashMap<String, Code>();
		CodeGen.usedVariableNames = new HashSet<String>();
	}

}
