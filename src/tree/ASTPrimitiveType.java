/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTPrimitiveType.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.HashSet;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import typeChecker.TypeType;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.MainCodeGen;
import codeGen.Method;

public class ASTPrimitiveType extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTPrimitiveType(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTPrimitiveType(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		if (jjtGetNumChildren() == 1)
			return getChild(0).getType(env); // Channel

		Type type;

		Token token = jjtGetFirstToken();
		switch (token.kind) {
		case PseuCoParserConstants.BOOL:
			type = new Type(Type.BOOL);
			break;

		case PseuCoParserConstants.INT:
			type = new Type(Type.INT);
			break;

		case PseuCoParserConstants.STRING:
			type = new Type(Type.STRING);
			break;

		case PseuCoParserConstants.LOCK:
			type = new Type(Type.LOCK);
			break;

		case PseuCoParserConstants.AGENT:
			type = new Type(Type.AGENT);
			break;

		case PseuCoParserConstants.IDENTIFIER:
			type = env.getGlobalValue(token.image, this.firstToken);
			break;

		default:
			throw new IllegalStateException("Something is wrong here!");
		}

		return new TypeType(type);
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		if (this.getNumberOfChildren() == 1) {
			return this.getChild(0).generateCode(relatedClass, relatedMethod,
					true);
		}
		passNodeFieldsToTheParentNode();
		Token token = this.firstToken;
		switch (token.kind) {
		case PseuCoParser.INT:
			HashSet<Integer> set = new HashSet<Integer>();
			set.add(token.beginLine);
			set.add(token.endLine);
			return new CodeLineList(new CodeLine("int", set));
		case PseuCoParser.STRING:
			set = new HashSet<Integer>();
			set.add(token.beginLine);
			set.add(token.endLine);
			return new CodeLineList(new CodeLine("String", set));
		case PseuCoParser.BOOL:
			set = new HashSet<Integer>();
			set.add(token.beginLine);
			set.add(token.endLine);
			return new CodeLineList(new CodeLine("boolean", set));
		case PseuCoParser.LOCK:
			relatedClass.addImport(new CodeLine(
					"java.util.concurrent.locks.ReentrantLock", -2));
			set = new HashSet<Integer>();
			set.add(token.beginLine);
			set.add(token.endLine);
			return new CodeLineList(new CodeLine("ReentrantLock", set));
		case PseuCoParser.AGENT:
			set = new HashSet<Integer>();
			set.add(token.beginLine);
			set.add(token.endLine);
			return new CodeLineList(new CodeLine("PseuCoThread", set));
		case PseuCoParser.IDENTIFIER: // structType or monitorType
			String firstL = this.firstToken.image.substring(0, 1);
			String structType = this.firstToken.image.replaceFirst(firstL,
					firstL.toUpperCase());
			MainCodeGen.throwNoASCII(structType, this.firstToken.beginLine);
			set = new HashSet<Integer>();
			set.add(token.beginLine);
			set.add(token.endLine);
			return new CodeLineList(new CodeLine(structType, set));
		default:
			return null;
		}
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}
}
/*
 * JavaCC - OriginalChecksum=414c9bc1de444911669c1872de532a30 (do not edit this
 * line)
 */
