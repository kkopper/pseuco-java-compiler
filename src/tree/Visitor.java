/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/

package tree;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public interface Visitor <T> {
	
	public T visit(ASTAdditiveExpression node);
	public T visit(ASTArguments node);
	public T visit(ASTArrayExpression node);
	public T visit(ASTAssignDestination node);
	public T visit(ASTAssignment node);
	public T visit(ASTAssignmentOperator node);
	public T visit(ASTBlockStatement node);
	public T visit(ASTBooleanLiteral node);
	public T visit(ASTCallExpression node);
	public T visit(ASTCase node);
	public T visit(ASTChan node);
	public T visit(ASTConditionalAndExpression node);
	public T visit(ASTConditionalExpression node);
	public T visit(ASTConditionalOrExpression node);
	public T visit(ASTConditionDeclStmt node);
	public T visit(ASTDecl node);
	public T visit(ASTDeclStatement node);
	public T visit(ASTDoStatement node);
	public T visit(ASTEqualityExpression node);
	public T visit(ASTExpression node);
	public T visit(ASTExpressionList node);
	public T visit(ASTForInit node);
	public T visit(ASTFormalParameters node);
	public T visit(ASTFormalParameter node);
	public T visit(ASTForStatement node);
	public T visit(ASTForUpdate node);
	public T visit(ASTIfStatement node);
	public T visit(ASTImport node);
	public T visit(ASTLiteral node);
	public T visit(ASTMainAgent node);
	public T visit(ASTMonCall node);
	public T visit(ASTMonitor node);
	public T visit(ASTMonitorCode node);
	public T visit(ASTMultiplicativeExpression node);
	public T visit(ASTPostfixExpression node);
	public T visit(ASTPrimaryExpression node);
	public T visit(ASTPrimitiveStatement node);
	public T visit(ASTPrimitiveType node);
	public T visit(ASTPrintln node);
	public T visit(ASTProcCall node);
	public T visit(ASTProcedure node);
	public T visit(ASTProgram node);
	public T visit(ASTReceiveExpression node);
	public T visit(ASTRelationalExpression node);
	public T visit(ASTResultType node);
	public T visit(ASTReturnStatement node);
	public T visit(ASTSelectStatement node);
	public T visit(ASTSend node);
	public T visit(ASTStartExpression node);
	public T visit(ASTStatement node);
	public T visit(ASTStatementExpression node);
	public T visit(ASTStatementExpressionList node);
	public T visit(ASTStmtBlock node);
	public T visit(ASTStruct node);
	public T visit(ASTStructCode node);
	public T visit(ASTType node);
	public T visit(ASTUnaryExpression node);
	public T visit(ASTVariableDeclarator node);
	public T visit(ASTVariableInitializer node);
	public T visit(ASTWhileStatement node);
	public T visit(LDNode node);
	

}
