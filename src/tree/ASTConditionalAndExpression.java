/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTConditionalAndExpression.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.HashSet;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTConditionalAndExpression extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTConditionalAndExpression(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTConditionalAndExpression(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		if (jjtGetNumChildren() == 1)
			return getChild(0).getType(env);

		for (int i = 0; i < jjtGetNumChildren(); i++) {
			Type type = getChild(i).getType(env);
			if (type.getKind() != Type.BOOL) {
				throw new TypeIllegalException(type, "Illegal type \"" + type
						+ "\" in 'and' expression", getChild(i)
						.jjtGetFirstToken());
			}
		}

		return new Type(Type.BOOL);
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		CodeLineList code = this.getChild(0).generateCode(relatedClass,
				relatedMethod, true);
		for (int i = 1; i < this.getNumberOfChildren(); i++) {
			HashSet<Integer> set = new HashSet<Integer>();
			Token condToken = firstTokenAfterLastTokenOfChild(this.getChild(0),
					null);
			set.add(condToken.beginLine);
			set.add(condToken.endLine);
			code.addCodeToLastLine(new CodeLine(" && ", set)).concatLast(
					this.getChild(i).generateCode(relatedClass, relatedMethod,
							true));
		}
		passNodeFieldsToTheParentNode();
		return code;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
/*
 * JavaCC - OriginalChecksum=c3aced86fcf43c306c05e4cf8f275168 (do not edit this
 * line)
 */
