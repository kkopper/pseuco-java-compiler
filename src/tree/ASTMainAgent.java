/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTMainAgent.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.HashSet;

import typeChecker.Environment;
import typeChecker.GlobalEnvironment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeGen;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.MainCode;
import codeGen.MainCodeGen;
import codeGen.Method;

public class ASTMainAgent extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTMainAgent(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTMainAgent(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {

		Type t = new Type(Type.MAINAGENT);
		getChild(0).getType(env);

		return t;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected void addEncapsulatingTypesToGlobalEnvironment(
			GlobalEnvironment<Type> env) throws TypeEnvironmentException {
		Type t = new Type(Type.MAINAGENT);
		env.registerGlobalValue("mainAgent", t, this.firstToken); // avoids
		// registering
		// mainAgent
		// twice
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected void collectProcedureDeclarations(Environment<Type> env)
			throws TypeIllegalException, TypeEnvironmentException {
		getChild(0).collectProcedureDeclarations(env);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected boolean insideProcedure() {
		return true; // mainAgent is like a procedure
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String qTree() {
		return " [.{MainAgent} {\"'" + jjtGetFirstToken().image + "\"'} "
				+ getChild(0).qTree() + " ]";
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public void init(Code relatedClass, Method relatedMethod) {
		relatedClass = CodeGen.mainCodeObject;

		for (int i = 0; i < this.getNumberOfChildren(); i++) {
			this.getChild(i).init(relatedClass, relatedMethod);
		}
		if (this.returnsStartExpression && this.getParent() != null) {
			this.getParent().returnsStartExpression = true;
		}
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public void organisedCodeGen(Code relatedClass, boolean forCodeGeneration) {
		relatedClass = CodeGen.mainCodeObject;
		CodeLineList blockCode = this.getChild(0).generateCode(relatedClass,
				null, MainCodeGen.isJavaCodeGeneration);
		CodeLineList mainCodeList = new CodeLineList();
		mainCodeList.concatLast(blockCode);

		HashSet<Integer> mainSet = new HashSet<Integer>();
		mainSet.add(this.firstToken.beginLine);
		((MainCode) relatedClass).setMainMethod(mainCodeList, new CodeLine(
				"main", mainSet));
		passNodeFieldsToTheParentNodeWithoutScope();
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}
}
/*
 * JavaCC - OriginalChecksum=98a9ce29d2b3ee54d16bc5146510aa3c (do not edit this
 * line)
 */
