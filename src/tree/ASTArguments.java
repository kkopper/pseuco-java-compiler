/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTArguments.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.LinkedList;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTArguments extends LDNode {
	
	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTArguments(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTArguments(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected LinkedList<Type> getTypeList(Environment<Type> env)
			throws TypeIllegalException, TypeEnvironmentException {
		if (jjtGetNumChildren() == 0)
			return new LinkedList<Type>();

		return ((ASTExpressionList) getChild(0)).getTypeList(env);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String qTree() {
		return getQTreeList();
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		CodeLineList code = new CodeLineList(new CodeLine("(", -1));
		if (this.getNumberOfChildren() == 1) {
			code.concatLast(this.getChild(0).generateCode(relatedClass,
					relatedMethod, true));
		}
		passNodeFieldsToTheParentNode();
		return code.addCodeToLastLine(new CodeLine(")", -1));
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
/*
 * JavaCC - OriginalChecksum=d579b96a152cf91302b594dbe769787d (do not edit this
 * line)
 */
