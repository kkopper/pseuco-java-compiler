/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTUnaryExpression.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.HashSet;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTUnaryExpression extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTUnaryExpression(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTUnaryExpression(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		LDNode child = getChild(0);
		Type type = child.getType(env);

		if (child instanceof ASTUnaryExpression) {
			Token t = jjtGetFirstToken();
			if (t.kind == PseuCoParserConstants.PLUS
					|| t.kind == PseuCoParserConstants.MINUS) {
				if (type.getKind() != Type.INT)
					throw new TypeIllegalException(type,
							"Operators + and - can only used with integers, not with "
									+ type, child.jjtGetFirstToken());
			} else if (t.kind == PseuCoParserConstants.BANG) {
				if (type.getKind() != Type.BOOL)
					throw new TypeIllegalException(type,
							"Operator \"!\" can only used with booleans, not with "
									+ type, child.jjtGetFirstToken());
			} else {
				throw new IllegalStateException("Something's wrong here");
			}
		}

		return type;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String qTree() {
		String result = " [.UnaryExpression ";
		LDNode node = getChild(0);
		if (node instanceof ASTUnaryExpression)
			result += getQTreeLeave(jjtGetFirstToken());
		result += node.qTree();
		result += " ] ";

		return result;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		CodeLineList expCode = this.getChild(0).generateCode(relatedClass,
				relatedMethod, true);

		passNodeFieldsToTheParentNode();
		Token token = this.firstToken;
		HashSet<Integer> set = new HashSet<Integer>();
		set.add(token.beginLine);
		set.add(token.endLine);
		switch (token.kind) {
		case PseuCoParserConstants.PLUS:
			return new CodeLineList(new CodeLine("+", set)).concatLast(expCode);
		case PseuCoParserConstants.MINUS:
			return new CodeLineList(new CodeLine("-", set)).concatLast(expCode);
		case PseuCoParserConstants.BANG:
			return new CodeLineList(new CodeLine("!", set)).concatLast(expCode);
		default:
			return expCode;

		}
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
/*
 * JavaCC - OriginalChecksum=cbd36085dde5cf5ff1457d7df14389b2 (do not edit this
 * line)
 */
