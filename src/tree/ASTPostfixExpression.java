/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTPostfixExpression.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.HashSet;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTPostfixExpression extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTPostfixExpression(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTPostfixExpression(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		Type type = getChild(0).getType(env);

		if (type.getKind() != Type.INT)
			throw new TypeIllegalException(type,
					"Increment and decrement can only be used with integers, but not "
							+ type, jjtGetFirstToken());

		return type;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String qTree() {
		String result = " [.PostfixExpression ";
		result += getChild(0).qTree();
		result += getQTreeLeave(jjtGetLastToken());
		result += " ] ";

		return result;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		CodeLineList expCode = this.getChild(0).generateCode(relatedClass,
				relatedMethod, true);
		Token opToken = this.firstTokenAfterLastTokenOfChild(this.getChild(0),
				null);
		String op = opToken.image;
		HashSet<Integer> opSet = new HashSet<Integer>();
		opSet.add(opToken.beginLine);
		opSet.add(opToken.endLine);

		passNodeFieldsToTheParentNode();
		return expCode.addCodeToLastLine(new CodeLine(op, opSet));
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
/*
 * JavaCC - OriginalChecksum=3bef3a73a8a2acdaa57cc3c8cfe670f7 (do not edit this
 * line)
 */
