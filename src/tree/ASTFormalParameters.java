/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTFormalParameters.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.LinkedList;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTFormalParameters extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTFormalParameters(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTFormalParameters(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		return checkChildrenTypes(env);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected LinkedList<Type> getTypeList(Environment<Type> env)
			throws TypeIllegalException, TypeEnvironmentException {
		// if (jjtGetNumChildren() == 0)
		// return null;

		LinkedList<Type> result = new LinkedList<Type>();
		for (int i = 0; i < jjtGetNumChildren(); i++) {
			result.add(getChild(i).getType(env));
		}

		return result;
	}

	public String qTree() {
		return getQTreeList();
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		if (this.getChild(0) == null) {
			passNodeFieldsToTheParentNode();
			return new CodeLineList(new CodeLine("()", -1));
		}
		CodeLineList code = this.getChild(0).generateCode(relatedClass,
				relatedMethod, true);
		for (int i = 1; i < this.getNumberOfChildren(); i++) {
			code.addCodeToLastLine(new CodeLine(", ", -1));
			code.concatLast(this.getChild(i).generateCode(relatedClass,
					relatedMethod, true));
		}
		passNodeFieldsToTheParentNode();
		return code.addCodeToLastLine(new CodeLine(")", -1))
				.addCodeAtTheBeginning(new CodeLine("(", -1));
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
/*
 * JavaCC - OriginalChecksum=6a0b5096a1939e43560ddf6a7a21f08e (do not edit this
 * line)
 */
