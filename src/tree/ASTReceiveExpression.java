/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTReceiveExpression.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import java.util.HashSet;
import typeChecker.ChannelType;
import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeGen;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTReceiveExpression extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTReceiveExpression(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTReceiveExpression(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		Type type = getChild(0).getType(env);

		Token t = jjtGetFirstToken();

		while (t.kind == PseuCoParserConstants.RECIEVEOP) {
			if (!(type instanceof ChannelType))
				throw new TypeIllegalException(type,
						"Receive operator needs a chancel, but you provided a "
								+ type, getChild(0).jjtGetFirstToken());

			type = ((ChannelType) type).getChannelledType();

			t = t.next;
		}
		passNodeFieldsToTheParentNode();
		return type;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected boolean usesSendOrReceiveOperator() {
		return jjtGetFirstToken().kind == PseuCoParserConstants.RECIEVEOP
				|| getChild(0).usesSendOrReceiveOperator();
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String qTree() {
		return getQTreeUnExp();
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		CodeLineList chan = this.getChild(0).generateCode(relatedClass,
				relatedMethod, true);
		Token token = this.firstToken;
		while (token.kind == PseuCoParserConstants.RECIEVEOP) {// receive
			token = token.next;

			String thread = "Thread.currentThread()";

			String workName = getNewVariableName("work", 0);
			CodeGen.usedVariableNames.add(workName);
			String before = "Work " + workName + " = new Work(" + chan
					+ ", true, " + thread + ", 1);";
			HashSet<Integer> set = new HashSet<Integer>();
			set.add(this.firstToken.beginLine);
			set.add(this.firstToken.endLine);

			chan.addCodeToLastLine(new CodeLine(".handleSelect(" + workName
					+ ")", set));

			this.before = this.before.addCodeToLastLine(new CodeLine(before,
					set));

			LDNode node2 = this;
			while (node2.getParent() != null
					&& !(node2 instanceof ASTVariableDeclarator)) {
				node2 = node2.getParent();
			}
		}
		passNodeFieldsToTheParentNode();
		return chan;

	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}
}
/*
 * JavaCC - OriginalChecksum=a8da5575b02784620a3b9c0e6a339863 (do not edit this
 * line)
 */
