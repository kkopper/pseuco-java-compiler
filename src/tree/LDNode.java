/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
package tree;

import java.util.HashMap;
import java.util.HashSet;

import typeChecker.Environment;
import typeChecker.GlobalEnvironment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeGen;
import codeGen.CodeLineList;
import codeGen.MainCodeGen;
import codeGen.Method;

public class LDNode extends SimpleNode {

	/**
	 * Specifies if an interrupted exception has to be caught after this node.
	 * 
	 * @author Lisa Detzler
	 */
	protected boolean needsToCatchInterruptedException = false;

	/**
	 * Specifies the code that has to be handled before an interrupted exception
	 * is caught.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLineList beforeCatchInterruptedException = new CodeLineList();

	/**
	 * Specifies the code that has to be defined before the code of the node.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLineList before = new CodeLineList();

	/**
	 * Stores the variables defined in the scope of the node.
	 * 
	 * @author Lisa Detzler
	 */
	protected HashMap<String, String> variablesInScope = new HashMap<String, String>();

	/**
	 * Specifies the code that has to be defined before the code of the node.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLineList after = new CodeLineList();

	/**
	 * Specifies whether the PseuCo method referring to the node returns a start
	 * expression.F
	 * 
	 * @author Lisa Detzler
	 */
	protected boolean returnsStartExpression = false;

	/**
	 * Specifies whether the declarations of the code referring to the node
	 * needs to be defined before the code as with modifier <tt>final</tt>.
	 * modifier.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLineList finalDeclarationsToAdd = new CodeLineList();

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public LDNode(int i) {
		super(i);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public LDNode(PseuCoParser p, int i) {
		super(p, i);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public void interprete() {

	}

	/**
	 * Initializes the node fields and initializes all children.
	 * 
	 * @author Lisa Detzler
	 */
	public void init(Code relatedClass, Method relatedMethod) {
		for (int i = 0; i < this.getNumberOfChildren(); i++) {
			LDNode child = this.getChild(i);
			child.init(relatedClass, relatedMethod);
		}
		if (this.returnsStartExpression && this.getParent() != null) {
			this.getParent().returnsStartExpression = true;
		}
	}

	/**
	 * Generates the code of the actual node and stores it in the list
	 * CodeGen.organisedCode.
	 * 
	 * @param relatedClass
	 *            The code of the class in which this node is defined.
	 * @param forCodeGeneration
	 *            Specifies if the code generation is done for execution or for
	 *            code generation only.
	 * @author Lisa Detzler
	 */
	public void organisedCodeGen(Code relatedClass, boolean forCodeGeneration) {
		if (this.children != null) {
			for (Node child : this.children) {
				assert (child instanceof LDNode);
				((LDNode) child).organisedCodeGen(relatedClass,
						forCodeGeneration);
			}
			passNodeFieldsToTheParentNode();
		}
	}

	/**
	 * Proofs if there already exists a variable with the given variable name.
	 * If necessary generates an new one by adding an number.
	 * 
	 * @param variableName
	 *            The variable name to proof.
	 * @param n
	 *            The number to add.
	 * @return Returns the given variable name if it not already exists.
	 *         Otherwise returns the new one.
	 * @author Lisa Detzler
	 */
	public String getNewVariableName(String variableName, int n) {
		String newVariableName;
		if (n == 0) {
			newVariableName = variableName;
		} else {
			newVariableName = variableName + n;
		}
		Token token = MainCodeGen.rootNode.firstToken;
		while (token != null) {
			if (token.image.equals(newVariableName)) {
				return this.getNewVariableName(variableName, n + 1);
			}
			token = token.next;
		}
		if (CodeGen.usedVariableNames.contains(newVariableName)) {
			return this.getNewVariableName(variableName, n + 1);
		}
		return newVariableName;
	}

	/**
	 * Generates the code of the actual node and returns it.
	 * 
	 * @param relatedClass
	 *            Specifies the code of the class this node is defined in.
	 * @param relatedMethod
	 *            The code of the method this node is defined in.
	 * @param forCodeGeneration
	 *            Specifies if the code generation is done for execution or for
	 *            code generation only.
	 * @param inStaticContext
	 *            Declares if the call context is static or not.
	 * @return The generated code
	 * @author Lisa Detzler
	 */
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		return generateCode(this.getNumberOfChildren(), relatedClass,
				relatedMethod);
	}

	/**
	 * Generates the code of the actual node by concatenating the code of every
	 * childNode.
	 * 
	 * @param numberOfChilds
	 *            The number of the child nodes.
	 * @param relatedMethod
	 *            The code of the method this node is defined in.
	 * @param inStaticContext
	 *            Declares if the call context is static or not
	 * @return The generated code.
	 * @author Lisa Detzler
	 */
	public CodeLineList generateCode(int numberOfChilds, Code relatedClass,
			Method relatedMethod) {
		CodeLineList code = new CodeLineList();
		for (int i = 0; i < numberOfChilds; i++) {
			assert (i < this.children.length - 1);
			LDNode child = (LDNode) this.children[i];
			CodeLineList codeChild = child.generateCode(relatedClass,
					relatedMethod, true);
			code.concatLast(codeChild);
		}
		passNodeFieldsToTheParentNode();
		return code;
	}

	/**
	 * Passes the fields of this node to the parent node. The scope variables
	 * are not passed to the parent.
	 * 
	 * @author Lisa Detzler
	 */
	public void passNodeFieldsToTheParentNode() {
		LDNode parent = this.getParent();
		if (parent != null) {
			parent.variablesInScope.putAll(this.variablesInScope);

			this.passNodeFieldsToTheParentNodeWithoutScope();
		}
	}

	/**
	 * Passes the fields of this node to the parent node. The scope variables
	 * are not passed to the parent.
	 * 
	 * @author Lisa Detzler
	 */
	public void passNodeFieldsToTheParentNodeWithoutScope() {
		LDNode parent = this.getParent();
		if (parent != null) {

			if (this.needsToCatchInterruptedException) {
				parent.needsToCatchInterruptedException = true;
			}
			if (this.returnsStartExpression) {
				parent.returnsStartExpression = true;
			}
			parent.beforeCatchInterruptedException = beforeCatchInterruptedException;
			parent.before = parent.before.concatLast(before);
			parent.finalDeclarationsToAdd = parent.finalDeclarationsToAdd
					.concatLast(this.finalDeclarationsToAdd);

			parent.after = this.after;
		}
	}

	/**
	 * Writes all identifiers occurring in the code of this node into the
	 * specified list.
	 * 
	 * @param list
	 *            The list the identifiers should be added to.
	 * 
	 * @author Lisa Detzler
	 */
	public void getArgumentAndCallVariables(HashSet<String> list) {
		for (int i = 0; i < this.getNumberOfChildren(); i++) {
			this.getChild(i).getArgumentAndCallVariables(list);
		}
	}

	/**
	 * Returns the type of the specified variable without array bracket "[" and
	 * "]".
	 * 
	 * @param identifier
	 *            The variable the type is wanted.
	 * @return The type of the variable.
	 * @author Lisa Detzler
	 */
	public String getTypeOfVariable(String variableName) {
		LDNode node = this;
		while (!node.variablesInScope.containsKey(variableName)) {
			node = node.getParent();
		}
		String type = node.variablesInScope.get(variableName);
		if (type.contains(",")) {
			String type2 = type.split(",")[0];
			if (type.contains("[")) {
				int i = type.indexOf("[");
				String type1 = type.substring(i);
				type = type2 + type1;
			} else {
				type = type2;
			}
		}
		return type;
	}

	/**
	 * Checks whether the specified identifier is a field in of the main
	 * program.
	 * 
	 * @param identifier
	 * @return <tt>true</tt> if the specified identifier is a field in of the
	 *         main program, <tt>false</tt> otherwise.
	 * @author Lisa Detzler
	 */
	public boolean isMainField(String identifier) {
		if (CodeGen.mainCodeObject.getDeclarations().containsKey(identifier)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the type of the specified variable.
	 * 
	 * @param identifier
	 *            The variable the type is wanted.
	 * @return The type of the variable.
	 * @author Lisa Detzler
	 */
	public String getCompleteTypeOfVariable(String identifier) {
		LDNode node = this;
		while (node.getParent() != null
				&& !node.variablesInScope.containsKey(identifier)) {
			node = node.getParent();
		}

		String type = node.variablesInScope.get(identifier);
		String s = type.substring(0, 1);
		type = type.replaceFirst(s, s.toLowerCase());
		return type;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	// Only for Nodes that present binary expression ( child (token child)* )
	protected Token[] getOperands() {
		if (jjtGetNumChildren() == 0)
			return null;

		Token[] result = new Token[jjtGetNumChildren() - 1];
		Token token = null;
		for (int i = 0; i < jjtGetNumChildren() - 1; i++) {
			LDNode node = getChild(i);
			token = LDNode.FirstTokenAfterLastTokenOfChild(this, node, token);
			result[i] = token;
		}

		return result;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		throw new IllegalStateException("getType() must be overwritten! "
				+ this);
		// return null;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected Type checkChildrenTypes(Environment<Type> env)
			throws TypeIllegalException, TypeEnvironmentException {
		for (int i = 0; i < jjtGetNumChildren(); i++) {
			getChild(i).getType(env);
		}

		return new Type(Type.VOID);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected void addEncapsulatingTypesToGlobalEnvironment(
			GlobalEnvironment<Type> env) throws TypeEnvironmentException { /* chill */
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected void collectProcedureDeclarations(Environment<Type> env)
			throws TypeIllegalException, TypeEnvironmentException {
		for (int i = 0; i < jjtGetNumChildren(); i++) {
			LDNode n = getChild(i);
			n.collectProcedureDeclarations(env);
		}
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected boolean usesSendOrReceiveOperator() {
		for (int i = 0; i < jjtGetNumChildren(); i++) {
			if (getChild(i).usesSendOrReceiveOperator())
				return true;
		}

		return false;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected boolean insideMonitor() {
		if (jjtGetParent() != null)
			return ((LDNode) jjtGetParent()).insideMonitor();

		return false;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected boolean insideProcedure() {
		if (jjtGetParent() != null)
			return ((LDNode) jjtGetParent()).insideProcedure();

		return false;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public LDNode getChild(int i) {
		if (children != null)
			return (LDNode) children[i];
		else
			return null;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public LDNode getParent() {
		return (LDNode) parent;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public int getNumberOfChildren() {
		if (this.children != null) {
			return this.children.length;
		} else {
			return 0;
		}
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected static boolean TokenEqualToToken(Token t1, Token t2) {
		if (t1 == null || t2 == null)
			return false;

		return (t1.beginColumn == t2.beginColumn
				&& t1.endColumn == t2.endColumn && t1.beginLine == t2.beginLine && t1.endLine == t2.endLine);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public static Token FirstTokenAfterLastTokenOfChild(LDNode parent,
			LDNode child, Token startFrom) {
		if (startFrom == null)
			startFrom = parent.jjtGetFirstToken();

		Token lastToken = child.jjtGetLastToken();
		while (!TokenEqualToToken(lastToken, startFrom)) {
			if (lastToken == null || startFrom == null)
				return null;

			startFrom = startFrom.next;
		}

		return startFrom.next;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected Token firstTokenAfterLastTokenOfChild(LDNode child,
			Token startFrom) {
		return FirstTokenAfterLastTokenOfChild(this, child, startFrom);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected String nodeName() {
		return getClass().getName().replaceAll(".*\\.AST", "");
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String qTree() {
		if (jjtGetNumChildren() == 0) {
			String result = " [ .{" + nodeName() + "} ";
			Token t = jjtGetFirstToken();
			if (t != null) {
				result += getQTreeLeave(t);
			}
			result += " ]";
			return result;
		}

		return getQTreeList();
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected String getQTreeLeave(Token token) {
		return " {\"'" + token.image + "\"'} ";
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected String getQTreeList() {
		if (jjtGetNumChildren() == 0) {
			return "  {" + nodeName() + "} ";
		}

		String result = " [.{" + nodeName() + "}";

		for (int i = 0; i < jjtGetNumChildren(); i++) {
			result += " " + getChild(i).qTree();
		}

		result += " ]";

		return result;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected String getQTreeBinExp() {
		String result = " [.{" + nodeName() + "} ";
		Token token = null;
		for (int i = 0; i < jjtGetNumChildren() - 1; i++) {
			LDNode node = getChild(i);
			result += node.qTree();
			token = LDNode.FirstTokenAfterLastTokenOfChild(this, node, token);
			result += getQTreeLeave(token);
		}

		result += getChild(jjtGetNumChildren() - 1).qTree() + " ] ";

		return result;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	protected String getQTreeUnExp() {
		String result = " [. {" + nodeName() + "} ";
		LDNode node = getChild(0);
		Token firstChildToken = node.jjtGetFirstToken();

		Token token = jjtGetFirstToken();
		while (!TokenEqualToToken(token, firstChildToken)) {
			result += getQTreeLeave(token);
			token = token.next;
		}

		result += node.qTree() + " ] ";

		return result;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String printTree() {
		return this.printTree("");
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public String printTree(String intent) {
		String re = this.toString();
		re = intent + " [ " + re;
		re += " Tokens: ";
		re += this.firstToken;
		Token t = this.firstToken;
		while (t != this.lastToken) {
			re += ", " + t;
			t = t.next;
		}
		re += this.lastToken;
		if (this.children != null) {
			re += "\n";
			for (Node child : this.children) {
				assert (child instanceof LDNode);
				re += intent + ((LDNode) child).printTree(intent + "  ") + "\n";
			}
			re += intent;
		}
		re += " ] ";
		return re;

	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
