/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Lisa Detzler, Janis Sprenger
 ******************************************************************************/
/* Generated By:JJTree: Do not edit this line. ASTStatementExpressionList.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package tree;

import typeChecker.Environment;
import typeChecker.Type;
import typeChecker.TypeEnvironmentException;
import typeChecker.TypeIllegalException;
import codeGen.Code;
import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.Method;

public class ASTStatementExpressionList extends LDNode {

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTStatementExpressionList(int id) {
		super(id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public ASTStatementExpressionList(PseuCoParser p, int id) {
		super(p, id);
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	public Type getType(Environment<Type> env) throws TypeIllegalException,
			TypeEnvironmentException {
		return checkChildrenTypes(env);
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateCode(Code relatedClass, Method relatedMethod,
			boolean forCodeGeneration) {
		CodeLineList code = this.getChild(0).generateCode(relatedClass,
				relatedMethod, true);
		for (int i = 1; i < this.getNumberOfChildren(); i++) {
			code.addCodeToLastLine(new CodeLine(", ", -1)).concatLast(
					this.getChild(i).generateCode(relatedClass, relatedMethod,
							true));
		}
		passNodeFieldsToTheParentNode();
		return code;
	}

	/**
	 * @author Sebastian Biewer, Janis Sprenger
	 */
	@Override
	public <T> T accept(Visitor<T> visitor) {
		return visitor.visit(this);
	}

}
/*
 * JavaCC - OriginalChecksum=96cd13b4b6bff87566201d8ede6551b2 (do not edit this
 * line)
 */
