/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer
 ******************************************************************************/
package JavaScriptExport;

/**
 * @author Sebastian Biewer
 */
import tree.*;

/**
 * 
 * @author Sebastian Biewer
 * 
 * Implementation note: Methods that return a string beginning with "(new PC" are regular/root methods. All others are help methods and may have further implementation information.
 *
 */

public class Exporter implements Visitor<String> {
	
	private LDNode tree;
	
	private Exporter(LDNode tree) {
		this.tree = tree;
	}
	
	private String generateJavaScript() {
		return this.tree.accept(this);
	}
	
	public static String convertTree(LDNode tree) {
		return new Exporter(tree).generateJavaScript();
	}
	
	/**
	 * Creates a list of arguments to pass to functions like (x...) ->
	 * @param node
	 * @return ", <node.children[0].accept(this)>, <node.children[1].accept(this)>, ..., <node.children[n].accept(this)>"
	 */
	protected String slicedArgumentsForChildrenOfNode(LDNode node) {
		return slicedArgumentsForChildrenOfNode(node, 0, false);
	}
	
	protected String slicedArgumentsForChildrenOfNode(LDNode node, int firstChild, boolean omitFirstComma) {
		String res = "";
		for (int i = firstChild; i < node.jjtGetNumChildren(); ++i) {
			if (i > 0 || !omitFirstComma)
				res += ", ";
			res += node.getChild(i).accept(this);
		}
		return res;
	}
	
	protected String codeForBinaryOperator(LDNode node, String targetClass, boolean omitOperator) {
		String left = node.getChild(0).accept(this);
		
		Token op = null;
		for (int i = 1; i < node.jjtGetNumChildren(); i++) {
			left = "(new " + targetClass + "(" + left + ", ";
			if (!omitOperator) {
				op = LDNode.FirstTokenAfterLastTokenOfChild(node, node.getChild(i-1), op);
				left += op.image + ", ";
			}
			left += node.getChild(i).accept(this) + "))";
		}
		
		return left;
	}
	


	@Override
	public String visit(ASTProgram node) {
		String res = "(new PCProgram([";
		for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
			res += node.getChild(i).accept(this);
		}
		res += "]))";
		
		return res;
	}

	@Override
	public String visit(ASTMainAgent node) {
		return "(new PCMainAgent(" + node.getChild(0).accept(this) + "))";
	}

	@Override
	public String visit(ASTProcedure node) {
		String resultType = node.getChild(0).accept(this);
		String name = LDNode.FirstTokenAfterLastTokenOfChild(node, node.getChild(0), null).image;
		String body = node.getChild(2).accept(this);
		String parameters = node.getChild(1).accept(this);
		
		return "(new PCProcedure(" + resultType + ", " + name + ", " + body + parameters + "))";
	}

	@Override
	// Help method: returns comma separated list of PCFormalParameter objects
	public String visit(ASTFormalParameters node) {
		return this.slicedArgumentsForChildrenOfNode(node);
	}

	@Override
	public String visit(ASTFormalParameter node) {
		String type = node.getChild(0).accept(this);
		String id = LDNode.FirstTokenAfterLastTokenOfChild(node, node.getChild(0), null).image;
		return "(new PCFormalParameter(" + type + ", " + id + "))";
	}

	@Override
	public String visit(ASTMonitor node) {
		String name = node.jjtGetFirstToken().next.image;
		String decls = node.getChild(0).accept(this);
		return "(new PCMonitor(" + name + decls + "))";
	}

	@Override
	// Help method: returns comma separated list of monitor declaration items
	public String visit(ASTMonitorCode node) {
		return this.slicedArgumentsForChildrenOfNode(node);
	}

	@Override
	public String visit(ASTStruct node) {
		String name = node.jjtGetFirstToken().next.image;
		String decls = node.getChild(0).accept(this);
		return "(new PCStruct(" + name + decls + "))";
	}

	@Override
	// Help method: returns comma separated list of monitor declaration items
	public String visit(ASTStructCode node) {
		return this.slicedArgumentsForChildrenOfNode(node);
	}

	@Override
	public String visit(ASTConditionDeclStmt node) {
		String name = node.jjtGetFirstToken().next.image;
		String exp = node.getChild(0).accept(this);
		return "(new PCConditionDecl(" + name + ", " + exp + "))";
	}
	
	protected String decl(LDNode node, String targetClass) {
		String type = node.getChild(0).accept(this);
		String declarators = slicedArgumentsForChildrenOfNode(node, 1, false);
		return "(new " + targetClass + "(" + type + declarators + "))";
	}

	@Override
	public String visit(ASTDecl node) {
		return decl(node, "PCDecl");
	}

	@Override
	public String visit(ASTDeclStatement node) {
		return decl(node, "PCDeclStmt");
	}

	@Override
	public String visit(ASTVariableDeclarator node) {
		String name = node.jjtGetFirstToken().image;
		String initializer = (node.jjtGetNumChildren() > 0) ? node.getChild(0).accept(this) : "null";
		return "(new PCVariableDeclarator(" + name + ", " + initializer + "))";
	}

	@Override
	public String visit(ASTVariableInitializer node) {
		String children = slicedArgumentsForChildrenOfNode(node, 0, true);
		return "(new PCVariableInitializer(" + children + "))";
	}
	


	@Override
	public String visit(ASTType node) {
		LDNode primitive = node.getChild(0);
		String type = primitive.accept(this);
		if (node.jjtGetLastToken().equals(primitive.jjtGetLastToken())) {
			return type;
		}
			
		Token firstIndex = LDNode.FirstTokenAfterLastTokenOfChild(node, primitive, null);
		String array = "[" + firstIndex.image;
		firstIndex = firstIndex.next;
		while (firstIndex.kind == PseuCoParserConstants.INTEGER_LITERAL) {
			array += ", " + firstIndex.image;
			firstIndex = firstIndex.next;
		}
		array += "]";

		return "(new PCArrayType(" + type + ", " + array + "))";
	}

	@Override
	public String visit(ASTResultType node) {
		if (node.jjtGetNumChildren() == 1)
			return visit((ASTType)node.getChild(0));
		
		return "(new PCSimpleType(" + Constants.PCSimpleTypeVoid + "))";
	}

	@Override
	public String visit(ASTPrimitiveType node) {
		if (node.jjtGetNumChildren() == 1) {
			return node.getChild(0).accept(this);
		}
		
		Token t = node.jjtGetFirstToken();
		String type;
		switch (t.kind) {
		case PseuCoParserConstants.IDENTIFIER:
			return "(new PCClassType(" + t.image + "))";
			
		case PseuCoParserConstants.BOOL:
			type = Constants.PCSimpleTypeBool;
			break;
			
		case PseuCoParserConstants.INT:
			type = Constants.PCSimpleTypeInt;
			break;
			
		case PseuCoParserConstants.STRING:
			type = Constants.PCSimpleTypeString;
			break;
			
		case PseuCoParserConstants.LOCK:
			type = Constants.PCSimpleTypeMutex;
			break;
			
		case PseuCoParserConstants.AGENT:
			type = Constants.PCSimpleTypeAgent;
			break;

		default:
			throw new IllegalStateException("Something's wrong here");
		}
		
		return "(new PCSimpleType(" + type + "))";
	}

	@Override
	public String visit(ASTChan node) {
		Token t = node.jjtGetFirstToken();
		String valueType;
		if (t.kind == PseuCoParserConstants.INTCHAN) {
			valueType = Constants.PCSimpleTypeInt;
		} else if (t.kind == PseuCoParserConstants.BOOLCHAN) {
			valueType = Constants.PCSimpleTypeBool;
		} else if (t.kind == PseuCoParserConstants.STRINGCHAN) {
			valueType = Constants.PCSimpleTypeString;
		} else
			throw new IllegalStateException("Something's wrong here");
		
		int bufferSize = node.getBufferSize();
		String capacity = (bufferSize < 0) ? Constants.PCChannelTypeCapacityUnknown : "" + bufferSize;
		
		return "(new PCChannelType(" + valueType + ", " + capacity + "))";
	}
	
	


	@Override
	public String visit(ASTExpression node) {
		return node.getChild(0).accept(this);
	}

	@Override
	// Help method: returns a list of expressions
	public String visit(ASTExpressionList node) {
		return slicedArgumentsForChildrenOfNode(node, 0, true);
	}

	@Override
	public String visit(ASTStartExpression node) {
		return "(new PCStartExpression(" + node.getChild(0).accept(this) + "))";
	}

	@Override
	public String visit(ASTAssignment node) {
		String dest = node.getChild(0).accept(this);
		String op = node.getChild(1).jjtGetFirstToken().image;
		String exp = node.getChild(2).accept(this);
		
		return "(new PCAssignExpression(" + dest + ", " + op + ", " + exp + "))";
	}

	@Override
	public String visit(ASTAssignDestination node) {
		String id = node.jjtGetFirstToken().image;
		String indices = slicedArgumentsForChildrenOfNode(node);
		return "(new PCAssignDestination(" + id + indices + "))";
	}

	@Override
	public String visit(ASTSend node) {
		String left = node.getChild(0).accept(this);
		if (node.jjtGetNumChildren() == 2) {
			left = "(new PCSendExpression(" + left + ", " + node.getChild(1).accept(this) + "))";
		}
		return left;
	}

	@Override
	public String visit(ASTConditionalExpression node) {
		String left = node.getChild(0).accept(this);
		if (node.jjtGetNumChildren() == 3) {
			left = "(new PCSendExpression(" + left + slicedArgumentsForChildrenOfNode(node, 1, false) + "))";
		}
		return left;
	}

	@Override
	public String visit(ASTConditionalOrExpression node) {
		return codeForBinaryOperator(node, "PCOrExpression", true);
	}

	@Override
	public String visit(ASTConditionalAndExpression node) {
		return codeForBinaryOperator(node, "PCAndExpression", true);
	}

	@Override
	public String visit(ASTEqualityExpression node) {
		return codeForBinaryOperator(node, "PCEqualityExpression", false);
	}

	@Override
	public String visit(ASTRelationalExpression node) {
		return codeForBinaryOperator(node, "PCRelationalExpression", false);
	}
	
	@Override
	public String visit(ASTAdditiveExpression node) {
		return codeForBinaryOperator(node, "PCAdditiveExpression", false);
	}

	@Override
	public String visit(ASTMultiplicativeExpression node) {
		return codeForBinaryOperator(node, "PCMultiplicativeExpression", false);
	}

	@Override
	public String visit(ASTUnaryExpression node) {
		LDNode child = node.getChild(0);
		String exp = child.accept(this);
		if (!(child instanceof ASTUnaryExpression)) {
			return exp;
		}
		String op = node.jjtGetFirstToken().image;
		return "(new PCUnaryExpression(" + op + ", " + exp + "))";
	}

	@Override
	public String visit(ASTPostfixExpression node) {
		String dest = node.getChild(0).accept(this);
		String op = node.jjtGetLastToken().image;
		return "(new PCPostfixExpression(" + dest + ", " + op + "))";
	}

	@Override
	public String visit(ASTReceiveExpression node) {
		Token t = node.jjtGetFirstToken();
		int c = 0;
		while (t.kind == PseuCoParserConstants.RECIEVEOP) {
			++c;
			t = t.next;
		}
		String right = node.getChild(0).accept(this);
		while (c > 0) {
			right = "(new PCReceiveExpression(" + right + "))";
		}
		
		return right;
	}

	@Override
	public String visit(ASTCallExpression node) {
		return node.getChild(0).accept(this);
	}

	@Override
	public String visit(ASTProcCall node) {
		String procName = node.jjtGetFirstToken().image;
		String argsList = node.getChild(0).accept(this);
		return "(new PCProcedureCall(" + procName + argsList + "))";
	}

	@Override
	public String visit(ASTArguments node) {
		if (node.jjtGetNumChildren() == 0)
			return "";
		String res = node.getChild(0).accept(this);	// Expression List returns list 1, ..., n
		if (res != "")
			res = ", " + res;
		return res;
	}

	@Override
	public String visit(ASTMonCall node) {
		String left = node.getChild(0).accept(this);
		for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
			left = "(new PCClassCall(" + left + ", " + node.getChild(i).accept(this) + "))";
		}
		return left;
	}

	@Override
	public String visit(ASTArrayExpression node) {
		String left = node.getChild(0).accept(this);

		for (int i = 1; i < node.jjtGetNumChildren(); ++i) {
			left = "(new PCArrayExpression(" + left + ", " + node.getChild(i).accept(this) + "))";
		}
		
		return left;
	}

	@Override
	public String visit(ASTLiteral node) {
		String value;
		Token t = node.jjtGetFirstToken();
		if (node.jjtGetNumChildren() == 1)
			value = node.getChild(0).accept(this); // bool
		else if (t.kind == PseuCoParserConstants.INTEGER_LITERAL)
			value = "parseInt(" + t.image + ")";
		else if (t.kind == PseuCoParserConstants.STRING_LITERAL) {
			value = t.image.replaceAll("\\", "\\\\");
			value = value.replaceAll("\"", "\\\"");
			value = "\"" + value + "\"";
		} else
			throw new IllegalStateException("Something's wrong here");
		
		return "(new PCLiteralExpression(" + value + "))";
	}

	@Override
	public String visit(ASTBooleanLiteral node) {
		return (node.jjtGetFirstToken().kind == PseuCoParserConstants.TRUE) ? "true" : "false";
	}

	@Override
	public String visit(ASTPrimaryExpression node) {
		if (node.jjtGetNumChildren() == 1)
			return node.getChild(0).accept(this);
		
		String id = node.jjtGetFirstToken().image;
		return "(new PCIdentifierExpression(" + id + "))";
	}

	@Override
	public String visit(ASTStatement node) {
		if (node.jjtGetNumChildren() == 0) {
			Token t = node.jjtGetFirstToken();
			if (t.kind == PseuCoParserConstants.BREAK)
				return "(new PCBreakStmt())";
			else if (t.kind == PseuCoParserConstants.CONTINUE)
				return "(new PCContinueStmt())";
			else if (t.kind == PseuCoParserConstants.SEMICOLON)
				return "(new PCStatement())";
			else
				throw new IllegalStateException("Something's wrong here");
		}
		return node.getChild(0).accept(this);
	}

	@Override
	public String visit(ASTStmtBlock node) {
		return "(new PCStmtBlock(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}

	@Override
	public String visit(ASTBlockStatement node) {
		return node.getChild(0).accept(this);
	}

	@Override
	public String visit(ASTStatementExpression node) {
		return "(new PCStmtExpression(" + node.getChild(0).accept(this) + "))";
	}

	@Override
	public String visit(ASTStatementExpressionList node) {
		return slicedArgumentsForChildrenOfNode(node, 0, false);
	}

	@Override
	public String visit(ASTSelectStatement node) {
		return "(new PCSelectStmt(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}

	@Override
	public String visit(ASTCase node) {
		String exec;
		String cond;
		if (node.jjtGetNumChildren() == 1) {
			exec = node.getChild(0).accept(this);
			cond = "null";
		} else {
			exec = node.getChild(1).accept(this);
			cond = node.getChild(0).accept(this);
		}
		return "(new PCCase(" + exec + ", " + cond + "))";
	}

	@Override
	public String visit(ASTIfStatement node) {
		return "(new PCIfStmt(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}

	@Override
	public String visit(ASTWhileStatement node) {
		return "(new PCWhileStmt(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}

	@Override
	public String visit(ASTDoStatement node) {
		return "(new PCDoStmt(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}

	@Override
	public String visit(ASTForStatement node) {
		String init = "null", exp = "null", updates = "";
		int i = 0;
		for (; i < node.jjtGetNumChildren(); ++i) {
			LDNode n = node.getChild(i);
			if (n instanceof ASTForInit) {
				assert (init.equals("null"));
				init = n.accept(this);
			} else if (n instanceof ASTExpression) {
				assert (exp.equals("null"));
				exp = n.accept(this);
			} else {
				assert (n instanceof ASTForUpdate);
				updates = n.accept(this);
			}
		}
		return "(new PCForStmt(" + init + ", " + exp + updates + "))";
	}

	@Override
	public String visit(ASTForInit node) {
		return "(new PCForInit(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}

	@Override
	public String visit(ASTForUpdate node) {
		return node.getChild(0).accept(this);
	}

	@Override
	public String visit(ASTReturnStatement node) {
		return "(new PCReturnStmt(" + ((node.jjtGetNumChildren() == 1) ? node.getChild(0).accept(this) : "") + "))";
	}

	@Override
	public String visit(ASTPrimitiveStatement node) {
		Token t = node.jjtGetFirstToken();
		String kind;
		switch (t.kind) {
		case PseuCoParserConstants.JOIN:
			kind = Constants.PCPrimitiveStmtJoin;
			break;
			
		case PseuCoParserConstants.LOCK:
			kind = Constants.PCPrimitiveStmtLock;
			break;
			
		case PseuCoParserConstants.UNLOCK:
			kind = Constants.PCPrimitiveStmtUnlock;
			break;
			
		case PseuCoParserConstants.WAIT:
			kind = Constants.PCPrimitiveStmtWait;
			break;
			
		case PseuCoParserConstants.SIGNAL:
			kind = Constants.PCPrimitiveStmtSignal;
			break;
			
		case PseuCoParserConstants.SIGNALALL:
			kind = Constants.PCPrimitiveStmtSignalAll;
			break;
			
		default:
			throw new IllegalStateException("Something's wrong here");
		}
		
		String exp = (node.jjtGetNumChildren() == 1) ? ", " + node.getChild(0).accept(this) : "";
		return "(new PCPrimitiveStmt(" + kind + exp + "))";
	}

	@Override
	public String visit(ASTPrintln node) {
		return "(new PCPrintStmt(" + slicedArgumentsForChildrenOfNode(node, 0, true) + "))";
	}
	
	
	

	@Override
	public String visit(ASTAssignmentOperator node) {
		throw new UnsupportedOperationException();	// Should never be called
	}

	@Override
	public String visit(ASTImport node) {
		// Ignore
		return "";
	}

	@Override
	public String visit(LDNode node) {
		throw new UnsupportedOperationException("Not implemented!");
	}

}
