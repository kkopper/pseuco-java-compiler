/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer
 ******************************************************************************/
package JavaScriptExport;

/**
 * 
 * @author Sebastian Biewer
 *
 */
public class Constants {
	
	public static String PCSimpleTypeVoid = "PCSimpleType.prototype.VOID";
	public static String PCSimpleTypeBool = "PCSimpleType.prototype.BOOL";
	public static String PCSimpleTypeInt = "PCSimpleType.prototype.INT";
	public static String PCSimpleTypeString = "PCSimpleType.prototype.STRING";
	public static String PCSimpleTypeMutex = "PCSimpleType.prototype.LOCK";
	public static String PCSimpleTypeAgent = "PCSimpleType.prototype.AGENT";
	
	
	public static String PCChannelTypeCapacityUnknown = "PCChannelType.prototype.CAPACITY_UNKNOWN";
	
	
	public static String PCPrimitiveStmtJoin = "PCPrimitiveStmt.prototype.JOIN";
	public static String PCPrimitiveStmtLock = "PCPrimitiveStmt.prototype.LOCK";
	public static String PCPrimitiveStmtUnlock = "PCPrimitiveStmt.prototype.UNLOCK";
	public static String PCPrimitiveStmtWait = "PCPrimitiveStmt.prototype.WAIT";
	public static String PCPrimitiveStmtSignal = "PCPrimitiveStmt.prototype.SIGNAL";
	public static String PCPrimitiveStmtSignalAll = "PCPrimitiveStmt.prototype.SIGNAL_ALL";
	
}
