/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import tree.Token;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class GlobalEnvironment<T> extends Environment<T> {
	
	private Environment<T> globalValues;

	public GlobalEnvironment(Environment<T> parent, boolean syncReturnValueWithParent) {
		super(parent, syncReturnValueWithParent);
		Environment<T> e = parent;
		while (e != null) {
			if (e instanceof GlobalEnvironment<?>) {
				e = ((GlobalEnvironment<T>)e).getGlobalValues();
				break;
			}
			e = e.getParent();
		}
		globalValues = new Environment<T>(e, false);
	}
	
	public Environment<T> getGlobalValues() {
		return globalValues;
	}
	
	public void registerGlobalValue(String id, T v, Token occ) throws TypeEnvironmentException {
		try {
			globalValues.addValueForIdentifier(id, v, occ);
		} catch (TypeEnvironmentException e) {
			throw new TypeEnvironmentException(id, "type can't be registered twice", occ);
		}
	}
	
	public T getGlobalValue(String id, Token occ) throws TypeEnvironmentException {
		try {
			return globalValues.getValueForIdentifier(id, occ);
		} catch (TypeEnvironmentException e) {
			throw new TypeEnvironmentException(id, "type unknown", occ);
		}
	}

}
