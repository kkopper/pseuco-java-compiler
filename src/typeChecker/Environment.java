/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import tree.Token;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class Environment<T> {
	
	private Environment<T> parent;
	private HashMap<String, T> env;
	private T returnValue;
	private boolean syncReturnValueWithParent;
	private boolean returnValueIsExhaustive = false;
	private HashMap<String, T> interceptedValues;	// Keys: all identifiers that should be intercepted; values: the result of getValueForIdentifier(..)
	
	public Environment(Environment<T> parent, boolean syncReturnValueWithParent) {
		this.parent = parent;
		this.env = new HashMap<String, T>();
		if (parent != null)
			this.interceptedValues = parent.getInterceptedValues();
		this.syncReturnValueWithParent = syncReturnValueWithParent;
		
		if (syncReturnValueWithParent && parent != null) {
			returnValue = parent.getReturnValue();
			returnValueIsExhaustive = parent.isReturnValueExhaustive();
		}
	}
	
	public Environment<T> getParent() {
		return parent;
	}
	
	public HashMap<String, T> getInterceptedValues() {
		return interceptedValues;
	}
	
	public void setInterceptedValues(HashMap<String, T> iv) {
		interceptedValues = iv;
	}
	
	public void addValueForIdentifier(String id, T v, Token occ) throws TypeEnvironmentException {
		if (interceptedValues != null && interceptedValues.containsKey(id))
			throw new TypeEnvironmentException(id, "is a reserved identifier and can't be declared", occ);
		
		try {
			getValueForIdentifier(id, occ);
		} catch (TypeEnvironmentException e) {
			// Here we are if everything's okay
			env.put(id, v);
			return;
		}
		
		throw new TypeEnvironmentException(id, "can't be declared twice", occ);	// Exception above was not thrown
	}
	
	public T getValueForIdentifier(String id, Token occ) throws TypeEnvironmentException {
		if (interceptedValues != null && interceptedValues.containsKey(id))
			return interceptedValues.get(id);
		
		T result = env.get(id);
		if (result == null) {
			if (parent == null) {
				throw new TypeEnvironmentException(id, "is not declared", occ);
			} else {
				result = parent.getValueForIdentifier(id, occ);
			}
		}
		
		return result;
	}
	
	public T getGlobalValue(String id, Token occ) throws TypeEnvironmentException {
		if (parent == null) {
			throw new TypeEnvironmentException(id, "type unknown", occ);
		}
		return parent.getGlobalValue(id, occ);
	}
	
	public Map<String, T>getLevelEnvironment() {	// The env with all values specified on this environment level (not in super, not in child)
		return Collections.unmodifiableMap(env);
	}
	
	public Collection<T>getLevelTypes() {	// Types declared in current level
		return env.values();
	}
	
	
	public T getReturnValue() {
		return returnValue;
	}
	
	public void setReturnValue(T returnValue) {
		this.returnValue = returnValue;
		if (syncReturnValueWithParent && parent != null)
			parent.setReturnValue(returnValue);
	}
	
	public void setExhaustiveReturnValue(T returnValue) {
		setReturnValue(returnValue);
		setReturnValueIsExhaustive(true);
	}
	
//	public void setReturnValue(T returnValue, boolean exhaustive) {
//		this.returnValue = returnValue;
//		this.returnValueIsExhaustive = exhaustive;
//	}
	
	public boolean getSyncReturnValueWithParent() {
		return syncReturnValueWithParent;
	}
	
	public boolean isReturnValueExhaustive() {
		return returnValueIsExhaustive;
	}
	
	public void setReturnValueIsExhaustive(boolean exhaustive) {
		returnValueIsExhaustive = exhaustive;
		if (syncReturnValueWithParent && parent != null)
			parent.setReturnValueIsExhaustive(exhaustive);
	}
	
	

}
