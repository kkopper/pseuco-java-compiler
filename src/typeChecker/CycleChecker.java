/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import java.util.HashSet;
import java.util.LinkedList;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class CycleChecker {
	private HashSet<EncapsulatingType> typesToBeChecked;
	
	public CycleChecker(HashSet<EncapsulatingType> typesToBeChecked) {
		if (typesToBeChecked == null)
			throw new NullPointerException();
		
		this.typesToBeChecked = typesToBeChecked;
	}
	
	// @return: If a cycle exists from type m a trace of all types in cycle or null if no trace could be found
	@SuppressWarnings("unchecked")
	private LinkedList<EncapsulatingType> cycleTraceForType(EncapsulatingType m, HashSet<EncapsulatingType>alreadyReachable, LinkedList<EncapsulatingType>trace) {
		trace.add(m);
		if (alreadyReachable.contains(m))
			return trace;
		
		alreadyReachable.add(m);
		if (!typesToBeChecked.remove(m))
			return null;
		
		for (EncapsulatingType t : m.getUsedEncapsulatingTypes()) {
			LinkedList<EncapsulatingType> result = cycleTraceForType(t, alreadyReachable, (LinkedList<EncapsulatingType>)trace.clone()); 	// For a new path we need a new trace stack
			if (result != null)	
				return result;
		}
		
		return null;
	}
	
	public String cycleTraceForTypes() {
		while (!typesToBeChecked.isEmpty()) {
			LinkedList<EncapsulatingType> trace = cycleTraceForType(typesToBeChecked.iterator().next(), new HashSet<EncapsulatingType>(), new LinkedList<EncapsulatingType>());
			if (trace != null) {
				String s = trace.pop().toString();
				while (!trace.isEmpty()) {
					s += " -> " + trace.pop().toString();
				}
				
				return s;
			}
		}
		
		return null;
	}
	
}
