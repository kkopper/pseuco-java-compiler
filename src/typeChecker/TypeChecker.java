/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import tree.LDNode;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class TypeChecker {
	
	private LDNode root;
	
	public TypeChecker(LDNode root) {
		this.root = root;
	}
	
	public LDNode getRoot() {
		return root;
	}
	
	public void checkTypes() throws Exception {
		//Environment<Type> environment = new Environment<Type>(null);
		root.getType(null);
	}
	
	public boolean isTypedCorrectly() {
		try {
			checkTypes();
		} catch (Exception e) {
			System.err.println(e.toString());
			return false;
		}
		
		return true;
	}

}
