/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import java.util.Arrays;
import java.util.Collection;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class ProcedureType extends Type {
	
	private Type retType;
	private Type[] argTypes;

	public ProcedureType(Type returnType, Collection<Type> argumentTypes) {
		super(PROCEDURE);
		retType = returnType;
		if (argumentTypes == null)
			argTypes = null;
		else
			argTypes = argumentTypes.toArray(new Type[0]);
		
		if (retType == null)
			throw new NullPointerException();
	}
	
	public Type getReturnType() {
		return retType;
	}
	
	public Collection<Type> getArgumentTypes() {
		if (argTypes == null)
			return null;
		
		return Arrays.asList(argTypes);
	}
	
	public Type getArgumentTypeAtIndex(int i) {
		return argTypes[i];
	}
	
	public int getArgumentCount() {
		return argTypes.length;
	}
	
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof ProcedureType))
			return false;
		
		ProcedureType t = (ProcedureType) o;
		
		return t.getReturnType().equals(getReturnType()) && Arrays.equals(t.argTypes, argTypes);
	}
	
	public String toString() {
		String arg = "(";
		boolean comma = false;
		for (int i = 0; i < argTypes.length; i++) {
			if (comma)
				arg += " x ";
			else
				comma = true;
			
			arg += argTypes[i].toString();
		}
		arg += ")";
		return getReturnType().toString() + " -> " + arg;
	}

}
