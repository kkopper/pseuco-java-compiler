/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class ArrayType extends Type {
	
	private Type elementsType;
	private int capacity;

	public ArrayType(Type elementsType, int capacity) {
		super(ARRAY);
		this.elementsType = elementsType;
		this.capacity = capacity;
		
		if (elementsType == null)
			throw new NullPointerException();
		if (capacity < 1)
			throw new IllegalArgumentException("Arrays must contain at least 1 element!");
	}
	
	public Type getElementsType() {
		return elementsType;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof ArrayType))
			return false;
		
		ArrayType t = (ArrayType) o;
//		if (isMinimumCapacity() && t.isMinimumCapacity())
//			throw new IllegalStateException("This should not happen in our cases!");
//		
//		boolean capacityFulfilled = false;
//		if (isMinimumCapacity())
//			capacityFulfilled = (t.getCapacity() >= getCapacity());
//		else if (t.isMinimumCapacity())
//			capacityFulfilled = (t.getCapacity() <= getCapacity());
//		else
		boolean capacityFulfilled = (t.getCapacity() == getCapacity() || getCapacity() == 0 || t.getCapacity() == 0);
		
		return t.getKind() == getKind() && capacityFulfilled && t.getElementsType().equals(getElementsType());
	}
	
	public String toString() {
		return getElementsType().toString() + "[" + getCapacity() + "]";
	}

}
