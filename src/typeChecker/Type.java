/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class Type {
	
	public final static int VOID = 0;
	public final static int BOOL = 1;
	public final static int INT = 2;
	public final static int STRING = 3;
	
	public final static int CHANNEL = 4;
	public final static int ARRAY = 5;
	public final static int MONITOR = 6;
	public final static int STRUCTURE = 7;
	
	public final static int LOCK = 8;
	public final static int CONDITION = 9;
	
	public final static int PROCEDURE = 10;
	public final static int TYPE = 11;
	public final static int MAINAGENT = 12;
	
	public final static int AGENT = 13;
	
	public final static int WILDCARD = 14;
	
	
	private int kind;
	
	public Type(int kind) {
		this.kind = kind;
		
		if (kind < 0 || kind > 14)
			throw new IllegalArgumentException("Unknown kind of type!");
	}
	
	public int getKind() {
		return kind;
	}
	
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Type))
			return false;
		
		Type t = (Type) o;
		
		return t.getKind() == getKind();
	}
	
	public boolean isAssignableTo(Type t) {	// is this assignable to type?
		if (this.getKind() == Type.WILDCARD)
			return false;	// You can't assign wildcards!!
		
		return (equals(t) || t.getKind() == Type.WILDCARD);
	}
	
	
	public String toString() {
		switch (kind) {
		
		case INT:
			return "int";
			
		case BOOL:
			return "bool";
			
		case STRING:
			return "string";
			
		case CHANNEL:
			return "channel";
			
		case ARRAY:
			return "array";
			
		case MONITOR:
			return "monitor";
			
		case STRUCTURE:
			return "struct";
			
		case LOCK:
			return "lock";
			
		case CONDITION:
			return "condition";
			
		case PROCEDURE:
			return "procedure";
			
		case TYPE:
			return "type";
			
		case MAINAGENT:
			return "mainAgent";
			
		case AGENT:
			return "agent";
			
		case WILDCARD:
			return "wildcard";

		default:
			return "void";
		}
	}

}
