/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import tree.Token;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class TypeIllegalException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5988181639539436520L;
	
	
	private Type type;
	private String description;
	private Token token;
	
	public TypeIllegalException(Type type, String description, Token occurence) {
		this.type = type;
		this.description = description;
		this.token = occurence;
	}
	
	public Type getType() {
		return type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Token getOccurence() {
		return token;
	}
	
	public String toString() {
		return "[line " + token.beginLine + ", column " + token.beginColumn + "] " + description;
	}

}
