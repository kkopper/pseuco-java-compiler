/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import tree.Token;

/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class TypeEnvironmentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 79799317468590531L;
	
	private String id;
	//private Token occurence;
	private String description;
	private Token occurance;
	
	public TypeEnvironmentException(String id, String description, Token occurance) {
		this.id = id;
		//this.occurence = occurence;
		this.description = description;
		this.occurance = occurance;
	}
	
	@Override
	public String toString() {
		return "[line" + occurance.beginLine + ", column " + occurance.beginColumn + "] \"" + id + "\" " + description;
	}
	
	@Override
	public String getMessage() {
		return toString();
	}

}
