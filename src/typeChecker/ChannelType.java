/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class ChannelType extends Type {
	
	private Type channelledType;
	private int capacity;

	public ChannelType(Type channelledType, int capacity) {
		super(CHANNEL);
		this.capacity = capacity;
		this.channelledType = channelledType;
		
		if (channelledType == null)
			throw new NullPointerException();
		if (channelledType.getKind() != INT && channelledType.getKind() != BOOL && channelledType.getKind() != STRING)
			throw new IllegalArgumentException("Channels must be of type int, boll or string");
		if (capacity < 1)
			throw new IllegalArgumentException("Channels must have capacity of 1 (handshake) or greater!");
	}
	
	
	public Type getChannelledType() {
		return channelledType;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof ChannelType))
			return false;
		
		ChannelType t = (ChannelType) o;
		
		return t.getKind() == getKind() && t.getCapacity() == getCapacity() && t.getChannelledType().equals(getChannelledType());
	}
	
	public boolean isAssignableTo(Type type) {	// is this assignable to type?
		if (type == null || !(type instanceof ChannelType))
			return false;
		
		ChannelType t = (ChannelType)type;
		return t.getKind() == getKind() && (t.getCapacity() == getCapacity() || t.getCapacity() == 1) && t.getChannelledType().equals(getChannelledType());
	}
	
	public String toString() {
		if (getCapacity() == 1) {
			return "handshake " + getChannelledType().toString() + " " + super.toString();
		}
		return getChannelledType().toString() + " " + super.toString() + " of capacity " + getCapacity();
	}

}
