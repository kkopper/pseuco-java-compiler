/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import tree.Token;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class EncapsulatingType extends Type {
	
	private String id;
	private Environment<Type> env;
	private HashSet<EncapsulatingType> usedEncapsulatingTypes = new HashSet<EncapsulatingType>();	// as instance variables

	public EncapsulatingType(int kind, String identifier, Environment<Type> environment) {
		super(kind);
		this.id = identifier;
		this.env = new Environment<Type>(environment, false);
		
		if (kind != MONITOR && kind != STRUCTURE)
			throw new IllegalArgumentException("Data encapsulation types are monitors and structs!");
		if (id == null)
			throw new NullPointerException();
	}
	
	public String getIdentifier() {
		return id;
	}
	

	public Environment<Type> getEnvironment() {
		return env;
	}
	
	public Set<EncapsulatingType> getUsedEncapsulatingTypes() {
		return Collections.unmodifiableSet(usedEncapsulatingTypes);
	}
	
	public void addUsedEncapsulatingType(EncapsulatingType type) {
		usedEncapsulatingTypes.add(type);
	}
	
	public void removeUsedEncapsulatingType(EncapsulatingType type) {
		usedEncapsulatingTypes.remove(type);
	}
	
	public void addTypeForIdentifier(String id, Type type, Token occ) throws TypeEnvironmentException {
		env.addValueForIdentifier(id, type, occ);
	}
	
	public Type getTypeForIdentifier(String id, Token occ) throws TypeEnvironmentException {
		return env.getValueForIdentifier(id, occ);
	}
	
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof EncapsulatingType))
			return false;
		
		EncapsulatingType t = (EncapsulatingType) o;
		
		return t.getIdentifier().equals(getIdentifier());
	}
	
	public String toString() {
		return super.toString() + " " + getIdentifier();
	}
	
	public int hashCode() {
		return getIdentifier().hashCode();
	}

}
