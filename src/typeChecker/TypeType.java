/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Sebastian Biewer, Janis Sprenger
 ******************************************************************************/
package typeChecker;
/**
 * @author Sebastian Biewer, Janis Sprenger
 */
public class TypeType extends Type {
	
	private Type type;
	
	public TypeType(Type type) {
		super(TYPE);
		this.type = type;
		
		if (type == null) {
			throw new NullPointerException();
		}
	}
	
	public Type getType() {
		return type;
	}
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof TypeType))
			return false;
		
		TypeType t = (TypeType) o;
		
		return t.getType().equals(getType());
	}

}
