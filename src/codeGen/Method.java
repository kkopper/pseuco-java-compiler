/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.HashMap;

/**
 * Represents a method object.
 * 
 * @author Lisa Detzler
 * 
 */
public class Method {
	/**
	 * Specifies the class the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	private Code relatedClass;

	/**
	 * Specifies the code of the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	private CodeLineList code;

	/**
	 * Specifies the argument of the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	private CodeLine args;

	/**
	 * Specifies the name of the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLine name;

	/**
	 * Specifies the modifier of the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	private String modifier;

	/**
	 * Specifies the class the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	private boolean returnsStartExpression;

	/**
	 * Specifies the return type of the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	private CodeLine returnType;

	/**
	 * Specifies if the method is locked implicitly or explicitly. <tt>true</tt>
	 * if it is locked implicitly, <tt>false</tt> otherwise.
	 * 
	 * @author Lisa Detzler
	 */
	private boolean isSynchronized;

	/**
	 * Stores the methods that are defined inside of this method. The key
	 * specifies the method names, the values the method object.
	 * 
	 * @author Lisa Detzler
	 */
	private HashMap<String, Method> innerMethods = new HashMap<String, Method>();

	/**
	 * Creates a new method object.
	 * 
	 * @param modifier
	 *            The modifier of the method.
	 * @param returnType
	 *            The return type of the method.
	 * @param name
	 *            The name of the method.
	 * @param args
	 *            The arguments of the method.
	 * @param code
	 *            The body code of the method.
	 * @param relatedClass
	 *            The class the method is defined in.
	 * @author Lisa Detzler
	 */
	public Method(String modifier, CodeLine returnType, CodeLine name,
			CodeLine args, CodeLineList code, Code relatedClass) {
		this.relatedClass = relatedClass;
		this.modifier = modifier;
		if (!CodeGen.needsPseuCoPrefix(returnType.toString())) {
			this.returnType = returnType;
		} else {
			this.returnType = returnType;
			this.returnType.concatFirst(new CodeLine("PseuCo_", -1));
		}
		this.name = name;
		if (!(this instanceof MainMethod)) {
			this.name.concatFirst(new CodeLine("PseuCo_", -1));
		}
		this.args = args;
		this.code = code;
		this.setReturnsStartExpression(false);
	}

	/**
	 * Returns the modifier of the method.
	 * 
	 * @return The modifier of the method.
	 * @author Lisa Detzler
	 */
	public String getModifier() {
		return modifier;
	}

	/**
	 * Set the modifier of the method to the specified one.
	 * 
	 * @param The
	 *            modifier of the method.
	 * @author Lisa Detzler
	 */
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	/**
	 * Returns the arguments of the method.
	 * 
	 * @return The arguments of the method.
	 * @author Lisa Detzler
	 */
	public CodeLine getArgs() {
		return args;
	}

	/**
	 * Sets the arguments of the method to the specified one.
	 * 
	 * @return The arguments of the method.
	 * @author Lisa Detzler
	 */
	public void setArgs(CodeLine args) {
		this.args = args;
	}

	/**
	 * Returns the name of the method.
	 * 
	 * @return The name of the method.
	 * @author Lisa Detzler
	 */
	public CodeLine getName() {
		return name;
	}

	/**
	 * Sets the name of the method to the specified one.
	 * 
	 * @return The name of the method.
	 * @author Lisa Detzler
	 */
	public void setName(CodeLine name) {
		this.name = name;
		this.name.concatFirst(new CodeLine("PseuCo_", -1));
	}

	/**
	 * Generates the Java code represented by the method object..
	 * 
	 * @return The generated code.
	 * @author Lisa Detzler
	 */
	public CodeLineList getMethodCode() {
		if (this.code.isEmpty()) {
			this.code = new CodeLineList(new CodeLine("{}", -1));
		}
		CodeLineList returnCode = new CodeLineList();

		// needs locking
		if (!isSynchronized && relatedClass instanceof MonitorCode) {
			CodeLineList code1 = new CodeLineList()

			.addNewLine(new CodeLine(CodeGen.monitorLockName + ".lock();", -2))
					.addNewLine(new CodeLine("try", -2))
					.addCodeToLastLine(new CodeLine("{", -1)).addNewLine();
			this.code.replaceFirst("{", "");
			this.code = code1.concatLast(this.code).addCodeToLastLine(
					new CodeLine("finally{", -2));
			this.code.addNewLine(new CodeLine("    " + CodeGen.monitorLockName
					+ ".unlock();", -2));
			this.code.addNewLine(new CodeLine("}", -1));
			returnCode.addNewLine(new CodeLine(this.modifier + " "
					+ this.returnType + " " + this.name + this.args + "{", -1));
			returnCode.concatLast(CodeGen.shiftCode(this.code));
			returnCode.addNewLine(new CodeLine("}", -1));
			relatedClass.addImport(new CodeLine(
					"java.util.concurrent.locks.ReentrantLock", -2));
		} else {
			// needs no extra locking -> monitor methods are synchronized
			if (relatedClass instanceof MonitorCode) {
				this.modifier = "synchronized " + this.modifier;
			}
			returnCode.addNewLine(new CodeLine(this.modifier + " "
					+ this.returnType + " " + this.name + this.args, name
					.getPseuCoLineNumbers()));
			returnCode.concatLast(this.code);
		}
		return returnCode;
	}

	/**
	 * Sets the body code of the method to the specified one.
	 * 
	 * @return The body code of the method.
	 * @author Lisa Detzler
	 */
	public void setCode(CodeLineList code) {
		this.code.concatLast(code);
	}

	/**
	 * Checks whether the PseuCo method corresponding to the method object
	 * returns a start expression.
	 * 
	 * @return <tt>true</tt> if the PseuCo method returns a start expression,
	 *         <tt>false</tt> otherwise.
	 * @author Lisa Detzler
	 */
	public boolean returnsStartExpression() {
		return returnsStartExpression;
	}

	/**
	 * Stores that the PseuCo method corresponding to the method object returns
	 * a start expression if <tt>returnStartExpression</tt> is <tt>true</tt>. It
	 * returns no start expression if <tt>returnStartExpression</tt> is
	 * <tt>false</tt>.
	 * 
	 * @param returnStartExpression
	 *            Specifies whether the PseuCo method corresponding to the
	 *            method object returns a start expression or not.
	 * @author Lisa Detzler
	 */
	public void setReturnsStartExpression(boolean returnStartExpression) {
		this.returnsStartExpression = returnStartExpression;
	}

	/**
	 * Returns the return type of the method.
	 * 
	 * @return The return type of the method.
	 * @author Lisa Detzler
	 */
	public CodeLine getReturnType() {
		return returnType;
	}

	/**
	 * Sets the return type of the method to the specified one.
	 * 
	 * @param returnType
	 *            The return tpye of the method.
	 * @author Lisa Detzler
	 */
	public void setReturnType(CodeLine returnType) {
		if (!CodeGen.needsPseuCoPrefix(returnType.toString())) {
			this.returnType = returnType;
		} else {
			this.returnType = returnType;
			this.returnType.addCodeBefore("PseuCo_");
		}
	}

	/**
	 * Checks whether the method is locked implicitly or explicitly.
	 * 
	 * @return <tt>true</tt> if the method is implicitly locked, <tt>false</tt>
	 *         otherwise.
	 * @author Lisa Detzler
	 */
	public boolean isSynchronized() {
		return isSynchronized;
	}

	/**
	 * Stored that the method is locked implicitly if <tt>isSynchronized</tt> it
	 * <tt>true</tt>. Otherwise it is explicitly locked.
	 * 
	 * @param isSynchronized
	 *            Specifies whether the method is locked implicitly or
	 *            explicitly.
	 * @author Lisa Detzler
	 */
	public void setSynchronized(boolean isSynchronized) {
		this.isSynchronized = isSynchronized;
	}

	/**
	 * Returns the methods defined inside of this method object.
	 * 
	 * @return The methods defined inside of this method object.
	 * @author Lisa Detzler
	 */
	public HashMap<String, Method> getInnerMethods() {
		return innerMethods;
	}

	/**
	 * Adds the specified method to the set of methods that are defined inside
	 * of this method object.
	 * 
	 * @param The
	 *            methods to add to the set of methods defined inside of this
	 *            method object.
	 * @author Lisa Detzler
	 */
	public Method addInnerMethod(CodeLine innerMethod) {
		Method newMethod = new Method("", new CodeLine(), innerMethod,
				new CodeLine(), new CodeLineList(), relatedClass);
		this.innerMethods.put(innerMethod.toString(), newMethod);
		return newMethod;
	}

}
