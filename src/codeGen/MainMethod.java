/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

/**
 * Represents the method <tt>main</tt>.
 * 
 * @author Lisa Detzler
 * 
 */
public class MainMethod extends Method {

	/**
	 * Creates a new main method.
	 * 
	 * @param modifier
	 *            The modifier of the main method.
	 * @param returnType
	 *            The return type of the main method.
	 * @param name
	 *            The name of the main method.
	 * @param args
	 *            The arguments of the main method.
	 * @param code
	 *            The code of the method body.
	 * @param relatedClass
	 *            The class the method is defined in.
	 * 
	 * @author Lisa Detzler
	 */
	public MainMethod(String modifier, CodeLine returnType, CodeLine name,
			CodeLine args, CodeLineList code, Code relatedClass) {
		super(modifier, returnType, name, args, code, relatedClass);
		super.name = name;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public void setName(CodeLine name) {
		this.name = name;
	}

}
