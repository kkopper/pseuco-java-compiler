/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.io.File;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import tree.LDNode;

/**
 * Organizes the code generation process.
 * 
 * @author Lisa Detzler
 * 
 */
public class MainCodeGen {
	/**
	 * Defines the line separator.
	 * 
	 * @author Lisa Detzler
	 */
	public static String seperator = System.getProperties().getProperty(
			"file.separator");

	/**
	 * Defines the root node of the parsed tree.
	 * 
	 * @author Lisa Detzler
	 */
	public static LDNode rootNode;

	/**
	 * Specifies whether the generated Java code should be printed to the
	 * console at the end or not.
	 * 
	 * @author Lisa Detzler
	 */
	public static boolean output = true;

	/**
	 * Stores the generated Java code of the complete PseuCo source code.
	 * 
	 * @author Lisa Detzler
	 */
	public static String generatedCode;

	/**
	 * Stores the generated Java code of the complete PseuCo source code as code
	 * line list.
	 * 
	 * @author Lisa Detzler
	 */
	public static CodeLineList generatedCodeList;

	/**
	 * Specifies the name of the folder in which the generated Java files should
	 * be located.
	 * 
	 * @author Lisa Detzler
	 */
	public static String generatedJavaCodeFolder = getPlatformIndependentPath("include");

	/**
	 * Specifies the path of the folder in which the generated Java files should
	 * be stored - for testing only.
	 * 
	 * @author Lisa Detzler
	 */
	public static String generatedJavaCodeFolderPath = getPlatformIndependentPath(""
			+ generatedJavaCodeFolder);

	/**
	 * Specifies if the compiler is executed for the generation of Java code
	 * only or for the execution of the PseuCo source code.
	 * 
	 * @author Lisa Detzler
	 */
	public static boolean isJavaCodeGeneration = true;

	/**
	 * Specifies if the outputs should be short or long.
	 * 
	 * @author Lisa Detzler
	 */
	public static boolean shortOutputs = true;

	/**
	 * Specifies the version of the compiler.
	 * 
	 * @author Lisa Detzler
	 */
	public static String version = "2.3";

	/**
	 * Stores the files defined external.
	 * 
	 * @author Lisa Detzler
	 */
	public static LinkedList<String> listOfExternJavaFiles = new LinkedList<String>();

	/**
	 * Starts the code generation and execution process.
	 * 
	 * @param args
	 *            Not used.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * 
	 * @author Lisa Detzler
	 */
	public static void main(String[] args) throws IOException {
		MainCodeGen.initiateListOfExternJavaFiles();

		// Generates the Java code.
		CodeGen.startCodeGen(rootNode);

		CodeLineList mainClassCode = CodeGen.mainCodeObject.generateClassCode();

		// Writes the generates Java code to the files.
		MainCodeGen.deleteFiles(new File(generatedJavaCodeFolderPath));

		String fileName = generatedJavaCodeFolderPath + MainCodeGen.seperator
				+ CodeGen.mainClassName + ".java";
		MainCodeGen.writeToFile(mainClassCode, fileName, CodeGen.mainClassName);

		String monitorCode = MainCodeGen.writeCodeToFiles(CodeGen.monitors);
		String structCode = MainCodeGen.writeCodeToFiles(CodeGen.structs);

		// Maps the Java line numbers to the corresponding PseuCo ones.
		MainCodeGen.generateLineNumberMapping();
		if (!MainCodeGen.isJavaCodeGeneration) {
			MainCodeGen.generateLineNumberMapping();
		}

		// Prints the Code
		String code = mainClassCode + CodeGen.getLineSeparator()
				+ CodeGen.getLineSeparator() + monitorCode
				+ CodeGen.getLineSeparator() + CodeGen.getLineSeparator()
				+ structCode;

		generatedCode = code;
		if (output) {
			System.out.println(code);
		}
	}

	/**
	 * Maps the Java line numbers of the generated Java code line list to the
	 * corresponding PseuCo line numbers and stores the mapping into the file
	 * "Settings.java" such that it is accessible during the execution the
	 * generated Java code.
	 * 
	 * @author Lisa Detzler
	 */
	private static void generateLineNumberMapping() {

		// Generates the code to write to the file "Settings.java".
		String returnString = "public static HashMap<String, HashMap<Integer, HashSet<Integer>>> "
				+ "lineNumberMapping = new HashMap<String, HashMap<Integer, HashSet<Integer>>>();";
		String procCode = generateProcCode();
		returnString = returnString + procCode;
		returnString = CodeGen.shiftCode(CodeGen.getLineSeparator()
				+ returnString, 1);
		String before = "package " + MainCodeGen.generatedJavaCodeFolder + ";"
				+ CodeGen.getLineSeparator() + CodeGen.getLineSeparator()
				+ "import java.util.HashMap;" + CodeGen.getLineSeparator()
				+ "import java.util.LinkedList;" + CodeGen.getLineSeparator()
				+ "import java.util.HashSet;" + CodeGen.getLineSeparator()
				+ CodeGen.getLineSeparator() + "public class Settings {"
				+ CodeGen.getLineSeparator();
		returnString = before + returnString + CodeGen.getLineSeparator()
				+ CodeGen.getLineSeparator();
		returnString = returnString
				+ "    public static LinkedList<String> getListOfExternJavaFiles(){"
				+ CodeGen.getLineSeparator()
				+ "        LinkedList<String> list = new LinkedList<String>();"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Boolchan.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Intchan.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Stringchan.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"IntChannel.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"BoolChannel.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"StringChannel.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"IntHandshakeChan.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"BoolHandshakeChan.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"StringHandshakeChan.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"PseuCoThread.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Work.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"DefaultWork.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Message.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"WorkList.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Channel.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Simulate.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Handshake.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"ErrorHandling.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"Settings.java\");"
				+ CodeGen.getLineSeparator()
				+ "        list.add(\"CodeGenError.java\");"
				+ CodeGen.getLineSeparator() + "        return list;"
				+ CodeGen.getLineSeparator() + "    }"
				+ CodeGen.getLineSeparator();

		returnString = returnString
				+ "    public static LinkedList<Integer> getPseuCoLineNumber(int javaLineNumber,"
				+ CodeGen.getLineSeparator()
				+ "           String className, boolean onlyPositivNumbers) {"
				+ CodeGen.getLineSeparator()
				+ "        // copy of lineNumberMapping"
				+ CodeGen.getLineSeparator()
				+ "        HashMap<Integer, HashSet<Integer>> map2 = new HashMap<Integer, HashSet<Integer>>();"
				+ CodeGen.getLineSeparator()
				+ "        map2.putAll(lineNumberMapping.get(className));"
				+ CodeGen.getLineSeparator()
				+ "        LinkedList<Integer> returnList = new LinkedList<Integer>();"
				+ CodeGen.getLineSeparator()
				+ "        if (map2.isEmpty()) {"
				+ CodeGen.getLineSeparator()
				+ "            returnList.add(-1);"
				+ CodeGen.getLineSeparator()
				+ "             return returnList;"
				+ CodeGen.getLineSeparator()
				+ "        }"
				+ CodeGen.getLineSeparator()
				+ "        if (map2.containsKey(javaLineNumber)) {"
				+ CodeGen.getLineSeparator()
				+ "            HashSet<Integer> set = new HashSet<Integer>();"
				+ CodeGen.getLineSeparator()
				+ "            if (onlyPositivNumbers) {"
				+ CodeGen.getLineSeparator()
				+ "                // if list contains only negativ values, only return -1"
				+ CodeGen.getLineSeparator()
				+ "                if (set.contains(-1) && set.contains(-2) && set.size() == 2) {"
				+ CodeGen.getLineSeparator()
				+ "                    set.remove(-2);"
				+ CodeGen.getLineSeparator()
				+ "                }"
				+ CodeGen.getLineSeparator()
				+ "                // if list contains at least one postitiv value, remove all"
				+ CodeGen.getLineSeparator()
				+ "                // negativ ones"
				+ CodeGen.getLineSeparator()
				+ "                if (!(set.contains(-1) && set.size() == 1)"
				+ CodeGen.getLineSeparator()
				+ "		                 && !(set.contains(-2) && set.size() == 1)) {"
				+ CodeGen.getLineSeparator()
				+ "                    set.remove(-1);"
				+ CodeGen.getLineSeparator()
				+ "                    set.remove(-2);"
				+ CodeGen.getLineSeparator() + "                }"
				+ CodeGen.getLineSeparator() + "            }"
				+ CodeGen.getLineSeparator()
				+ "            returnList.addAll(map2.get(javaLineNumber));"
				+ CodeGen.getLineSeparator() + "        }"
				+ CodeGen.getLineSeparator() + "        return returnList;"
				+ CodeGen.getLineSeparator() + "    }"
				+ CodeGen.getLineSeparator() + "}";
		try {
			MainCodeGen.writeToFile(new CodeLineList(new CodeLine(returnString,
					-1)), MainCodeGen.generatedJavaCodeFolderPath
					+ MainCodeGen.seperator + "Settings.java", "Settings");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generates the code to initialize the mapping in the file "Settings.java".
	 * 
	 * @return The generated code.
	 * 
	 * @author Lisa Detzler
	 */
	private static String generateProcCode() {
		String returnString = CodeGen.getLineSeparator();
		returnString = returnString + CodeGen.getLineSeparator()
				+ "public static void initLineNumberMapping(){";
		String procCode = CodeGen.getLineSeparator()
				+ "HashMap<Integer, HashSet<Integer>> map2;";
		procCode += CodeGen.getLineSeparator() + "HashSet<Integer> set;";
		procCode += CodeGen.getLineSeparator();
		for (Map.Entry<String, HashMap<Integer, HashSet<Integer>>> map1 : CodeLineMapping
				.getMap().entrySet()) {
			procCode = procCode + generateMapInitString(map1);
			procCode = procCode + CodeGen.getLineSeparator()
					+ "lineNumberMapping.put(\"" + map1.getKey() + "\", map2);";
			procCode = procCode + CodeGen.getLineSeparator();
		}
		returnString = returnString + CodeGen.shiftCode(procCode, 1)
				+ CodeGen.getLineSeparator() + "}";
		return returnString;
	}

	/**
	 * Generates the code to initialize the map mapping Java line numbers to
	 * PseuCo ones.
	 * 
	 * @param map1
	 *            The map mapping Java file names to the corresponding mappings.
	 * @return The generated code.
	 * 
	 * @author Lisa Detzler
	 */
	private static String generateMapInitString(
			Map.Entry<String, HashMap<Integer, HashSet<Integer>>> map1) {
		String map2String = "map2 = new HashMap<Integer, HashSet<Integer>>();";
		for (Map.Entry<Integer, HashSet<Integer>> map2 : map1.getValue()
				.entrySet()) {
			map2String = map2String + generateInitSetString(map2);
			map2String = map2String + CodeGen.getLineSeparator();
			map2String = map2String + "map2.put(" + map2.getKey() + ", set);";
		}
		return CodeGen.getLineSeparator() + map2String;
	}

	/**
	 * Generates the code to initialize the set PseuCo line numbers.
	 * 
	 * @param map2
	 *            The map mapping mapping Java line numbers to PseuCo ones.
	 * @return The generated code.
	 * 
	 * @author Lisa Detzler
	 */
	private static String generateInitSetString(
			Map.Entry<Integer, HashSet<Integer>> map2) {
		String setString = "set = new HashSet<Integer>();";
		for (Integer set : map2.getValue()) {
			setString = setString + CodeGen.getLineSeparator();
			setString = setString + "set.add(" + set + ");";
		}
		return CodeGen.getLineSeparator() + setString;
	}

	/**
	 * Makes the specified path platform independent.
	 * 
	 * @param path
	 *            The platform dependent path.
	 * @return The platform independent path.
	 * @author Lisa Detzler
	 */
	public static String getPlatformIndependentPath(String path) {
		path = path.replace("/", seperator);
		path = path.replace("\\", seperator);
		return path;
	}

	/**
	 * Writes the code of specified classes (the values of the specified hash
	 * map) to the corresponding files.
	 * 
	 * @param code
	 *            The hash map storing the code classes.
	 * @return The code written to the files.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * 
	 * @author Lisa Detzler
	 */
	public static String writeCodeToFiles(HashMap<String, Code> code)
			throws IOException {
		CodeLineList structCode = new CodeLineList();
		String outputCode = "";
		for (Code s : code.values()) {
			structCode = s.generateClassCode();
			outputCode = outputCode + CodeGen.getLineSeparator()
					+ structCode.toString();
			String structName = s.getName().getCode();
			String fileName = MainCodeGen.generatedJavaCodeFolderPath
					+ MainCodeGen.seperator + structName + ".java";
			MainCodeGen.writeToFile(structCode, fileName, structName);
		}
		return outputCode;
	}

	/**
	 * Writes the given string to the given file. (Overrides the old content of
	 * the file.)
	 * 
	 * @param textList
	 *            List with string lines to write.
	 * @param filePath
	 *            Name of the target file.
	 * @param className
	 *            Name of the destination class.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * 
	 * @author Lisa Detzler
	 */
	public static void writeToFile(CodeLineList textList, String filePath,
			String className) throws IOException {

		String text = "";
		textList = new CodeLineList(
				new CodeLine(
						"/*******************************************************************************",
						-1))
				.addNewLine(
						new CodeLine(
								"* Copyright (c) 2013, Saarland University. All rights reserved.",
								-1))
				.addNewLine(new CodeLine("* Lisa Detzler", -1))
				.addNewLine(
						new CodeLine(
								"******************************************************************************/",
								-1)).addNewLine().addNewLine(textList);

		String fileName = filePath.replace(
				MainCodeGen.generatedJavaCodeFolderPath + "\\", "");
		if (!MainCodeGen.listOfExternJavaFiles.contains(fileName)) {
			text = textList.toStringWithLineNumberMapping(className);
		} else {
			text = textList.toString();
		}
		FileOutputStream stream = new FileOutputStream(filePath);
		try {
			for (int i = 0; i < text.length(); i++) {
				stream.write((byte) text.charAt(i));
			}
		} finally {
			stream.close();
		}
	}

	/**
	 * Deletes the file with the specified path.
	 * 
	 * @param path
	 *            The path of the file to delete.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * 
	 * @author Lisa Detzler
	 */
	public static void deleteFiles(File path) throws IOException {
		if (path.listFiles() != null) {
			for (File file : path.listFiles()) {
				String filePath = file.getPath();
				boolean delete = true;
				for (String s : listOfExternJavaFiles) {
					if (!s.equals("Main.java") && filePath.contains(s)) {
						delete = false;
						break;
					}
				}
				if (delete) {
					file.delete();
				}
			}
		}
	}

	/**
	 * Creates the classes of the files that are defined external.
	 * 
	 * @param dstFolder
	 *            The destination folder the files should be stored in.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * @author Lisa Detzler
	 */
	public static void createClassesOfExternalFiles(String dstFolder)
			throws IOException {
		for (String fileName : listOfExternJavaFiles) {
			File src1 = new File(MainCodeGen.generatedJavaCodeFolder
					+ MainCodeGen.seperator + fileName);
			File dst1 = new File(dstFolder + MainCodeGen.seperator + fileName);
			copyFile(src1, dst1);
		}

	}

	/**
	 * Copies the file with the specified source path to the specified
	 * destination path.
	 * 
	 * @param src
	 *            The source path of the file to copy.
	 * @param dst
	 *            The destination path.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * @author Lisa Detzler
	 */
	public static void copyFile(File src, File dst) throws IOException {
		Reader reader = null;
		CodeLineList text = new CodeLineList();
		try {
			reader = new FileReader(src);
			for (int c; (c = reader.read()) != -1;)
				text.addCodeToLastLine(new CodeLine(String.valueOf((char) c),
						-1));
		} finally {
			reader.close();
		}

		text = text.split(";")[1];
		text.addCodeAtTheBeginning(new CodeLine("package "
				+ CodeGen.mainPackageName, -2));

		writeToFile(text, dst.getPath(), dst.getName());
	}

	/**
	 * Throws an exception with the message that an invalid identifier was used.
	 * 
	 * @param s
	 *            The id of the identifier.
	 * @param line
	 *            The PseuCo line of the identifier.
	 * 
	 * @author Lisa Detzler
	 */
	public static void throwNoASCII(String s, int line) {
		if (!s.matches("^\\p{ASCII}*$")) {
			CodeGenError.invalidIdentifier(s, line);
			System.exit(-1);
		}
	}

	/**
	 * Reads the file with the specified name.
	 * 
	 * @param fileName
	 *            The name of the file to read.
	 * @return The code of the file.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * @author Lisa Detzler
	 */
	public static String readFile(String fileName) throws IOException {
		BufferedReader br = null;
		String file = "";
		try {
			br = new BufferedReader(new FileReader(new File(fileName)));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (file != "") {
					file += CodeGen.getLineSeparator();
				}
				file += line;
			}
		} finally {
			if (br != null) {
				br.close();
			}
		}
		return file;
	}

	/**
	 * Creates the list with all files that are external defined.
	 * 
	 * @author Lisa Detzler
	 */
	public static void initiateListOfExternJavaFiles() {
		listOfExternJavaFiles = new LinkedList<String>();

		listOfExternJavaFiles.add("Boolchan.java");
		listOfExternJavaFiles.add("Intchan.java");
		listOfExternJavaFiles.add("Stringchan.java");
		listOfExternJavaFiles.add("IntChannel.java");
		listOfExternJavaFiles.add("BoolChannel.java");
		listOfExternJavaFiles.add("StringChannel.java");
		listOfExternJavaFiles.add("IntHandshakeChan.java");
		listOfExternJavaFiles.add("BoolHandshakeChan.java");
		listOfExternJavaFiles.add("StringHandshakeChan.java");
		listOfExternJavaFiles.add("PseuCoThread.java");
		listOfExternJavaFiles.add("Work.java");
		listOfExternJavaFiles.add("DefaultWork.java");
		listOfExternJavaFiles.add("Message.java");
		listOfExternJavaFiles.add("WorkList.java");
		listOfExternJavaFiles.add("Channel.java");
		listOfExternJavaFiles.add("Simulate.java");
		listOfExternJavaFiles.add("Handshake.java");
		listOfExternJavaFiles.add("ErrorHandling.java");
		listOfExternJavaFiles.add("Settings.java");
		listOfExternJavaFiles.add("CodeGenError.java");
	}

}
