/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import tree.LDNode;

/**
 * Provides functionalities helpful for the code generation.
 * 
 * @author Lisa Detzler
 * 
 */
public class CodeGen {

	/**
	 * The package name of the generated Java class <tt>Main</tt>.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static String mainPackageName = MainCodeGen.generatedJavaCodeFolder;

	/**
	 * The name of the package in which the generated Java classes representing
	 * PseuCo's <tt>monitor</tt> structure are located.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static String monitorPackageName = MainCodeGen.generatedJavaCodeFolder;

	/**
	 * The name of the package in which the generated Java classes representing
	 * PseuCo's <tt>struct</tt> structure are located.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static String structPackageName = MainCodeGen.generatedJavaCodeFolder;

	/**
	 * The name of the <tt>Main</tt> class.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	final public static String mainClassName = "Main";

	/**
	 * The name of the monitor lock used when explicitly locking a monitor.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	final public static String monitorLockName = "monitorLock";

	/**
	 * The object organizing the code of the <tt>Main</tt> class.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static MainCode mainCodeObject = new MainCode();

	/**
	 * Stores the code of the generated Java classes representing PseuCo's
	 * <tt>monitor</tt> structure as values. The key is specified by the
	 * particular monitor names.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static HashMap<String, Code> monitors = new HashMap<String, Code>();

	/**
	 * Stores the code of the generated Java classes representing PseuCo's
	 * <tt>struct</tt> structure as values. The key is specified by the
	 * particular struct names.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static HashMap<String, Code> structs = new HashMap<String, Code>();

	/**
	 * Stores all variable names used in the generated code.
	 * 
	 * @author Lisa Detzler
	 * 
	 */
	public static HashSet<String> usedVariableNames = new HashSet<String>();

	/**
	 * Starts the CodeGeneration of the given tree.
	 * 
	 * @param tree
	 *            The root node of the parsed AST.
	 */
	public static void startCodeGen(LDNode node) {
		node.init(null, null);

		if (MainCodeGen.isJavaCodeGeneration) {
			node.organisedCodeGen(null, true);
		} else {
			node.organisedCodeGen(null, false);
		}
	}

	/**
	 * Returns the string representing a line separator.
	 * 
	 * @return Line sparator.
	 */
	public static String getLineSeparator() {
		return System.getProperty("line.separator");
	}

	/**
	 * Shifts the complete block one unit (4 spaces) to the left.
	 * 
	 * @param code
	 *            : The code to shift.
	 * @param number
	 *            : The number of the units to shift.
	 * @return The shifted String.
	 */
	public static String shiftCode(String code, int number) {
		for (int i = 0; i < number; i++) {
			code = code.replace(CodeGen.getLineSeparator(),
					CodeGen.getLineSeparator() + "    ");
		}
		return code;
	}

	/**
	 * Shifts the complete code one unit (4 spaces) to the left.
	 * 
	 * @param code
	 *            : The code to shift.
	 * @param number
	 *            : The number of the units to shift.
	 * @return The shifted <tt>CodeLineList</tt>.
	 */
	public static CodeLineList shiftCode(CodeLineList codeList) {
		int size = codeList.getNUmberOfElements();
		for (int n = 1; n < size; n++) {
			CodeLine s = codeList.getCodeLine(n);
			codeList.setCodeLine(new CodeLine("    ", -1).concatLast(s), n);
		}
		return codeList;
	}

	/**
	 * Generates the code of a Java method which initializes an array of the
	 * specified type. Adds the generates method to the specified class.
	 * 
	 * @param dim
	 *            The dimension of the array.
	 * @param typeLine
	 *            The type of the array.
	 * @param relatedClass
	 *            The class the generated method should be stored in.
	 */
	public static void handleInitMethodCode(int dim, CodeLine typeLine,
			Code relatedClass) {
		String type = typeLine.toString();
		String initBrackets = "";
		String initBracketsWithLength = "";
		for (int i = 0; i < dim; i++) {
			initBrackets = initBrackets + "[]";
			initBracketsWithLength = initBracketsWithLength + "[length[" + i
					+ "]]";
		}

		String sizeOfBufchan = "";
		if (type.equalsIgnoreCase("Intchan")
				|| type.equalsIgnoreCase("Boolchan")
				|| type.equalsIgnoreCase("Stringchan")) {
			sizeOfBufchan = ", int sizeOfBufchan";
		}

		String pseuCoType = type;

		if (needsPseuCoPrefix(pseuCoType)) {
			pseuCoType = "PseuCo_" + type;
		}

		CodeLineList code = new CodeLineList();
		code.addNewLine(new CodeLine(pseuCoType + initBrackets
				+ " arrayName = new " + pseuCoType + initBracketsWithLength
				+ ";", -2));
		code.concatLast(generateInitCode(dim, dim - 1, pseuCoType));
		code.addNewLine(new CodeLine("return arrayName;", -2));
		String args = "(int[] length" + sizeOfBufchan + ")";
		CodeGen.shiftCode(code);
		code.addCodeAtTheBeginning(new CodeLine("{", -2));
		code.addNewLine(new CodeLine("}", -2));

		relatedClass.addMethod(
				new CodeLine(type + initBrackets, typeLine
						.getPseuCoLineNumbers()), new CodeLine("initArray"
						+ type + dim, -2), new CodeLine(args, -2), code);
	}

	private static CodeLineList generateInitCode(int numberOfDim,
			int currentDim, String type) {
		CodeLineList code = new CodeLineList();
		code.addNewLine(new CodeLine("for(int i" + currentDim + " = 0; i"
				+ currentDim + " < length[" + currentDim + "]; i" + currentDim
				+ "++){", -2));
		if (currentDim > 0) {
			code.concatLast(CodeGen.shiftCode(generateInitCode(numberOfDim,
					currentDim - 1, type)));
			code.addNewLine(new CodeLine("}", -2));
		} else {
			String initBrackets = "";
			String initBracketsWithLength = "";
			for (int i = 0; i < numberOfDim; i++) {
				initBracketsWithLength = initBracketsWithLength + "[i" + i
						+ "]";
				initBrackets = initBrackets + "[]";
			}
			code.addNewLine(new CodeLine("    arrayName"
					+ initBracketsWithLength + " = ", -2));
			if (type.equalsIgnoreCase("Intchan")
					|| type.equalsIgnoreCase("Boolchan")
					|| type.equalsIgnoreCase("Stringchan")) {
				code.addCodeToLastLine(new CodeLine("new " + type
						+ "(sizeOfBufchan);", -2));
			} else {
				code.addCodeToLastLine(new CodeLine("new " + type + "();", -2));
			}
			code.addNewLine(new CodeLine("}", -2));
		}
		return code;
	}

	/**
	 * Derivates a name for the inner class in which the method with the
	 * specified name is located in.
	 * 
	 * @param innerMethodName
	 *            The inner method from which the inner class name should be
	 *            derivated.
	 * @return
	 */
	public static String getInnerClassObjName(String innerMethodName) {
		innerMethodName = innerMethodName.replace("PseuCo_", "");
		String s = innerMethodName.substring(1);
		String first = innerMethodName.substring(0, 1).toUpperCase();
		return "inner" + first + s;
	}

	/**
	 * Checks whether the specified class name needs the prefix "PseuCo_" or
	 * not. The prefix is needed if the class is defined by the user and thus
	 * not predefined.
	 * 
	 * @param className
	 *            The name of the class.
	 * @return <tt>true</tt> if the prefix "PseuCo_" is needed, <tt>false</tt>
	 *         otherwise.
	 */
	public static boolean needsPseuCoPrefix(String className) {
		if (className.startsWith("PseuCo_")) {
			return false;
		}
		if (className.contains("[")) {
			int i = className.indexOf("[");
			className = className.substring(0, i);
		}
		return !(className.equals("BoolHandshakeChan")
				|| className.equals("IntHandshakeChan")
				|| className.equals("StringHandshakeChan")
				|| className.equals("boolean") || className.equals("String")
				|| className.equals("int") || className.equals("Boolean")
				|| className.equals("Integer") || className.equals("Intchan")
				|| className.equals("Boolchan")
				|| className.equals("Stringchan")
				|| className.equals("IntChannel")
				|| className.equals("BoolChannel")
				|| className.equals("StringChannel")
				|| className.equals("PseuCoThread")
				|| className.equals("ReentrantLock") || className
					.equals("void"));
	}

	/**
	 * Replaces the specified identifier in the call code by the specified new one.
	 * 
	 * @param callCode
	 *            The callCode.
	 * @param oldVariableName
	 *            The identifier to replace.
	 * @param newVariableName
	 *            The new name for the identifier.
	 * @return Returns the call code after replacement.
	 */
	public static String replace(String callCode, String oldVariableName,
			String newVariableName) {

		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList("+",
				"-", "*", "/", ",", ")", "(", ".", "", " ", "{", "}", "[", "]",
				"%", "&", "|", "<", ">", "=", "?", "!", ":",
				System.getProperty("line.separator")));

		// For every appearance of the old identifier split the call code in
		// a prefix, the identifier and a postfix

		int lIndex1 = 0; // bounds of prefix
		int lIndex2;
		int rIndex1; // bounds of postfix
		int rIndex2 = callCode.length();
		int index = callCode.indexOf(oldVariableName); // index of the
														// identifier

		while (index != -1) { // there exists an occurance of the identifier
			// set the bounds
			rIndex2 = callCode.length();
			if (index > 0) {
				lIndex2 = index - 1;
			} else {
				lIndex2 = 0;
			}
			if (index < callCode.length()) {
				rIndex1 = index + oldVariableName.length();
			} else {
				rIndex1 = callCode.length();
			}

			String lChar;
			if (lIndex2 == 0) {
				lChar = "";
			} else {
				lChar = callCode.substring(lIndex2, lIndex2 + 1);
			}
			String rChar = callCode.substring(rIndex1, rIndex1 + 1);

			// proofs that the identifier is not a part of another one
			if (arrayList.contains(lChar) && arrayList.contains(rChar)) {
				String prefix;
				if (lIndex2 == 0) {
					prefix = "";
				} else {
					prefix = callCode.substring(lIndex1, lIndex2 + 1);
				}
				String postfix = callCode.substring(rIndex1, rIndex2);

				// removes the identifier's call "Main." if present
				if (lIndex2 - 4 >= 0) {
					String identifierBeforeVariable = callCode.substring(
							lIndex2 - 4, lIndex2 + 1);
					if (identifierBeforeVariable.equals(CodeGen.mainClassName
							+ ".")) {
						prefix = callCode.substring(lIndex1, lIndex2 - 4);
					}
				}
				callCode = prefix + newVariableName + postfix;
			}

			index = callCode.indexOf(oldVariableName, rIndex1);
		}
		return callCode;
	}
}
