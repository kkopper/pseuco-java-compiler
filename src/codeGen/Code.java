/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Represents the code of a generated Java class.
 * 
 * @author Lisa Detzler
 * 
 */
public abstract class Code {

	/**
	 * The name of the class represented by this Java class.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLine name = new CodeLine();

	/**
	 * The packages that must be imported in the code class.
	 * 
	 * @author Lisa Detzler
	 */
	protected CodeLineList imports = new CodeLineList();

	/**
	 * The declarations of the class represented by this Java class. For each
	 * declaration the hash map stores the declaration code as values together
	 * with the name of the declared variable as key.
	 * 
	 * @author Lisa Detzler
	 */
	protected HashMap<String, CodeLineList> declarations = new HashMap<String, CodeLineList>();

	/**
	 * The methods of the class represented by this Java class. For each method
	 * the hash map stores the data structure
	 * <tt>Method</tt> storing the code of the method as values. The keys store 
	 * the method name.
	 * 
	 * @author Lisa Detzler
	 */
	protected HashMap<String, Method> methods = new HashMap<String, Method>();

	/**
	 * A list of all declarations of the class represented by this Java class in
	 * the correct order.
	 * 
	 * @author Lisa Detzler
	 */
	protected ArrayList<String> declarationOrder = new ArrayList<String>();

	/**
	 * A list of all methods of the class represented by this Java class in the
	 * correct order.
	 * 
	 * @author Lisa Detzler
	 */
	protected ArrayList<String> methodOrder = new ArrayList<String>();

	/**
	 * Types the locking concepts of the class code.
	 * <tt>locksInsteadOfSynchronized</tt> is <tt>true</tt> if explicit locks 
	 * are used and <tt>false</tt> otherwise.
	 * 
	 * @author Lisa Detzler
	 */
	protected boolean locksInsteadOfSynchronized = true;

	/**
	 * Checks whether the class code locks using explicit locks.
	 * 
	 * @author Lisa Detzler
	 * @return <tt>true</tt>, if the code locks using explicit locks.
	 *         <tt>false</tt> if the code locks using implicit locks.
	 */
	public boolean isLocksInsteadOfSynchronized() {
		return locksInsteadOfSynchronized;
	}

	/**
	 * Specifies that the class code locks explicitly if
	 * <tt>locksInsteadOfSynchronized</tt> is <tt>true</tt>, otherwise
	 * the code is locked implicitly.
	 * 
	 * @param locksInsteadOfSynchronized
	 * @author Lisa Detzler
	 */
	public void setLocksInsteadOfSynchronized(boolean locksInsteadOfSynchronized) {
		if (locksInsteadOfSynchronized) {
			this.locksInsteadOfSynchronized = true;
		}
	}

	/**
	 * Returns a list with all declarations of the class code in the correct
	 * order.
	 * 
	 * @return The resultant list.
	 * @author Lisa Detzler
	 */
	public ArrayList<String> getDeclarationOrder() {
		return this.declarationOrder;
	}

	/**
	 * Returns a list with all methods of the class code in the correct order.
	 * 
	 * @return The resultant list.
	 * @author Lisa Detzler
	 */
	public ArrayList<String> getMethodOrder() {
		return this.methodOrder;
	}

	/**
	 * Returns a hash map storing all declarations of the class code as values
	 * together with the particular method name as key.
	 * 
	 * @return The resultant hash map.
	 * @author Lisa Detzler
	 */
	public HashMap<String, Method> getMethods() {
		return this.methods;
	}

	/**
	 * Returns the name of the class code with the prefix "PseuCo_".
	 * 
	 * @return The name of the class code with the prefix "PseuCo_".
	 * @author Lisa Detzler
	 */
	public CodeLine getName() {
		return new CodeLine("PseuCo_", -1).concatLast(name);
	}

	/**
	 * Sets the name of the class code to the specified argument.
	 * 
	 * @param The
	 *            new name of the class code.
	 * @author Lisa Detzler
	 */
	public void setName(CodeLine name) {
		this.name = name;
	}

	/**
	 * Returns a hash map storing the declarations of the class code as values
	 * together with the particular declaration name as key.
	 * 
	 * @return The resultant hash map.
	 * @author Lisa Detzler
	 */
	public HashMap<String, CodeLineList> getDeclarations() {
		return declarations;
	}

	/**
	 * Returns a
	 * <tt>CodeLineList</tt> storing the Java code responsible for the imports.
	 * 
	 * @return The resultant list.
	 * @author Lisa Detzler
	 */
	public CodeLineList getImportCode() {
		CodeLineList returnCode = new CodeLineList();
		if (this.imports.isEmpty()) {
			return null;
		}

		int size = this.imports.getNUmberOfElements();
		for (int i = 0; i < size; i++) {
			CodeLine importPackage = this.imports.getCodeLine(i);
			returnCode.addNewLine(new CodeLine("import ", -2))
					.addCodeToLastLine(importPackage)
					.addCodeToLastLine(new CodeLine(";", -1));
		}
		return returnCode;
	}

	/**
	 * Adds the specified package to the import packages of the class code.
	 * 
	 * @param importPackage
	 *            Specifies a
	 *            <tt>CodeLine</tt> containing a package name that must be imported.
	 * @author Lisa Detzler
	 */
	public void addImport(CodeLine importPackage) {
		if (!imports.contains(importPackage.getCode())) {
			if (imports.isEmpty()) {
				this.imports.addCodeToLastLine(importPackage);
			} else {
				this.imports.addNewLine(importPackage);
			}
		}
	}

	/**
	 * Returns a <tt>CodeLineList</tt> storing the Java code representing 
	 * the code of the methods of the class code.
	 * 
	 * @return The resultant list.
	 * @author Lisa Detzler
	 */
	protected CodeLineList getMethodsCode() {
		CodeLineList code = new CodeLineList();
		if (this.getMethods().size() > 0) {
			for (String s : this.getMethodOrder()) {
				CodeLineList sList = this.getMethods().get(s).getMethodCode();
				code.concatLast(sList);
			}
			code.addNewLine();
		}
		return code;
	}

	/**
	 * Returns a <tt>CodeLineList</tt> storing the Java code representing 
	 * the code of the declarations of the class code.
	 * 
	 * @return The resultant list.
	 * @author Lisa Detzler
	 */
	protected CodeLineList getDeclarationsCode() {
		CodeLineList code = new CodeLineList();
		if (this.getDeclarations().size() > 0) {
			for (String s : this.getDeclarationOrder()) {
				CodeLineList sList = this.getDeclarations().get(s);
				code.concatLast(sList);
			}
			code.addNewLine();
		}
		return code;
	}

	/**
	 * Adds the specified declaration to the declarations of the class code.
	 * 
	 * @param name
	 *            Specifies the name of the declared variable.
	 * @param decl
	 *            Specifies the code of the declaration.
	 * @author Lisa Detzler
	 */
	abstract public void addDeclaration(CodeLine name, CodeLineList decl);

	/**
	 * Adds the specified method to the methods of the class code.
	 * 
	 * @param returnType
	 *            Specifies the type of the method.
	 * @param name
	 *            Specifies the name of the method.
	 * @param args
	 *            Specifies the arguments of the method.
	 * @param code
	 *            Specifies the body code of the method.
	 * @return Returns the generated Method.
	 * @author Lisa Detzler
	 */
	abstract public Method addMethod(CodeLine returnType, CodeLine name,
			CodeLine args, CodeLineList code);

	/**
	 * Generates the code of the class code.
	 * 
	 * @return The generated code.
	 * @author Lisa Detzler
	 */
	abstract public CodeLineList generateClassCode();

}
