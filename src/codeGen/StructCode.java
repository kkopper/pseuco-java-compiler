/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

/**
 * Represents the generated Java class code corresponding to the PseuCo
 * <tt>struct</tt> structure.
 * 
 * @author Lisa Detzler
 * 
 */
public class StructCode extends Code {

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public void addDeclaration(CodeLine name, CodeLineList code) {
		super.declarations.put(name.toString(),
				new CodeLineList().addNewLine(new CodeLine("private ", -1))
						.concatLast(code));
		super.declarationOrder.add(name.toString());
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public Method addMethod(CodeLine returnType, CodeLine name, CodeLine args,
			CodeLineList code) {
		Method method = new Method("public", returnType, name, args, code, this);

		if (!this.methodOrder.contains(name.getCode())) {
			this.methods.put(name.toString(), method);
			this.methodOrder.add(name.toString());
		}
		return method;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateClassCode() {
		CodeLineList createPackage = new CodeLineList(new CodeLine("package "
				+ CodeGen.structPackageName + ";", -1));
		CodeLineList structCode = new CodeLineList();

		structCode.concatLast(this.getDeclarationsCode());
		structCode.concatLast(this.getMethodsCode());
		CodeGen.shiftCode(structCode);

		CodeLineList code = new CodeLineList().addNewLine().concatLast(
				this.getImportCode());
		code.addNewLine().addNewLine(
				new CodeLine(
						"public class " + this.getName().getCode() + " { ",
						this.getName().getPseuCoLineNumbers()));
		code.addNewLine().concatLast(structCode);
		code.addNewLine().addNewLine(new CodeLine("}", -1));
		return code.concatFirst(createPackage);
	}
}
