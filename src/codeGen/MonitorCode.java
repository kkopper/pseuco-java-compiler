/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.HashMap;

/**
 * Represents the generated Java class code corresponding to the PseuCo monitor
 * structure.
 * 
 * @author Lisa Detzler
 * 
 */
public class MonitorCode extends Code {

	/**
	 * Stores the conditions defined in the Java class. The key specifies the
	 * name of the condition, the value specifies its code.
	 * 
	 * @author Lisa Detzler
	 */
	protected HashMap<String, CodeLineList> conditions = new HashMap<String, CodeLineList>();

	/**
	 * Stores the code that has to be defined before a condition. The key
	 * specifies the the condition, the value specifies the code that has to be
	 * typed before the condition.
	 * 
	 * @author Lisa Detzler
	 */
	protected HashMap<String, CodeLineList> beforeCondition = new HashMap<String, CodeLineList>();

	/**
	 * Creates a new <tt>MonitorCode</tt> object representing the generated Java
	 * class code corresponding to the PseuCo monitor structure.
	 * 
	 * @author Lisa Detzler
	 */
	public MonitorCode() {
		this.locksInsteadOfSynchronized = false;
	}

	/**
	 * Returns the name of the monitor lock used when explicitly locking the
	 * monitor class.
	 * 
	 * @return The name of the monitor lock.
	 * 
	 * @author Lisa Detzler
	 */
	public String getMonitorLock() {
		return CodeGen.monitorLockName;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public void addDeclaration(CodeLine name, CodeLineList code) {
		super.declarations.put(name.toString(),
				new CodeLineList().addNewLine(new CodeLine("private ", -2))
						.concatLast(code));
		super.declarationOrder.add(name.toString());
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public Method addMethod(CodeLine returnType, CodeLine name, CodeLine args,
			CodeLineList code) {
		Method method;
		method = new Method("public", returnType, name, args, code, this);
		this.methods.put(name.toString(), method);
		if (!this.methodOrder.contains(name)) {
			this.methodOrder.add(name.toString());
		}
		return method;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateClassCode() {
		CodeLineList createPackage = new CodeLineList(new CodeLine("package "
				+ CodeGen.monitorPackageName + ";", -2));
		CodeLineList monitorCode = new CodeLineList();
		if (this.locksInsteadOfSynchronized) {
			monitorCode.addCodeToLastLine(new CodeLine(
					"    private final ReentrantLock " + this.getMonitorLock()
							+ " = new ReentrantLock();", -2));
		}
		monitorCode.concatLast(this.getDeclarationsCode());
		monitorCode.concatLast(this.getMethodsCode());
		CodeGen.shiftCode(monitorCode);

		CodeLineList code = new CodeLineList().addNewLine().concatLast(
				this.getImportCode());
		code.addNewLine().addNewLine(
				new CodeLine(
						"public class " + this.getName().getCode() + " { ",
						this.getName().getPseuCoLineNumbers()));
		code.addNewLine(monitorCode);
		code.addNewLine().addNewLine(new CodeLine("}", -1));
		return code.concatFirst(createPackage);
	}

	/**
	 * Checks whether the monitor class defines the specified condition.
	 * 
	 * @param condition
	 *            The condition name.
	 * @param procName
	 *            The name of the method the condition is used in.
	 * @return <tt>true</tt> if the monitor class defines a condition,
	 *         <tt>false</tt> otherwise.
	 * @author Lisa Detzler
	 */
	public boolean containsCondition(String condition, String procName) {
		return this.conditions.containsKey(condition + "." + procName);
	}

	/**
	 * Returns boolean expression of the specified condition.
	 * 
	 * @param condition
	 *            The condition name.
	 * @param procName
	 *            The name of the method the condition is used in.
	 * @return The boolean expession.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList getConditionsBoolean(String condition, String procName) {
		return this.conditions.get(condition + "." + procName);
	}

	/**
	 * Returns the code that has to by defined before the specified condition.
	 * 
	 * @param condition
	 *            The condition name.
	 * @param procName
	 *            The name of the method the condition is used in.
	 * @return The code that has to by defined before the specified condition.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList getBeforeCondition(String condition, String procName) {
		CodeLineList before = this.beforeCondition.get(condition + "."
				+ procName);
		return before;
	}

	/**
	 * Adds the specified condition to the method conditions.
	 * 
	 * @param condition
	 *            The condition name.
	 * @param procName
	 *            The name of the method the condition is used in.
	 * @param bool
	 *            The boolean expression of the condition.
	 * @param beforeCondition
	 *            The code that has to by defined before the specified
	 *            condition.
	 * 
	 * @author Lisa Detzler
	 */
	public void addCondition(String condition, String procName,
			CodeLineList bool, CodeLineList beforeCondition) {
		String[] op = { "<", ">", "=", "!", "+", "-", "*", "/", "||", "&&" };
		for (int i = 0; i < op.length; i++) {
			if (bool.contains(op[i])) {
				bool = bool.addCodeAtTheBeginning(new CodeLine("(", -1))
						.addCodeToLastLine(new CodeLine(")", -1));
				break;
			}
		}
		this.beforeCondition.put(condition + "." + procName, beforeCondition);
		this.conditions.put(condition + "." + procName, bool);
	}

}
