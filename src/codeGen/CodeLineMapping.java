/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Maps the Java code lines to the corresponding PseuCo lines.
 * 
 * @author Lisa Detzler
 */
public class CodeLineMapping {

	/**
	 * Stores for every generated Java class a hash map mapping the Java lines
	 * to the corresponding PseuCo ones. The names of the generated Java classes
	 * are stored as keys and the corresponding mappings as values.
	 * 
	 * <p>
	 * Every mapping stores the Java line number as key and the corresponding
	 * PseuCo line numbers as value. The PseuCo line number "-1" defines
	 * functional characters like brackets, semicolons etc. and the line number
	 * "-2" indicate an error or a problem in the compiler code.
	 * 
	 * @author Lisa Detzler
	 */
	private static HashMap<String, HashMap<Integer, HashSet<Integer>>> map = new HashMap<String, HashMap<Integer, HashSet<Integer>>>();

	/**
	 * Resets the map to a new one.
	 * 
	 * @author Lisa Detzler
	 */
	public static void init() {
		map = new HashMap<String, HashMap<Integer, HashSet<Integer>>>();
	}

	/**
	 * Returns the PseuCo line numbers referring to the specified Java line
	 * number of the generated Java class with the specified name. If
	 * <tt>onlyPositivNumbers</tt> is <tt>true</tt> only positive PseuCo line
	 * numbers are returned.
	 * 
	 * @param javaLineNumber
	 *            The Java line number the corresponding PseuCo line numbers are
	 *            in demand.
	 * @param className
	 *            The class name of the Java line number.
	 * @param onlyPositivNumbers
	 *            Specifies if both, positive and negative PseuCo line numbers
	 *            are returned of only the positive ones.
	 * @return A list with all corresponding PseuCo line numbers.
	 * 
	 * @author Lisa Detzler
	 */
	public static LinkedList<Integer> getPseuCoLineNumber(int javaLineNumber,
			String className, boolean onlyPositivNumbers) {
		// copy of map
		HashMap<Integer, HashSet<Integer>> map2 = new HashMap<Integer, HashSet<Integer>>();
		map2.putAll(map.get(className));

		LinkedList<Integer> returnList = new LinkedList<Integer>();
		if (map2.isEmpty()) {
			returnList.add(-1);
			return returnList;
		}
		if (map2.containsKey(javaLineNumber)) {
			HashSet<Integer> set = new HashSet<Integer>();
			if (onlyPositivNumbers) {
				// if list contains only negativ values, only return -1
				if (set.contains(-1) && set.contains(-2) && set.size() == 2) {
					set.remove(-2);
				}

				// if list contains at least one postitiv value, remove all
				// negativ ones
				if (!(set.contains(-1) && set.size() == 1)
						&& !(set.contains(-2) && set.size() == 1)) {
					set.remove(-1);
					set.remove(-2);
				}

			}
			returnList.addAll(map2.get(javaLineNumber));
		}
		return returnList;
	}

	/**
	 * Adds the line mapping to map that maps the specified Java line number to
	 * the specified PseuCo one and corresponds to the class with the specified
	 * class name.
	 * 
	 * @param javaLineNumber
	 * @param pseuCoLineNumber
	 * @param className
	 * 
	 * @author Lisa Detzler
	 */
	public static void addMapping(int javaLineNumber, int pseuCoLineNumber,
			String className) {
		if (map.containsKey(className)) {
			HashMap<Integer, HashSet<Integer>> littleMap = map
					.remove(className);
			if (littleMap.containsKey(javaLineNumber)) {
				HashSet<Integer> set = littleMap.get(javaLineNumber);
				set.add(pseuCoLineNumber);
			} else {
				HashSet<Integer> set = new HashSet<Integer>();
				set.add(pseuCoLineNumber);
				littleMap.put(javaLineNumber, set);
			}
			map.put(className, littleMap);
		} else {
			HashMap<Integer, HashSet<Integer>> map2 = new HashMap<Integer, HashSet<Integer>>();
			HashSet<Integer> set = new HashSet<Integer>();
			set.add(pseuCoLineNumber);
			map2.put(javaLineNumber, set);
			map.put(className, map2);
		}
	}

	/**
	 * Adds the line mapping to map that maps the specified Java line number to
	 * the specified PseuCo ones and corresponds to the class with the specified
	 * class name.
	 * 
	 * @param javaLineNumber
	 * @param pseuCoLineNumbers
	 * @param className
	 * 
	 * @author Lisa Detzler
	 */
	public static void addMapping(int javaLineNumber,
			HashSet<Integer> pseuCoLineNumbers, String className) {
		HashMap<Integer, HashSet<Integer>> map2 = new HashMap<Integer, HashSet<Integer>>();
		if (map.containsKey(className)) {
			map2.putAll(map.remove(className));
			if (map2.containsKey(javaLineNumber)) {
				map2.get(javaLineNumber).addAll(pseuCoLineNumbers);
			} else {
				map2.put(javaLineNumber, pseuCoLineNumbers);
			}
			map.put(className, map2);
		} else {
			map2.put(javaLineNumber, pseuCoLineNumbers);
			map.put(className, map2);
		}
	}

	/**
	 * Returns the size of the map.
	 * 
	 * @return The size of the map.
	 * 
	 * @author Lisa Detzler
	 */
	public static int size() {
		return map.size();
	}

	/**
	 * Returns the map.
	 * 
	 * @return The map object.
	 * 
	 * @author Lisa Detzler
	 */
	public static HashMap<String, HashMap<Integer, HashSet<Integer>>> getMap() {
		return map;
	}

}
