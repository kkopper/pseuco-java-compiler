/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.HashMap;

/**
 * Represents the generated Java class <tt>Main</tt>.
 * 
 * @author Lisa Detzler
 */
public class MainCode extends Code {

	/**
	 * Stores the code of the <tt>main</tt> method.
	 * 
	 * @author Lisa Detzler
	 */
	private Method mainMethod = new MainMethod("public static", new CodeLine(
			"void", -1), new CodeLine("main", -1), new CodeLine(
			"(String[] args)", -1), new CodeLineList(), CodeGen.mainCodeObject);

	/**
	 * Stores all methods that need to be defined in inner classes. The key
	 * defines the method name, the value the <tt>Method</tt> object.
	 * 
	 * @author Lisa Detzler
	 */
	private HashMap<String, Method> innerMethods = new HashMap<String, Method>();

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public void addDeclaration(CodeLine name, CodeLineList code) {
		this.declarations.put(
				name.toString(),
				(new CodeLineList()).addNewLine(
						new CodeLine("public static ", -1)).concatLast(code));
		this.declarationOrder.add(name.toString());
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public Method addMethod(CodeLine returnType, CodeLine name, CodeLine args,
			CodeLineList code) {
		Method method = new Method("public static", returnType, name, args,
				code, this);
		// name.addCodeBefore("PseuCo_");
		this.methods.put(name.toString(), method);
		if (!this.methodOrder.contains(name.toString())) {
			this.methodOrder.add(name.toString());
		}
		return method;
	}

	/**
	 * Returns the main method object.
	 * 
	 * @return The main method object.
	 * 
	 * @author Lisa Detzler
	 */
	public Method getMainMethod() {
		return mainMethod;
	}

	/**
	 * Sets the main method to a new object storing the specified code and name.
	 * 
	 * @param mainCode
	 *            The code of the new method.
	 * @param name
	 *            The name of the new method.
	 * 
	 * @author Lisa Detzler
	 */
	public void setMainMethod(CodeLineList mainCode, CodeLine name) {
		this.mainMethod = new MainMethod("public static", new CodeLine("void",
				-1), name, new CodeLine("(String[] args) ", -1), mainCode,
				CodeGen.mainCodeObject);
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public CodeLineList generateClassCode() {
		CodeLineList mainClassCode = new CodeLineList();

		mainClassCode.addNewLine(new CodeLine(
				"public static Object wildcard = new Object();", -2));
		mainClassCode
				.addNewLine(
						new CodeLine(
								"public static PseuCoThread thread = new PseuCoThread(\"mainAgentThread\");",
								-2)).concatLast(getDeclarationsCode());
		mainClassCode.concatLast(getMethodsCode());

		mainClassCode.addNewLine(this.getMainMethod().getMethodCode());

		// adds the header of the main class
		mainClassCode.addNewLine();
		// mainClassCode.concatLast(mainClassCode);
		mainClassCode = CodeGen.shiftCode(mainClassCode);

		CodeLineList createPackage = new CodeLineList(new CodeLine("package "
				+ CodeGen.mainPackageName + ";", -2));
		createPackage.addNewLine(this.getImportCode());
		CodeLineList mainClass = new CodeLineList(new CodeLine("public class "
				+ CodeGen.mainClassName + " { ", -2));
		mainClass.concatLast(mainClassCode);
		mainClass.addNewLine(new CodeLine("}", -2));
		createPackage.addNewLine();
		createPackage.addNewLine();
		createPackage.concatLast(mainClass);
		createPackage.replaceAll(CodeGen.mainClassName + ".", "");
		return createPackage;
	}

	/**
	 * Returns the methods that has to be defined in an inner class.
	 * 
	 * @return The methods that has to be defined in an inner class.
	 * 
	 * @author Lisa Detzler
	 */
	public HashMap<String, Method> getInnerMethods() {
		return innerMethods;
	}

	/**
	 * Adds an empty method with the specified name to the list of methods that
	 * has to be defined in an inner class.
	 * 
	 * @param innerMethod
	 *            The name of the method.
	 * @return The created method.
	 * 
	 * @author Lisa Detzler
	 */
	public Method addInnerMethod(CodeLine innerMethod) {
		Method newMethod = new Method("", new CodeLine(), innerMethod,
				new CodeLine(), new CodeLineList(), this);
		this.innerMethods.put(innerMethod.toString(), newMethod);
		return newMethod;
	}
}
