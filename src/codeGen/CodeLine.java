/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.HashSet;

/**
 * Represents a the code of a generates Java code line. Stores the Java code of
 * the line and the PseuCo line numbers referring to the Java code of the line.
 * 
 * @author Lisa Detzler
 * 
 */
public class CodeLine {

	/**
	 * The PseuCo line numbers the code of the Java line is referring to.
	 * 
	 * @author Lisa Detzler
	 */
	HashSet<Integer> pseuCoLineNumbers;

	/**
	 * The Java code of the line.
	 * 
	 * @author Lisa Detzler
	 */
	String code;

	/**
	 * Creates an empty <tt>CodeLine</tt> object.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLine() {
		this.code = "";
		this.pseuCoLineNumbers = new HashSet<Integer>();
	}

	/**
	 * Creates a new <tt>CodeLine</tt> object storing the specified code and
	 * referring to the specified PseuCo line numbers.
	 * 
	 * @param code
	 *            The code of the Java line.
	 * @param pseuCoLineNumbers
	 *            The PseuCo line numbers the code of the Java line is referring
	 *            to.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLine(String code, HashSet<Integer> pseuCoLineNumbers) {
		this.code = code;
		this.pseuCoLineNumbers = pseuCoLineNumbers;
	}

	/**
	 * Creates a new <tt>CodeLine</tt> object storing the specified code and
	 * referring to the specified PseuCo line number.
	 * 
	 * @param code
	 *            The code of the Java line.
	 * @param pseuCoLineNumber
	 *            The PseuCo line number the code of the Java line is referring
	 *            to.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLine(String code, int pseuCoLineNumber) {
		this.code = code;
		this.pseuCoLineNumbers = new HashSet<Integer>();
		if (pseuCoLineNumber >= 0) {
			this.pseuCoLineNumbers.add(pseuCoLineNumber);
		}
	}

	/**
	 * Sets the code of the Java line to the specified one.
	 * 
	 * @param code
	 *            The new code of the Java line.
	 * 
	 * @author Lisa Detzler
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Adds the specified code at the beginning of the Java line code.
	 * 
	 * @param code
	 *            The code to add at the beginning.
	 * 
	 * @author Lisa Detzler
	 */
	public void addCodeBefore(String code) {
		this.code = code + this.code;
	}

	/**
	 * Adds the specified code at the end of the Java line code.
	 * 
	 * @param code
	 *            The code to add at the end.
	 * @return The resultant Java line.
	 * @author Lisa Detzler
	 */
	public CodeLine addCode(String code) {
		this.code = this.code + code;
		return this;
	}

	/**
	 * Returns the code of the Java line.
	 * 
	 * @return The Java line code.
	 * 
	 * @author Lisa Detzler
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Returns the PseuCo line numbers the Java line is referring to.
	 * 
	 * @return The PseuCo line numbers of the line.
	 * 
	 * @author Lisa Detzler
	 */
	public HashSet<Integer> getPseuCoLineNumbers() {
		return pseuCoLineNumbers;
	}

	/**
	 * Adds the specified PseuCo line number to the line numbers the Java line
	 * code is referring to.
	 * 
	 * @param pseuCoLineNumber
	 *            The PseuCo line numebr to add.
	 * 
	 * @author Lisa Detzler
	 */
	public void addPseuCoLineNumber(int pseuCoLineNumber) {
		if (pseuCoLineNumber > 0) {
			this.pseuCoLineNumbers.add(pseuCoLineNumber);
		}
	}

	/**
	 * Adds the specified code line at the beginning of the Java line code.
	 * 
	 * @param code
	 *            The code line to add at the beginning.
	 * 
	 * @return The resultant Java line.
	 * @author Lisa Detzler
	 */
	public CodeLine concatFirst(CodeLine line) {
		this.code = line.getCode() + this.code;
		this.pseuCoLineNumbers.addAll(line.getPseuCoLineNumbers());
		return this;
	}

	/**
	 * Adds the specified code line at the end of the Java line code.
	 * 
	 * @param code
	 *            The code line to add at the end.
	 * @return The resultant Java line.
	 * @author Lisa Detzler
	 */
	public CodeLine concatLast(CodeLine line) {
		this.code = this.code + line.getCode();
		this.pseuCoLineNumbers.addAll(line.getPseuCoLineNumbers());
		return this;
	}

	/**
	 * Creates a duplicate of the Java line.
	 * 
	 * @return The copied Java line.
	 * @author Lisa Detzler
	 */
	public CodeLine copyLine() {
		HashSet<Integer> pseuCoLineNumbersCopy = new HashSet<Integer>();
		pseuCoLineNumbersCopy.addAll(this.pseuCoLineNumbers);
		CodeLine line = new CodeLine(this.getCode(), pseuCoLineNumbersCopy);
		return line;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public String toString() {
		return this.getCode();
	}

}
