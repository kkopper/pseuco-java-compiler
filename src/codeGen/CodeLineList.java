/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Represents a list of Java code lines.
 * 
 * @author Lisa Detzler
 */
public class CodeLineList {

	/**
	 * The list with the Java code lines. <br>
	 * code1 | code2 | code3 | ... <br>
	 * Every '|' represents a line separator. This means concatenating to lists
	 * together looks as follows: <br>
	 * (c1 | c2 | c3) @ (c4 | c5 | c6) -> c1 | c2 | c3 + c4 | c5 | c6
	 * 
	 * @author Lisa Detzler
	 */
	private LinkedList<CodeLine> codeLineList;

	/**
	 * Creates an empty list of code lines.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList() {
		codeLineList = new LinkedList<CodeLine>();
		addCodeToLastLine(new CodeLine());
	}

	/**
	 * Creates a new list of code lines containing the specified code line.
	 * 
	 * @param code
	 *            The code line to initiate the created list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList(CodeLine code) {
		this.codeLineList = new LinkedList<CodeLine>();
		this.addCodeToLastLine(code);
	}

	/**
	 * Creates a new list of code lines containing the code line of the
	 * specified list.
	 * 
	 * @param codeList
	 *            The list of code lines to initiate the created list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList(LinkedList<CodeLine> codeList) {
		this.codeLineList = codeList;
	}

	/**
	 * Returns the list of code lines.
	 * 
	 * @return The list of code lines.
	 * 
	 * @author Lisa Detzler
	 */
	public LinkedList<CodeLine> getCodeLineList() {
		return this.codeLineList;
	}

	/**
	 * Adds the specified code line to the last element of the code line list.
	 * 
	 * @param code
	 *            The code line to add.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList addCodeToLastLine(CodeLine code) {
		CodeLine last = codeLineList.pollLast();
		if (last == null) {
			last = code;
		} else {
			last.concatLast(code);
		}
		codeLineList.addLast(last);
		return this;
	}

	/**
	 * Adds the specified code line to the last element of the code line list.
	 * 
	 * @param code
	 *            The code to add.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList addCodeAtTheBeginning(CodeLine code) {
		codeLineList.set(0, code.concatLast(codeLineList.getFirst()));
		return this;
	}

	/**
	 * Concatenated the specified list at the end of the code line list.
	 * 
	 * @example Let '|' represent a line separator and c1,..., c6 be code lines.
	 *          Then the concatenation of list (c1 | c2 | c3) and (c4 | c5 | c6)
	 *          looks as follows: <br>
	 *          (c1 | c2 | c3) @ (c4 | c5 | c6) -> c1 | c2 | c3 + c4 | c5 | c6
	 *          where "+" represents the concatenation of <tt>CodeLine</tt>.
	 * 
	 * @param codeList
	 *            The code line list to add.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList concatLast(CodeLineList codeList) {
		if (codeList == null || codeList.getNUmberOfElements() == 0) {
			return this;
		}
		CodeLine line = codeList.getCodeLine(0);
		String code = line.getCode();
		this.addCodeToLastLine(new CodeLine(code, line.getPseuCoLineNumbers()));

		int size = codeList.getNUmberOfElements();
		for (int i = 1; i < size; i++) {
			addNewLine(codeList.getCodeLine(i));
		}
		return this;
	}

	/**
	 * Concatenated the specified list at the beginning of the code line list.
	 * 
	 * @example Let '|' represent a line separator and c1,..., c6 be code lines.
	 *          Then the concatenation of list (c1 | c2 | c3) and (c4 | c5 | c6)
	 *          looks as follows: <br>
	 *          (c1 | c2 | c3) @ (c4 | c5 | c6) -> c1 | c2 | c3 + c4 | c5 | c6
	 *          where "+" represents the concatenation of <tt>CodeLine</tt>.
	 * 
	 * @param codeList
	 *            The code line list to add.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList concatFirst(CodeLineList codeList) {
		if (codeList.getNUmberOfElements() == 0) {
			return this;
		}
		CodeLine line = codeList.getCodeLine(0);
		String code = line.getCode();
		addCodeAtTheBeginning(new CodeLine(code, line.getPseuCoLineNumbers()));

		int size = codeList.getNUmberOfElements();
		for (int i = 1; i < size; i++) {
			insertNewCodeLine(codeList.getCodeLine(i), 0);
		}
		return this;
	}

	/**
	 * Adds a new code line at the end of the list containing the specified code
	 * line.
	 * 
	 * @param codeLine
	 *            The new code line.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList addNewLine(CodeLine codeLine) {
		codeLineList.addLast(codeLine);
		return this;
	}

	/**
	 * Adds an empty code line at the end of the List.
	 * 
	 * @return The resulting list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList addNewLine() {
		codeLineList.addLast(new CodeLine());
		return this;
	}

	/**
	 * Adds a new Line at the end of the List and concatenates the specified
	 * list at the end of the resultant list.
	 * 
	 * @param codeLine
	 *            The list to concatenate.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList addNewLine(CodeLineList codeList) {
		addNewLine(new CodeLine());
		concatLast(codeList);
		return this;
	}

	/**
	 * Resets the line of the list with the specified index.
	 * 
	 * @param newCodeLine
	 *            The new code line.
	 * @param lineNumber
	 *            The index of the line.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList setCodeLine(CodeLine newCodeLine, int lineNumber) {
		codeLineList.set(lineNumber, newCodeLine);
		return this;
	}

	/**
	 * Returns the code of the line with the specified index.
	 * 
	 * @param lineNumber
	 *            The index of line.
	 * @return The code of the line with the specified index.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLine getCodeLine(int lineNumber) {
		if (codeLineList.size() > lineNumber) {
			return codeLineList.get(lineNumber);
		} else {
			return null;
		}
	}

	/**
	 * Insets a new code line to the list.
	 * 
	 * @param code
	 *            The new code line.
	 * @param lineNumber
	 *            The list index at which the new line should be inserted.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList insertNewCodeLine(CodeLine code, int lineNumber) {
		LinkedList<CodeLine> returnList = new LinkedList<CodeLine>();
		int i = 0;
		if (lineNumber > i) {
			for (i = 0; i < lineNumber; i++) {
				returnList.addLast(codeLineList.get(i));
			}
			returnList.get(i).concatLast(code);
		} else {
			returnList.addLast(code);
		}
		for (i = lineNumber; i < codeLineList.size(); i++) {
			returnList.addLast(codeLineList.get(i));
		}
		codeLineList = returnList;
		return this;
	}

	/**
	 * Removes the code line at the specified index from the list.
	 * 
	 * @param index
	 *            The index of the line to remove.
	 * 
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList removeCodeLine(int index) {
		codeLineList.remove(index);
		return this;
	}

	/**
	 * Returns the number of code lines of the list.
	 * 
	 * @return The size of the list.
	 * 
	 * @author Lisa Detzler
	 */
	public int getNUmberOfElements() {
		return codeLineList.size();
	}

	/**
	 * Returns the length of the code stored in the list.
	 * 
	 * @return The length of the code stored in the list.
	 * 
	 * @author Lisa Detzler
	 */
	public int getCodeSize() {
		int size = 0;
		for (CodeLine line : codeLineList) {
			size = size + line.getCode().length();
		}
		return size;
	}

	/**
	 * Replaces all occurrences of the old string by the new one.
	 * 
	 * @param oldStr
	 *            The string to replace.
	 * @param newStr
	 *            The new string.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList replaceAll(String oldStr, String newStr) {
		int n = codeLineList.size();
		for (int i = 0; i < n; i++) {
			CodeLine line = codeLineList.get(i);
			codeLineList.set(
					i,
					new CodeLine(line.getCode().replace(oldStr, newStr), line
							.getPseuCoLineNumbers()));
		}
		return this;
	}

	/**
	 * Replaces all occurrences of the specified string by the specified code
	 * lines.
	 * 
	 * @param oldStr
	 *            The string to replace.
	 * @param newStr
	 *            The code line list the old string should be replaced with.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList replaceAll(String oldStr, CodeLineList newStr) {
		CodeLineList newStrCopy = newStr.copyList();

		int n = codeLineList.size();
		for (int i = 0; i < n; i++) {
			CodeLine line = codeLineList.get(i);
			if (line.getCode().contains(oldStr)) {
				CodeLineList[] splittedArray = this.split(oldStr);
				CodeLineList left = splittedArray[0];
				CodeLineList right = splittedArray[1];
				right = right.substring(oldStr.length(), right.getCodeSize());
				right.replaceAll(oldStr, newStr);

				CodeLineList middle = newStrCopy;
				this.codeLineList = left.concatLast(middle).concatLast(right)
						.getCodeLineList();
				return this;

			}
		}
		return this;
	}

	/**
	 * Replaces the first occurrence of the old string by the new one.
	 * 
	 * @param oldStr
	 *            The string to replace.
	 * @param newStr
	 *            The new string.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList replaceFirst(String oldStr, String newStr) {
		int n = codeLineList.size();
		for (int i = 0; i < n; i++) {
			CodeLine line = codeLineList.get(i);
			if (line.getCode().contains(oldStr)) {
				codeLineList.set(i,
						new CodeLine(line.getCode().replace(oldStr, newStr),
								line.getPseuCoLineNumbers()));
				return this;
			}
		}
		return this;
	}

	/**
	 * Replaces the last occurrences of the old string by the new one.
	 * 
	 * @param oldStr
	 *            The string to replace.
	 * @param newStr
	 *            The new string.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList replaceLast(String oldStr, String newStr) {
		int n = codeLineList.size();
		int lastIndex = 0;
		for (int i = 0; i < n; i++) {
			CodeLine line = codeLineList.get(i);
			if (line.getCode().contains(oldStr)) {
				lastIndex = i;
			}
		}
		CodeLine line = codeLineList.get(lastIndex);
		int index = line.getCode().lastIndexOf(oldStr);
		line.setCode(line.getCode().substring(0, index) + newStr
				+ line.getCode().substring(index + oldStr.length() + 1, n));
		return this;
	}

	/**
	 * Replaces the occurrences of the old string by the new one if the
	 * replacement is possible.
	 * 
	 * @param oldStr
	 *            The string to replace.
	 * @param newStr
	 *            The new string.
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList replaceIfPossible(String oldStr, String newStr) {
		int n = codeLineList.size();
		for (int i = 0; i < n; i++) {
			CodeLine line = codeLineList.get(i);
			if (line.getCode().contains(oldStr)) {
				codeLineList.set(i,
						new CodeLine(line.getCode().replace(oldStr, newStr),
								line.getPseuCoLineNumbers()));
			}
		}
		return this;
	}

	/**
	 * Replaces the given identifier in the call code by the given new one.
	 * 
	 * @param callCode
	 *            The callCode.
	 * @param oldVariableName
	 *            The identifier to replace.
	 * @param newVariableName
	 *            The new name for the identifier.
	 * @return Returns the call code after replacement.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList replaceFinalIdentifiers(String oldVariableName,
			String newVariableName) {
		CodeLineList callCode = this.copyList();
		this.codeLineList = new LinkedList<CodeLine>();
		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList("+",
				"-", "*", "/", ",", ")", "(", ".", "", " ", "{", "}", "[", "]",
				"%", "&", "|", "<", ">", "=", "?", "!", ":",
				CodeGen.getLineSeparator()));

		// For every appearance of the old identifier split the call code in
		// a prefix, the identifier and a postfix

		int lIndex1 = 0; // bounds of prefix
		int lIndex2;
		int rIndex1; // bounds of postfix
		int rIndex2;
		int index = callCode.indexOf(oldVariableName); // index of the
														// identifier

		while (index != -1) { // there exists an occurance of the identifier
			// set the bounds
			rIndex2 = callCode.getCodeSize();
			if (index > 0) {
				lIndex2 = index - 1;
			} else {
				lIndex2 = 0;
			}
			if (index < callCode.getCodeSize()) {
				rIndex1 = index + oldVariableName.length();
			} else {
				rIndex1 = callCode.getCodeSize();
			}

			CodeLineList lChar;
			if (lIndex2 == 0) {
				lChar = new CodeLineList();
			} else {
				lChar = callCode.substring(lIndex2, lIndex2 + 1);
			}
			CodeLineList rChar = callCode.substring(rIndex1, rIndex1 + 1);
			// proofs that the identifier is not a part of another one
			if (arrayList.contains(lChar.toString())
					&& arrayList.contains(rChar.toString())) {
				CodeLineList prefix;
				if (lIndex2 == 0) {
					prefix = new CodeLineList();
				} else {
					prefix = callCode.substring(lIndex1, lIndex2 + 1);
				}
				CodeLineList postfix = callCode.substring(rIndex1, rIndex2);

				// removes the identifier's call "Main." if present
				prefix = removeMainCall(callCode, lIndex1, lIndex2, prefix);

				this.concatLast(prefix.copyList())
						.addCodeToLastLine(new CodeLine(newVariableName, -1))
						.getCodeLineList();
				callCode = new CodeLineList().concatLast(prefix)
						.addCodeToLastLine(new CodeLine(newVariableName, -1))
						.concatLast(postfix);
				rIndex1 = prefix.getCodeSize() + newVariableName.length();
			} else {
				rIndex1 = index + 1;
				this.concatLast(callCode.substring(0, rIndex1));
			}
			callCode = callCode.substring(rIndex1, callCode.getCodeSize());
			index = callCode.indexOf(oldVariableName);
			if (index == -1) {
				this.concatLast(callCode);
			}
		}
		return this;
	}

	private CodeLineList removeMainCall(CodeLineList callCode, int lIndex1,
			int lIndex2, CodeLineList prefix) {
		if (lIndex2 - 4 >= 0) {
			CodeLineList identifierBeforeVariable = callCode.copyList()
					.substring(lIndex2 - 4, lIndex2 + 1);
			if (identifierBeforeVariable.toString().equals(
					CodeGen.mainClassName + ".")) {
				prefix = callCode.copyList().substring(lIndex1, lIndex2 - 4);
			}
		}
		return prefix;
	}

	/**
	 * Splits a copy of the list in two parts. The first list contains the
	 * substring from index 0 to the index of s exclusively, the second list the
	 * substring from the index of s to the end of the list starting with s.
	 * 
	 * @param s
	 *            The list will be splitted at the first occurence of the input
	 *            string s.
	 * @return The splitted List.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList[] split(String s) {
		CodeLineList codeLineListCopy = this.copyList();
		LinkedList<CodeLine> codeLineList = codeLineListCopy.getCodeLineList();

		int size = codeLineList.size();
		for (int i = 0; i < size; i++) {
			CodeLine line = codeLineList.get(i);
			String code = line.getCode();
			if (code.contains(s)) {
				int index = code.indexOf(s);
				String leftStr = code.substring(0, index);
				String rightStr = code.substring(index);

				LinkedList<CodeLine> leftList = new LinkedList<CodeLine>();
				for (int n = 0; n < i; n++) {
					leftList.add(codeLineList.get(n));
				}
				leftList.add(new CodeLine(leftStr, line.getPseuCoLineNumbers()));

				LinkedList<CodeLine> rightList = new LinkedList<CodeLine>();
				for (int n = i + 1; n < codeLineList.size(); n++) {
					rightList.add(codeLineList.get(n));
				}
				rightList.add(0,
						new CodeLine(rightStr, line.getPseuCoLineNumbers()));

				CodeLineList[] array = { new CodeLineList(leftList),
						new CodeLineList(rightList) };
				return array;
			}
		}
		CodeLineList[] array = { codeLineListCopy };
		return array;
	}

	/**
	 * Returns a new <tt>CodeLineList</tt> whose code is a substring of the code
	 * of this <tt>CodeLineList</tt>. The substring begins with the character at
	 * the last index of the specified string and ends with the last character.
	 * 
	 * @param s
	 *            After the last occurrence of the string <tt>s</tt> the list is
	 *            splitted.
	 * @return The new list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList lastSubstring(String s) {
		CodeLineList codeLineListCopy = this.copyList();
		LinkedList<CodeLine> codeLineList = codeLineListCopy.getCodeLineList();

		int size = codeLineList.size();
		codeLineListCopy = this.copyList();
		for (int i = 0; i < size; i++) {
			CodeLine line = codeLineList.get(i).copyLine();
			String code = line.getCode();
			if (code.contains(s)) {
				int index = code.lastIndexOf(s);
				String rightStr = code.substring(index + 1);

				LinkedList<CodeLine> rightList = codeLineListCopy
						.getCodeLineList();
				rightList.subList(index, rightList.size()).add(0,
						new CodeLine(rightStr, line.getPseuCoLineNumbers()));
				codeLineListCopy = new CodeLineList(rightList);
			}
		}
		return codeLineListCopy;
	}

	/**
	 * Returns a new <tt>CodeLineList</tt> whose code is a substring of the code
	 * of this <tt>CodeLineList</tt>. The substring begins with the character at
	 * the specified start index and ends with the character of the specified
	 * end index.
	 * 
	 * @param beginIndex
	 *            The start index of the substring.
	 * @param endIndex
	 *            The end index of the substring.
	 * 
	 * @return The new list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList substring(int beginIndex, int endIndex) {
		CodeLineList codeLineListCopy = this.copyList();
		LinkedList<CodeLine> codeLineList = codeLineListCopy.getCodeLineList();

		int i = 0;
		int index = 0;
		CodeLineList substringList = new CodeLineList();
		// search begin index
		while (index < codeLineList.size()) {
			CodeLine line = codeLineList.get(index);
			String code = line.getCode();
			if (i + code.length() >= beginIndex) {

				if (i + code.length() >= endIndex) {
					code = code.substring(beginIndex - i, endIndex - i);
				} else {
					code = code.substring(beginIndex - i);
				}
				substringList.addCodeToLastLine(new CodeLine(code, line
						.getPseuCoLineNumbers()));
				index++;
				break;
			}
			i = i + code.length();
			index++;
		}

		// search end index
		while (index < codeLineList.size()) {
			CodeLine line = codeLineList.get(index);
			String code = line.getCode();
			if (i + code.length() < endIndex) {
				substringList.addNewLine(new CodeLine(code, line
						.getPseuCoLineNumbers()));
				i = i + code.length();
			} else {
				code = code.substring(0, endIndex - i);
				substringList.addNewLine(new CodeLine(code, line
						.getPseuCoLineNumbers()));
				break;
			}
			index++;
		}
		return substringList;
	}

	/**
	 * Checks whether the code of the list contains the specified string.
	 * 
	 * @param s
	 *            The string to check.
	 * @return <tt>true</tt> if the specified string is contained in the code of
	 *         the list, <tt>false</tt> otherwise.
	 * 
	 * @author Lisa Detzler
	 */
	public boolean contains(String s) {
		for (CodeLine line : codeLineList) {
			if (line.getCode().contains(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates a new <tt>HashSet</tt> containing all PseuCo line numbers of the
	 * code list by combining the HashSets of every code line.
	 * 
	 * @return The created <tt>HashSet</tt>.
	 * 
	 * @author Lisa Detzler
	 */
	public HashSet<Integer> combineAllPseuCoSets() {
		HashSet<Integer> set = new HashSet<Integer>();
		for (CodeLine line : codeLineList) {
			set.addAll(line.getPseuCoLineNumbers());
		}
		return set;
	}

	/**
	 * Checks whether the code of the list is empty.
	 * 
	 * @return <tt>true</tt> if the code of the list is empty, <tt>false</tt>
	 *         otherwise.
	 * 
	 * @author Lisa Detzler
	 */
	public boolean isEmpty() {
		for (CodeLine line : codeLineList) {
			if (!line.getCode().equals("")) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Creates a duplicate of the list.
	 * 
	 * @return The copied list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList copyList() {
		CodeLineList listCopy = new CodeLineList();
		for (CodeLine c : codeLineList) {
			HashSet<Integer> pseuCoLineNumbersCopy = new HashSet<Integer>();
			pseuCoLineNumbersCopy.addAll(c.getPseuCoLineNumbers());
			if (listCopy.isEmpty()) {
				listCopy.getCodeLineList().set(0,
						new CodeLine(c.getCode(), pseuCoLineNumbersCopy));
			} else {
				listCopy.getCodeLineList().addLast(
						new CodeLine(c.getCode(), pseuCoLineNumbersCopy));
			}
		}
		return listCopy;
	}

	/**
	 * Handles unreachable code of loops of the list code by surrounding the
	 * statements <tt>continue</tt>, <tt>return</tt> and <tt>break</tt> with
	 * <tt>if(true){</tt> and <tt>}</tt>.
	 * 
	 * @param addBeforeContinue
	 *            The code which has to be added before the statements
	 *            <tt>continue</tt>, <tt>return</tt> and <tt>break</tt>.
	 * 
	 * @return The resultant list.
	 * 
	 * @author Lisa Detzler
	 */
	public CodeLineList handleUnreachableCodeInLoop(
			CodeLineList addBeforeContinue) {
		CodeLineList addBeforeContinueCopy = addBeforeContinue.copyList();

		CodeGen.shiftCode(addBeforeContinueCopy);

		// continue
		if (this.contains("continue;")) {
			CodeLineList list = addBeforeContinueCopy.copyList();
			list.addCodeToLastLine(new CodeLine(";", -1));
			list.addNewLine(new CodeLine("continue", -1));

			this.replaceAll("continue", list);
			this.replaceAll("continue;", "    if(true){continue;}");
		}

		// return and break
		if (this.contains("return;")) {
			this.replaceAll("return;", "if(true){return;}");
		}
		if (this.contains("break;")) {
			this.replaceAll("break;", "if(true){break;}");
		}

		// update condition
		addBeforeContinueCopy.addCodeAtTheBeginning(new CodeLine("    ", -1));
		this.getCodeLineList().removeLast();
		this.addCodeToLastLine(new CodeLine("        ", -1)).addNewLine()
				.concatLast(addBeforeContinueCopy);
		this.addCodeToLastLine(new CodeLine(";", -1));
		this.addNewLine(new CodeLine("}", -1));

		return this;
	}

	/**
	 * Computes the index of the first occurrence of the input string.
	 * 
	 * @example For the list ["ab","cde","f"] the method returns the index 4 for
	 *          input "e" .
	 * 
	 * @param string
	 *            The string for that the index is wanted.
	 * @return The index of the first occurrence of the input string.
	 * 
	 * @author Lisa Detzler
	 */
	public int indexOf(String string) {
		int index = -1;
		for (CodeLine line : codeLineList) {
			if (line.getCode().contains(string)) {
				index = index + line.getCode().indexOf(string);
				break;
			} else {
				index = index + line.getCode().length();
			}
		}
		if (index != -1) {
			index++;
		}
		if (index >= this.getCodeSize()) {
			index = -1;
		}
		return index;
	}

	/**
	 * Computes the index of the last occurrence of the input string.
	 * 
	 * @example For the list ["ab","cde","f"] the method returns the index 4 for
	 *          input "e" .
	 * 
	 * @param string
	 *            The string for that the index is wanted.
	 * @return The index of the last occurrence of the input string.
	 * 
	 * @author Lisa Detzler
	 */
	public int lastIndexOf(String string) {
		int index = -1;
		for (CodeLine line : codeLineList) {
			if (line.getCode().contains(string)) {
				index = index + line.getCode().lastIndexOf(string);
			} else {
				index = index + line.getCode().length();
			}
		}
		if (index != -1) {
			index++;
		}
		return index;
	}

	/**
	 * Returns the code of the list as string and adds the resulting Java and
	 * PseuCo line references to the line number mapping.
	 * 
	 * @param fileName
	 *            The name of the file the code of the list is located in.
	 * @return The computed code of the list.
	 * 
	 * @author Lisa Detzler
	 */
	public String toStringWithLineNumberMapping(String fileName) {

		CodeLine firstLine = codeLineList.getFirst();
		String code = firstLine.getCode();
		CodeLineMapping.addMapping(1, firstLine.getPseuCoLineNumbers(),
				fileName);
		int size = codeLineList.size();
		for (int i = 1; i < size; i++) {
			CodeLine line = codeLineList.get(i);
			CodeLineMapping.addMapping(i + 1, line.getPseuCoLineNumbers(),
					fileName);
			code = code + CodeGen.getLineSeparator() + line.getCode();
		}
		return code;
	}

	/**
	 * @author Lisa Detzler
	 */
	@Override
	public String toString() {
		String code = codeLineList.getFirst().getCode();

		int size = codeLineList.size();
		for (int i = 1; i < size; i++) {
			code = code + CodeGen.getLineSeparator()
					+ codeLineList.get(i).getCode();
		}
		return code;
	}

}
