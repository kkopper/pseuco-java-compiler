/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGenTests;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;

import codeGen.Code;
import codeGen.CodeGen;
import codeGen.CodeLineMapping;
import codeGen.MainCode;
import codeGen.MainCodeGen;

import tree.ParseException;

/**
 * Tests the code generation process for all exercise sheet solutions.
 * 
 * @author Lisa Detzler
 * 
 */
public class TestAllSheetSolutionFiles {

	/**
	 * Initializes the static fields.
	 * 
	 * @author Lisa Detzler
	 */
	@Before
	public void init() {
		MainCodeGen.rootNode = null;
		MainCodeGen.output = true;
		MainCodeGen.generatedCode = null;
		MainCodeGen.isJavaCodeGeneration = true;
		CodeLineMapping.init();

		CodeGen.mainCodeObject = new MainCode();
		CodeGen.monitors = new HashMap<String, Code>();
		CodeGen.structs = new HashMap<String, Code>();
		CodeGen.usedVariableNames = new HashSet<String>();

	}

	/**
	 * Tests the code generation process of the file with the specified name. If
	 * <tt>printCode</tt> is <tt>true</tt> the generated code is printed to the
	 * console. If <tt>false</tt> it is not printed.
	 * 
	 * @param fileName
	 *            The name of the file to test.
	 * @param printCode
	 *            <tt>true</tt> if the generated code should be printed.
	 *            <tt>false</tt> otherwise.
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * @author Lisa Detzler
	 */
	public void testGenCode(String fileName, boolean printCode)
			throws ParseException, IOException {
		TestAllCodeGenFiles.testGenCode("SheetSolutionTests/" + fileName,
				printCode);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l1() throws ParseException, IOException {
		testGenCode("1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l2() throws ParseException, IOException {
		testGenCode("2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l3() throws ParseException, IOException {
		testGenCode("3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l4() throws ParseException, IOException {
		testGenCode("4", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l5() throws ParseException, IOException {
		testGenCode("5", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l6() throws ParseException, IOException {
		testGenCode("6", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l7() throws ParseException, IOException {
		testGenCode("7", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l8() throws ParseException, IOException {
		testGenCode("8", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l9() throws ParseException, IOException {
		testGenCode("9", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l10() throws ParseException, IOException {
		testGenCode("10", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l11() throws ParseException, IOException {
		testGenCode("11", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l12() throws ParseException, IOException {
		testGenCode("12", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l13() throws ParseException, IOException {
		testGenCode("13", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l14() throws ParseException, IOException {
		testGenCode("14", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l15() throws ParseException, IOException {
		testGenCode("15", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l16() throws ParseException, IOException {
		testGenCode("16", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l17() throws ParseException, IOException {
		testGenCode("17", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l18() throws ParseException, IOException {
		testGenCode("18", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l19() throws ParseException, IOException {
		testGenCode("19", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l20() throws ParseException, IOException {
		testGenCode("20", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l21() throws ParseException, IOException {
		testGenCode("21", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l22() throws ParseException, IOException {
		testGenCode("22", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void l23() throws ParseException, IOException {
		testGenCode("23", false);
	}
}
