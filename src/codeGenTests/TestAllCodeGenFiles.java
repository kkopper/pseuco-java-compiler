/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/

package codeGenTests;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;

import codeGen.Code;
import codeGen.CodeGen;
import codeGen.CodeLineMapping;
import codeGen.MainCode;
import codeGen.MainCodeGen;

import tree.ParseException;

/**
 * Tests the code generation process.
 * 
 * @author Lisa Detzler
 * 
 */
public class TestAllCodeGenFiles {

	/**
	 * Initializes the static fields.
	 * 
	 * @author Lisa Detzler
	 */
	@Before
	public void init() {
		MainCodeGen.rootNode = null;
		MainCodeGen.output = true;
		MainCodeGen.generatedCode = null;
		MainCodeGen.isJavaCodeGeneration = true;
		CodeLineMapping.init();

		CodeGen.mainCodeObject = new MainCode();
		CodeGen.monitors = new HashMap<String, Code>();
		CodeGen.structs = new HashMap<String, Code>();
		CodeGen.usedVariableNames = new HashSet<String>();
	}

	/**
	 * Tests the code generation process of the file with the specified name. If
	 * <tt>printCode</tt> is <tt>true</tt> the generated code is printed to the
	 * console. If <tt>false</tt> it is not printed.
	 * 
	 * @param fileName
	 *            The name of the file to test.
	 * @param printCode
	 *            <tt>true</tt> if the generated code should be printed.
	 *            <tt>false</tt> otherwise.
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * @author Lisa Detzler
	 */
	static public void testGenCode(String fileName, boolean printCode)
			throws ParseException, IOException {
		TestCodeGen testCodeGen = new TestCodeGen();
		MainCodeGen.initiateListOfExternJavaFiles();
		testCodeGen.generateCode(fileName);
		String fileCode = MainCodeGen.readFile("TestFiles/CodeGenTests/"
				+ fileName + ".txt");
		testCodeGen.assertString(fileCode, true, printCode);
	}

	/**
	 * Tests the line number mappings of the file with the specified name. If
	 * <tt>printCode</tt> is <tt>true</tt> the generated code is printed to the
	 * console. If <tt>false</tt> it is not printed.
	 * 
	 * @param pseuCoFileName
	 *            The name of the file to test.
	 * @param mappingFileName
	 *            The file containing the correct mapping.
	 * @param printCode
	 *            <tt>true</tt> if the generated code should be printed.
	 *            <tt>false</tt> otherwise.
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 * @author Lisa Detzler
	 */
	public void testLineNumberMatching(String pseuCoFileName,
			String mappingFileName, boolean printCode) throws ParseException,
			IOException {
		TestCodeGen testCodeGen = new TestCodeGen();
		testCodeGen.generateCode(pseuCoFileName);
		HashMap<String, HashMap<Integer, Integer>> lineNumberMapping = testCodeGen
				.readFileIntoHashMap(mappingFileName);
		testCodeGen.assertLineNumberMatching(lineNumberMapping, printCode);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arrays1() throws ParseException, IOException {
		testGenCode("Arrays1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arrays2() throws ParseException, IOException {
		testGenCode("Arrays2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arrays3() throws ParseException, IOException {
		testGenCode("Arrays3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysBufchan1() throws ParseException, IOException {
		testGenCode("ArraysBufchan1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysBufchan2() throws ParseException, IOException {
		testGenCode("ArraysBufchan3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysBufchan3() throws ParseException, IOException {
		testGenCode("ArraysBufchan3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysLock1() throws ParseException, IOException {
		testGenCode("ArraysLock1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysLock2() throws ParseException, IOException {
		testGenCode("ArraysLock2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysLock3() throws ParseException, IOException {
		testGenCode("ArraysLock3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arraysThread() throws ParseException, IOException {
		testGenCode("ArraysThread", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void call1() throws ParseException, IOException {
		testGenCode("Call1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void conditions1() throws ParseException, IOException {
		testGenCode("Conditions1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void conditions2() throws ParseException, IOException {
		testGenCode("Conditions2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void conditions3() throws ParseException, IOException {
		testGenCode("Conditions3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void conditions4() throws ParseException, IOException {
		testGenCode("Conditions4", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void declarations() throws ParseException, IOException {
		testGenCode("Declarations", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void doWhileStatement1() throws ParseException, IOException {
		testGenCode("DoWhileStatement1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void doWhileStatement2() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = false;
		testGenCode("DoWhileStatement2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void forStatement1() throws ParseException, IOException {
		testGenCode("ForStatement1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void forStatement2() throws ParseException, IOException {
		testGenCode("ForStatement2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void forStatement3() throws ParseException, IOException {
		testGenCode("ForStatement3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void forStatement4() throws ParseException, IOException {
		testGenCode("ForStatement4", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void forStatement11() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = false;
		testGenCode("ForStatement11", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void forStatement22() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = false;
		testGenCode("ForStatement22", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void ifStatement() throws ParseException, IOException {
		testGenCode("IfStatement", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void importReentrantLock1() throws ParseException, IOException {
		testGenCode("ImportReentrantLock1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void importReentrantLock2() throws ParseException, IOException {
		testGenCode("ImportReentrantLock2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void interruptedExceptions1() throws ParseException, IOException {
		testGenCode("InterruptedExceptions1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void interruptedExceptions2() throws ParseException, IOException {
		testGenCode("InterruptedExceptions2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void join() throws ParseException, IOException {
		testGenCode("Join", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void listing815() throws ParseException, IOException {
		testGenCode("Listing8.15", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void lock1() throws ParseException, IOException {
		testGenCode("Lock1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void lock2() throws ParseException, IOException {
		testGenCode("Lock2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void lock3() throws ParseException, IOException {
		testGenCode("Lock3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void lock4() throws ParseException, IOException {
		testGenCode("Lock4", false);
	}
	
	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void monitorLock1() throws ParseException, IOException {
		testGenCode("MonitorLock1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void monitorLock2() throws ParseException, IOException {
		testGenCode("MonitorLock2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void monitorVariable() throws ParseException, IOException {
		testGenCode("MonitorVariable", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void noMainAgent() throws ParseException, IOException {
		testGenCode("NoMainAgent", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void postfixpp() throws ParseException, IOException {
		testGenCode("Postfixpp", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void returnStartExpression1() throws ParseException, IOException {
		testGenCode("ReturnStartExpression1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void returnStartExpression2() throws ParseException, IOException {
		testGenCode("ReturnStartExpression2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void runnables1() throws ParseException, IOException {
		testGenCode("Runnables1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void runnables2() throws ParseException, IOException {
		testGenCode("Runnables2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void simpleTest1() throws ParseException, IOException {
		testGenCode("SimpleTest1", false);
	}
	
	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void selectCaseStatement() throws ParseException, IOException {
		testGenCode("selectCaseStatement", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void selectCaseStatement2() throws ParseException, IOException {
		testGenCode("selectCaseStatement2", false);
	}
	
	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void singleStatementDoWhile() throws ParseException, IOException {
		testGenCode("SingleStatementDoWhile", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void singleStatementFor() throws ParseException, IOException {
		testGenCode("SingleStatementFor", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void singleStatementIf() throws ParseException, IOException {
		testGenCode("SingleStatementIf", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void singleStatementWhile() throws ParseException, IOException {
		testGenCode("SingleStatementWhile", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void startExpression1() throws ParseException, IOException {
		testGenCode("StartExpression1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void startExpression2() throws ParseException, IOException {
		testGenCode("StartExpression2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void startExpression3() throws ParseException, IOException {
		testGenCode("StartExpression3", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void startExpression4() throws ParseException, IOException {
		testGenCode("StartExpression4", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void startExpression5() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = false;
		testGenCode("StartExpression5", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void structVariable() throws ParseException, IOException {
		testGenCode("StructVariable", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void synchronizedMethods() throws ParseException, IOException {
		testGenCode("SynchronizedMethods", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void waitForBool() throws ParseException, IOException {
		testGenCode("WaitForBool", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void waitForCondition1() throws ParseException, IOException {
		testGenCode("WaitForCondition1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void waitForCondition2() throws ParseException, IOException {
		testGenCode("WaitForCondition2", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void whileStatement1() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = true;
		testGenCode("WhileStatement1", false);
	}

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void whileStatement2() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = false;
		testGenCode("WhileStatement2", false);
	}

	// ------------------ line number matching tests -------------------------

	/**
	 * @author Lisa Detzler
	 * @throws ParseException
	 *             If an occurs in the parsing step.
	 * @throws IOException
	 *             If an I/O error occurs.
	 */
	@Test
	public void arrays1LNM() throws ParseException, IOException {
		MainCodeGen.isJavaCodeGeneration = true;
		testLineNumberMatching("Arrays1", "Arrays1-LNM", false);
	}
}
