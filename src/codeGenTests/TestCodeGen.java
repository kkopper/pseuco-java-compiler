/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/
package codeGenTests;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import codeGen.CodeGen;
import codeGen.CodeLineMapping;
import codeGen.MainCode;
import codeGen.MainCodeGen;
import codeGen.Code;

import tree.ParseException;
import tree.PseuCoParser;

public class TestCodeGen {

	public static String generatedJavaCode;

	public void generateCode(String filename) throws ParseException,
			IOException {
		String[] args = new String[1];
		args[0] = "TestFiles/CodeGenTests/" + filename + ".pc";
		MainCodeGen.output = false;
		try {
			PseuCoParser.main(args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void assertString(String path, String expectedString, boolean print) {
		File f = new File(path);
		StringBuffer genCodeBuff = new StringBuffer();
		try {
			int c;
			FileReader reader = new FileReader(f);

			ArrayList<Integer> notAllowed = new ArrayList<Integer>(4);
			notAllowed.add((int) '\n');
			notAllowed.add((int) '\r');
			notAllowed.add((int) ' ');

			boolean lastWasNotAllowed = false;

			while ((c = reader.read()) != -1) {
				if (notAllowed.contains(c)) {
					lastWasNotAllowed = true;
				} else {
					if (lastWasNotAllowed) {
						lastWasNotAllowed = false;
					}
					genCodeBuff.append((char) c);
				}
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String genCode = genCodeBuff.toString();
		if (print) {
			System.out.println(genCode);
		}
		expectedString = expectedString.replaceAll(" ", "");

		assertTrue(genCode.contains(expectedString));
	}

	@Before
	public void init() {
		MainCodeGen.rootNode = null;
		MainCodeGen.output = true;
		MainCodeGen.isJavaCodeGeneration = true;

		CodeGen.mainCodeObject = new MainCode();
		CodeGen.monitors = new HashMap<String, Code>();
		CodeGen.structs = new HashMap<String, Code>();
		CodeGen.usedVariableNames = new HashSet<String>();
	}

	public void assertString(String expectedString, boolean onlySubstring,
			boolean print) {
		String genCode = MainCodeGen.generatedCode;

		if (print) {
			System.out.println(genCode);
		}

		genCode = genCode.replaceAll(" ", "");
		genCode = genCode.replaceAll("\n", "");
		genCode = genCode.replaceAll("\r", "");
		genCode = genCode.replaceAll("\t", "");

		genCode = replaceSimulateHWInterrupt(genCode);

		expectedString = expectedString.replaceAll(" ", "");
		expectedString = expectedString.replaceAll("\n", "");
		expectedString = expectedString.replaceAll("\r", "");
		expectedString = expectedString.replaceAll("\t", "");

		if (onlySubstring) {
			assertTrue(genCode.contains(expectedString));
		} else {
			assertTrue(genCode.equals(expectedString));
		}
	}

	public void assertLineNumberMatching(
			HashMap<String, HashMap<Integer, Integer>> map, boolean printCode) {
		for (Map.Entry<String, HashMap<Integer, Integer>> m : map.entrySet()) {
			String fileName = m.getKey();
			if (printCode) {
				System.out.println(fileName + CodeGen.getLineSeparator());
			}
			for (Map.Entry<Integer, Integer> littleMap : m.getValue()
					.entrySet()) {
				int javaLineNumber = littleMap.getKey();
				int expectedPseuCoNumber = littleMap.getValue();
				if (printCode) {
					System.out.println(javaLineNumber + "  "
							+ expectedPseuCoNumber);
				}
				assertTrue(CodeLineMapping.getPseuCoLineNumber(javaLineNumber,
						fileName, true).contains(expectedPseuCoNumber));
			}
		}

		assert (CodeLineMapping.size() == map.size());

		if (printCode) {
			String genCode = MainCodeGen.generatedCode;
			System.out.println(genCode);
		}
	}

	public String replaceSimulateHWInterrupt(String code) {
		while (code.contains("Simulate.HWInterrupt();")) {
			int index = code.indexOf("Simulate.HWInterrupt();");
			code = code.substring(0, index)
					+ code.substring(index + "Simulate.HWInterrupt();".length());
		}
		return code;
	}

	public HashMap<String, HashMap<Integer, Integer>> readFileIntoHashMap(
			String fileName) throws IOException {
		HashMap<String, HashMap<Integer, Integer>> map = new HashMap<String, HashMap<Integer, Integer>>();
		HashMap<Integer, Integer> littleMap = new HashMap<Integer, Integer>();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(
					"TestFiles/CodeGenTests/" + fileName + ".txt")));
			String line = null;
			String name = "";
			while ((line = br.readLine()) != null) {
				if (line.isEmpty() || line.startsWith("//"))
					continue;
				line = line.replace(" ", "");
				line = line.replace("\t", "");
				line = line.replace("\r", "");
				line = line.replace("\n", "");
//				line = line.replace("$", "");
				String[] numbers = line.split("-");
				if (numbers[0].startsWith("$")) {
					map.put(name, littleMap);
					name = numbers[0];
					littleMap = new HashMap<Integer, Integer>();
					continue;
				}
				if (numbers.length > 1) {
					int javaNumber = Integer.valueOf(numbers[0]);
					int pseuCoNumber = Integer.valueOf(numbers[1].replace("-",
							""));
					littleMap.put(javaNumber, pseuCoNumber);
				}
			}
			name = name.replace("$", "");
			map.put(name, littleMap);

		} finally {
			if (br != null) {
				br.close();
			}
		}
		return map;
	}

	
}
