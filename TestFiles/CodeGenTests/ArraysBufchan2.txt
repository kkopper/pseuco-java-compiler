public static Intchan[][] cArray = PseuCo_initArrayIntchan2(new int[] {1, 22}, 100);
    
public static void PseuCo_f(){
    Intchan c = new Intchan(100);
    cArray[0][3] = c;
}
public static Intchan[][] PseuCo_initArrayIntchan2(int[] length, int sizeOfBufchan){
    Intchan[][] arrayName = new Intchan[length[0]][length[1]];
    for(int i1 = 0; i1 < length[1]; i1++){
        for(int i0 = 0; i0 < length[0]; i0++){
            arrayName[i0][i1] = new Intchan(sizeOfBufchan);
        }
    }
    return arrayName;
}