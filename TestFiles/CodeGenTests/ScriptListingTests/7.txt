package include;


public class Main { 
    public static Object wildcard = new Object();
    public static PseuCoThread thread = new PseuCoThread("mainAgentThread");
    public static PseuCoThread[] a = PseuCo_initArrayPseuCoThread1(new int[] {4});
    public static Intchan cc = new Intchan(10);
    public static Intchan dd = new Intchan(10);
    
    public static void PseuCo_factorial(IntChannel d, IntChannel c){
        int j = 0, n = 0, z = 0;
        
        while (true) {
            Work work = new Work(d, true, Thread.currentThread(), 1);
            z = d.handleSelect(work);
            
            n = 1;
            
            for (j = z; j > 0; j--){
                
                n = n * j;
            }
            Work work1 = new Work(c, false, n, Thread.currentThread(), 1);
            c.handleSelect(work1);
        }
    }
    public static PseuCoThread[] PseuCo_initArrayPseuCoThread1(int[] length){
        PseuCoThread[] arrayName = new PseuCoThread[length[0]];
        for(int i0 = 0; i0 < length[0]; i0++){
            arrayName[i0] = new PseuCoThread();
        }
        return arrayName;
    }
    
    
    public static void main(String[] args) {
        int j = 0;
        
        for (j = 0; j < 4; j++){
            final Intchan ccCopy = cc;
            final Intchan ddCopy = dd;
            
            a[j] = new PseuCoThread("mainAgent", new Runnable() { 
                @Override
                public void run() { 
                    PseuCo_factorial(ddCopy, ccCopy);
                } 
            });
            a[j].start();
        }
        
        for (j = 1; j <= 8; j++){
            Work work2 = new Work(dd, false, j, Thread.currentThread(), 1);
            dd.handleSelect(work2);
        }
        
        for (j = 1; j <= 8; j++){
            Work work3 = new Work(cc, true, Thread.currentThread(), 1);int res = cc.handleSelect(work3);
            
            System.out.println(" Die Fakultaet von " + j + " ist " + res + ".");
        }
    }
    
}
