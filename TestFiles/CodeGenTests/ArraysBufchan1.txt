public static void PseuCo_f(){
    Intchan c = new Intchan(1);
    Intchan[] cArray = PseuCo_initArrayIntchan1(new int[] {1}, 1);
        
    cArray[0] = c;
}
public static Intchan[] PseuCo_initArrayIntchan1(int[] length, int sizeOfBufchan){
    Intchan[] arrayName = new Intchan[length[0]];
    for(int i0 = 0; i0 < length[0]; i0++){
        arrayName[i0] = new Intchan(sizeOfBufchan);
    }
    return arrayName;
}