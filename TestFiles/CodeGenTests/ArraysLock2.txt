public static void PseuCo_f(){
    final ReentrantLock c = new ReentrantLock();
    ReentrantLock[][] cArray = PseuCo_initArrayReentrantLock2(new int[] {1, 19});
       
    cArray[3][2] = c;
}
public static ReentrantLock[][] PseuCo_initArrayReentrantLock2(int[] length){
    ReentrantLock[][] arrayName = new ReentrantLock[length[0]][length[1]];
    for(int i1 = 0; i1 < length[1]; i1++){
        for(int i0 = 0; i0 < length[0]; i0++){
            arrayName[i0][i1] = new ReentrantLock();
        }
    }
    return arrayName;
}