package include;


public class Main { 
    public static Object wildcard = new Object();
    public static PseuCoThread thread = new PseuCoThread("mainAgentThread");
    public static Intchan k = new Intchan(4);
    
    public static void PseuCo_produce(int id){
        
        while (true) {
            
            System.out.println("Erzeuger " + id + " hat " + id + " produziert.");
            Work work = new Work(k, false, id, Thread.currentThread(), 1);
            k.handleSelect(work);
        }
    }
    public static void PseuCo_consume(String name){
        
        while (true) {
            Work work1 = new Work(k, true, Thread.currentThread(), 1);int c = k.handleSelect(work1);
            
            System.out.println(" Verbraucher " + name + " hat " + c + " konsumiert.");
        }
    }
    
    
    public static void main(String[] args) {
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_produce(1);
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_produce(2);
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_produce(3);
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_produce(4);
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_produce(5);
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_consume("A");
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_consume("B");
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_consume("C");
            } 
        }).start();
        
        new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_consume("D");
            } 
        }).start();
    }
    
}