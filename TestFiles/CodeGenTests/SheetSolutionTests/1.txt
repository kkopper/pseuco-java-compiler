package include;


public class Main { 
    public static Object wildcard = new Object();
    public static PseuCoThread thread = new PseuCoThread("mainAgentThread");
    
    public static void main(String[] args) {
    }
    
}


package include;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PseuCo_DibbelabbesBar { 
    private final ReentrantLock monitorLock = new ReentrantLock();
    private int guests = 0;
    private boolean dirty = false;
    private Condition mayEnter = monitorLock.newCondition();
    
    public void PseuCo_enter(){
        monitorLock.lock();
        try{
        
            
            try {
                while(!((guests < 5 && dirty == false))) {
                    mayEnter.await();
                }
            } catch (InterruptedException e) {
            };
            
            guests++;
            
            if (guests >= 5) {
            
            dirty = true;
            }
        }finally{
            monitorLock.unlock();
        }
    }
    public void PseuCo_leave(){
        monitorLock.lock();
        try{
        
            
            guests--;
            
            if (guests <= 0) {
            
            dirty = false;
            }
            
            mayEnter.signalAll();
        }finally{
            monitorLock.unlock();
        }
    }
    

}