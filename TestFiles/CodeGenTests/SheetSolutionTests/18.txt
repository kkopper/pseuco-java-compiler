package include;


public class Main { 
    public static Object wildcard = new Object();
    public static PseuCoThread thread = new PseuCoThread("mainAgentThread");
    public static PseuCoThread a1 = new PseuCoThread(), a2 = new PseuCoThread(), b = new PseuCoThread();
    public static int mid = 0, fin = 0;
    public static IntHandshakeChan dd = new IntHandshakeChan();
    public static IntHandshakeChan cc = new IntHandshakeChan();
    public static BoolHandshakeChan rdy = new BoolHandshakeChan();
    public static BoolHandshakeChan go = new BoolHandshakeChan();
    
    public static void PseuCo_factorial(IntChannel c, BoolChannel g){
        int j = 0, n = 1;
        Work work = new Work(dd, true, Thread.currentThread(), 1);int z = dd.handleSelect(work);
        
        for (j = 1; j <= z; j++){
            
            n = n * j;
        }
        Work work1 = new Work(c, false, n, Thread.currentThread(), 1);
        c.handleSelect(work1);
        Work work2 = new Work(g, true, Thread.currentThread(), 1);
        wildcard = g.handleSelect(work2);
    }
    public static void PseuCo_barrier(BoolChannel r, BoolChannel g){
        Work work3 = new Work(r, true, Thread.currentThread(), 1);
        wildcard = r.handleSelect(work3);
        Work work4 = new Work(g, false, true, Thread.currentThread(), 1);
        g.handleSelect(work4);
        Work work5 = new Work(g, false, true, Thread.currentThread(), 1);
        g.handleSelect(work5);
    }
    
    
    public static void main(String[] args) {
        final BoolHandshakeChan goCopy = go;
        final IntHandshakeChan ccCopy = cc;
        
        a1 = new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_factorial(ccCopy, goCopy);
            } 
        });
        a1.start();
        final BoolHandshakeChan goCopy1 = go;
        final IntHandshakeChan ccCopy1 = cc;
        
        a2 = new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_factorial(ccCopy1, goCopy1);
            } 
        });
        a2.start();
        final BoolHandshakeChan goCopy2 = go;
        final BoolHandshakeChan rdyCopy = rdy;
        
        b = new PseuCoThread("mainAgent", new Runnable() { 
            @Override
            public void run() { 
                PseuCo_barrier(rdyCopy, goCopy2);
            } 
        });
        b.start();
        Work work6 = new Work(dd, false, 3, Thread.currentThread(), 1);
        dd.handleSelect(work6);
        Work work7 = new Work(cc, true, Thread.currentThread(), 1);
        mid = cc.handleSelect(work7);
        Work work8 = new Work(dd, false, mid, Thread.currentThread(), 1);
        dd.handleSelect(work8);
        Work work9 = new Work(cc, true, Thread.currentThread(), 1);
        fin = cc.handleSelect(work9);
        Work work10 = new Work(rdy, false, true, Thread.currentThread(), 1);
        rdy.handleSelect(work10);
        
        System.out.println("Die Fakultaet von der Fakultaet von 3 ist " + fin + ".");
    }
    
}