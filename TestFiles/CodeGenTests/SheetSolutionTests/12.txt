package include;


public class Main { 
    public static Object wildcard = new Object();
    public static PseuCoThread thread = new PseuCoThread("mainAgentThread");
    
    public static void main(String[] args) {
    }
    
}


package include;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PseuCo_FairMutex { 
    private final ReentrantLock monitorLock = new ReentrantLock();
    private int[] q = new int[3];
    private int i = 0;
    private int l = 0;
    private Condition turn = monitorLock.newCondition();
    private Condition turnEnd = monitorLock.newCondition();
    
    public void PseuCo_Lock(int id){
        monitorLock.lock();
        try{
        
            
            if (q[0] != id) {
                
                q[i++] = id;
                
                while (q[0] != id) {
                    
                    try {
                        while(!((l > 0))) {
                            turnEnd.await();
                        }
                    } catch (InterruptedException e) {
                    };
                    
                    try {
                        while(!((l == 0))) {
                            turn.await();
                        }
                    } catch (InterruptedException e) {
                    };
                }
            }
            
            l++;
            
            turnEnd.signalAll();
        }finally{
            monitorLock.unlock();
        }
    }
    public void PseuCo_Unlock(){
        monitorLock.lock();
        try{
        
            
            l--;
            
            if (l == 0) {
                
                i--;
                
                for (int j = 0; j < i; j++){
                    q[j] = q[j + 1];
                }
                
                turn.signalAll();
            }
        }finally{
            monitorLock.unlock();
        }
    }
    

}