public static void PseuCo_f(){
    Intchan c = new Intchan(100);
    Intchan[][][] cArray = PseuCo_initArrayIntchan3(new int[] {1, 22, 3}, 100);
        
    cArray[0][3][1] = c;
}
public static Intchan[][][] PseuCo_initArrayIntchan3(int[] length, int sizeOfBufchan){
    Intchan[][][] arrayName = new Intchan[length[0]][length[1]][length[2]];
    for(int i2 = 0; i2 < length[2]; i2++){
        for(int i1 = 0; i1 < length[1]; i1++){
            for(int i0 = 0; i0 < length[0]; i0++){
                arrayName[i0][i1][i2] = new Intchan(sizeOfBufchan);
            }
        }
    }
    return arrayName;
}