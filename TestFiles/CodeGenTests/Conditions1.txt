public class PseuCo_M { 
    private final ReentrantLock monitorLock = new ReentrantLock();
    private boolean b = false;
    private Condition c = monitorLock.newCondition();
    
    public void PseuCo_foo(){
        monitorLock.lock();
        try{
            try {
                while(!b) {
                    c.await();
                }
            } catch (InterruptedException e) {
            };
            c.signal();
            c.signalAll();
        }finally{
            monitorLock.unlock();
        }
    }
}