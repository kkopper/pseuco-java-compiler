pseuco-java-compiler
====================

You can find additional details and the compiled version on [pseuCo.com](http://pseuco.com/#/pseuco-java-compiler).

## Building instructions

The compiler uses [Gradle](https://gradle.org) as building system.
To build the compiler, just call

    gradle build

This will build the project and execute all tests. The following artifacts will be created:
* `build/libs/pseuco-java-compiler-X.X.jar`
* `build/distributions/PseuCoCo-X.X.tar` - The `jar` bundled with the `include`-folder.
* `build/distributions/PseuCoCo-X.X.zip` - The `jar` bundled with the `include`-folder.

If you just want to build the project without running the tests, just call

    gradle assemble

This will generate the same artifacts as before.

To generate only the `jar`-file without the distribution bundles, just call

    gradle jar

### Cleanup

    gradle clean

## Tests

To run the tests separately, just call

    gradle test

A test report can be found in `build/reports/tests/test/index.html`

## Documentation

To generate the documentation, just call

    gradle javadoc

The documentation can be found in `build/docs/javadoc/index.html`
